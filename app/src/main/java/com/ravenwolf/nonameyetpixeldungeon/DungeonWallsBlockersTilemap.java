/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon;

import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;

public class DungeonWallsBlockersTilemap extends BaseTilemap {


	public DungeonWallsBlockersTilemap() {
		super(Dungeon.level.tilesTex(),DungeonTilemap.tileSet());
		map(  Dungeon.level.map, Level.WIDTH );
	}

	public void map( int[] data, int cols ) {
		int[] raisedData = new int[Dungeon.level.LENGTH];
		//first column is always empty (walls)
		for (int i=0; i <Dungeon.level.WIDTH ; i++) {
			raisedData[i] = Terrain.RAISED_EMPTY;
		}

		for (int i=Dungeon.level.WIDTH; i < Dungeon.level.LENGTH ; i++) {
			//default raised is empty
			raisedData[i] = Terrain.RAISED_EMPTY;

			//lower walls blocker
			if (Terrain.isWall(data[i]) && i+Level.WIDTH < Level.LENGTH && !Dungeon.level.visited[i+Level.WIDTH] && !Dungeon.level.mapped[i+Level.WIDTH])
				raisedData[i] = Terrain.RAISED_WALL_FLAT;

		}
		super.map(raisedData, cols);
	}


	public void updateRaisedTile( int pos ) {

		if (pos < Level.WIDTH)
			return;

		int upperRaisedCell =pos - Level.WIDTH;
		boolean updateCurrentTile = false;
		boolean updateUpperRaisedTile = false;
		int upperRaisedTile = data[upperRaisedCell];
		int currentRaisedTile = data[pos];

		//update wall blockers (a flat wall is drawn over every wall to avoid knowing
		// if is an isometric wall before visiting the lower tile to the wall)
		if (data[pos] == Terrain.RAISED_WALL_FLAT){
			if (pos + Level.WIDTH < Level.LENGTH
					&& (Dungeon.level.visited[pos + Level.WIDTH] || Dungeon.level.mapped[pos+Level.WIDTH])
					//prevent removing flat wall over exit as it should be raised
					&& !Terrain.isChapterExit(Dungeon.level.map[pos + Level.WIDTH])) {
				updateCurrentTile = true;
				currentRaisedTile = Terrain.RAISED_EMPTY;
			}

			//update wall blockers when discovering door
			if (Terrain.isAnyDoor(Dungeon.level.map[pos])) {
				updateCurrentTile = true;
				currentRaisedTile = Terrain.RAISED_EMPTY;
			}
		}

		//ugly solution to update walls when seeing lower terrain tile
		if (data[upperRaisedCell] == Terrain.RAISED_WALL_FLAT && (Dungeon.level.visited[pos] ||  Dungeon.level.mapped[pos])
			//prevent removing flat wall over exit as it should be raised
			 && !Terrain.isChapterExit(Dungeon.level.map[pos])) {
			updateUpperRaisedTile = true;
			upperRaisedTile = Terrain.RAISED_EMPTY;
		}

		if (updateUpperRaisedTile) {
			data[upperRaisedCell]=upperRaisedTile;
			updated.union(upperRaisedCell % Level.WIDTH, upperRaisedCell / Level.WIDTH);
		}
		if (updateCurrentTile) {
			data[pos]=currentRaisedTile;
			updated.union(pos % Level.WIDTH, pos / Level.WIDTH);
		}

	}

}
