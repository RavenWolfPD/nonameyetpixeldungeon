/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon;

import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.watabou.noosa.TextureFilm;
import com.watabou.noosa.Tilemap;
import com.watabou.utils.Point;
import com.watabou.utils.PointF;

public abstract class BaseTilemap extends Tilemap {

	public static final int SIZE = 16;

	public BaseTilemap(String tex) {
		super(tex, new TextureFilm( tex, SIZE, SIZE ) );
	}

	public BaseTilemap(String tex, TextureFilm tileset ) {
		super(tex, tileset );
	}

	public int screenToTile( int x, int y ) {
		Point p = camera().screenToCamera( x, y ).
			offset( this.point().negate() ).
			invScale( SIZE ).
			floor();
		return p.x >= 0 && p.x < Level.WIDTH && p.y >= 0 && p.y < Level.HEIGHT ? p.x + p.y * Level.WIDTH : -1;
	}

	
	@Override
	public boolean overlapsPoint( float x, float y ) {
		return true;
	}
	

	public static PointF tileToWorld( int pos ) {
		return new PointF( pos % Level.WIDTH, pos / Level.WIDTH).scale( SIZE );
	}
	
	public static PointF tileCenterToWorld( int pos ) {
		return new PointF( 
			(pos % Level.WIDTH + 0.5f) * SIZE,
			(pos / Level.WIDTH + 0.5f) * SIZE - Dungeon.isometricOffset()) ;
	}

	@Override
	public boolean overlapsScreenPoint( int x, int y ) {
		return true;
	}

	public int getTileVariation(int pos){
		return Dungeon.getVarianceFactor(pos) > 75 ? 1 : 0;
	}
	public int getTileVariationWitRare(int pos){
		return Dungeon.getVarianceFactor(pos) < 8 ? 2 : Dungeon.getVarianceFactor(pos) > 75 ? 1 : 0;
	}

}
