/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.windows.WndOptions;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Bundlable;
import com.watabou.utils.Bundle;

public abstract class Trap implements Bundlable {


	private static final String TXT_HIDDEN_PLATE_CLICKS = "A hidden pressure plate clicks!";
	private static final String TXT_TRAPPED = "This tile is trapped!";

	private static final String TXT_R_U_SURE =
			"You are aware of a trap on this tile. Once you step on it, the trap would be " +
					"activated, which would most likely be quite a painful experience. Are you " +
					"REALLY sure you want to step here?";

	private static final String TXT_YES			= "Yes, I know what I'm doing";
	private static final String TXT_NO			= "No, I changed my mind";

	public static boolean stepConfirmed = false;

	//trap colors
	public static final int RED     = 0;
	public static final int ORANGE  = 1;
	public static final int YELLOW  = 2;
	public static final int GREEN   = 3;
	public static final int TEAL    = 4;
	public static final int VIOLET  = 5;
	public static final int WHITE   = 6;
	public static final int GREY    = 7;
	public static final int BLACK   = 8;

	//trap shapes
	public static final int DOTS        = 0;
	public static final int LINES       = 1;
	public static final int GRILL       = 2;
	public static final int STARS       = 3;
	public static final int DIAMOND     = 4;
	public static final int CROSS     	= 5;
	public static final int ARROWS_H   	= 6;
	public static final int ARROWS_V   	= 7;


	public int color;
	public int shape;

	public int pos;

	public boolean visible;
	public boolean active = true;
	public boolean disarmedByActivation = true;
	
	public boolean canBeHidden = true;
	public boolean canBeSearched = true;

	public boolean avoidsHallways = false; //whether this trap should avoid being placed in hallways

	public Trap set(int pos){
		this.pos = pos;
		return this;
	}

	//Some traps need to be adjusted after being placed
	// like vertical horizontal traps
	public void adjustTrap(Level level) {
	}

	public Trap reveal() {
		visible = true;
		GameScene.updateMap(pos);
		return this;
	}

	public Trap hide() {
		if (canBeHidden) {
			visible = false;
			GameScene.updateMap(pos);
			return this;
		} else {
			return reveal();
		}
	}

	public void trigger() {
		if (active) {

			if (Dungeon.visible[pos]) {
				Sample.INSTANCE.play( Assets.SND_TRAP);
				if( ( Terrain.flags[ Dungeon.level.map[ pos ] ] & Terrain.TRAPPED ) != 0 ) {
					GLog.i(TXT_HIDDEN_PLATE_CLICKS);
				}
				if (Actor.findChar(pos) == Dungeon.hero) {
					Dungeon.hero.interrupt();
				}
			}
			if (disarmedByActivation) disarm();
			Dungeon.level.discover(pos);
			activate();
		}
	}

	public abstract void activate();


	public void disarm(){
		active = false;
		visible = true;
		Dungeon.level.disarmTrap(pos);
	}

	public static void askForConfirmation( final Hero hero ) {
		GameScene.show(
				new WndOptions( TXT_TRAPPED, TXT_R_U_SURE, TXT_YES, TXT_NO ) {
					@Override
					protected void onSelect( int index ) {
						if (index == 0) {
							stepConfirmed = true;
							hero.resume();
							stepConfirmed = false;
						}
					}
				}
		);
	}

	public String name(){
		return null;
	}

	public String desc() {
		return description() + (disarmedByActivation ? "" : dontDisarmOnActivationText());
	}

	public String description() {
		return "Stepping onto a hidden pressure plate will activate the trap.";
	}

	protected String dontDisarmOnActivationText(){
		return "\n\nThis trap is designed to only trigger when stepped in, and wont wer of upon activation.";
	}

	private static final String POS	= "pos";
	private static final String VISIBLE	= "visible";
	private static final String ACTIVE = "active";
	private static final String CAN_BE_SEARCHED	= "canBeSearched";
	private static final String DISARMED_BY_ACTIVATION = "disarmedByActivation";

	@Override
	public void restoreFromBundle( Bundle bundle ) {
		pos = bundle.getInt( POS );
		visible = bundle.getBoolean( VISIBLE );
		active = bundle.getBoolean(ACTIVE);
		disarmedByActivation = bundle.getBoolean(DISARMED_BY_ACTIVATION);
		canBeSearched =  bundle.getBoolean(CAN_BE_SEARCHED);
	}

	@Override
	public void storeInBundle( Bundle bundle ) {
		bundle.put( POS, pos );
		bundle.put( VISIBLE, visible );
		bundle.put( ACTIVE, active );
		bundle.put( CAN_BE_SEARCHED, canBeSearched );
		bundle.put( DISARMED_BY_ACTIVATION, disarmedByActivation );
	}
}
