/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Mob;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.SpectralGuardian;
import com.ravenwolf.nonameyetpixeldungeon.items.Generator;
import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.Weapon;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.melee.MeleeWeapon;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;


public class GuardianTrap extends AlarmTrap {

	{

		shape = STARS;
	}

	@Override
	public void activate() {
		super.activate();

		spawnGuardian();
	}

	public void spawnGuardian() {

		int spawnPos = Dungeon.level.randomRespawnCell(false, true);

		if (spawnPos != -1 ) {
			Weapon weap;
			do {
				weap = (Weapon) Generator.random(Generator.Category.WEAPON);
			} while (!(weap instanceof MeleeWeapon));

			weap.identify(Item.ENCHANT_KNOWN);
			Mob mob = new SpectralGuardian((MeleeWeapon) weap);
			mob.state = mob.HUNTING;
			mob.beckon(pos);
			mob.pos = spawnPos;

			GameScene.add(mob);
			Actor.occupyCell(mob);
		}
	}

}


