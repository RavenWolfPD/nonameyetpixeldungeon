/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.DungeonTilemap;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.NoNameYetPixelDungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfDisintegration;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.DeathRay;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.PurpleParticle;
import com.watabou.noosa.audio.Sample;
import com.watabou.noosa.particles.Emitter;
import com.watabou.utils.Bundle;
import com.watabou.utils.PointF;
import com.watabou.utils.Random;

public class DisintegrationTrap extends SideWallTrap {

    {
        color = VIOLET;

        canBeHidden = true;
        avoidsHallways = true;
    }

    private boolean charging; //stores if the trap was triggered, but not fired yet

    Emitter emitter1;
    Emitter emitter2;

    public String name(){
        return "Disintegration trap";
    }

    public String description() {
        return "This trap will trigger some arcane devices hidden on the walls, shooting disintegration beams from each sidewall." +
                "\\n\\nThankfully, it needs to build energy before shooting and will give you enough time to step aside.";
    }



    @Override
    public void activate() {
        charging=true;
        chargeRays(wall1, wall2);
    }
/*
    private void shootRays(final int wall1, final int wall2){
        if(Terrain.isWall(Dungeon.level.map[wall1])) {
            shootRay(wall1, wall2);
        }
        if(Terrain.isWall(Dungeon.level.map[wall2])) {
            shootRay(wall2, wall1);
        }
    }
*/
    private void chargeRays(final int wall1, final int wall2){

        if (Terrain.isRealWall(Dungeon.level.map[wall1])) {
            emitter1 = CellEmitter.get(wall1);
            emitter1.visible = Dungeon.visible[wall1];
            emitter1.pour(DIRECTED_PARTICLE, 0.2f);
        }
        if (Terrain.isRealWall(Dungeon.level.map[wall2])) {
            emitter2 = CellEmitter.get(wall2);
            emitter2.visible = Dungeon.visible[wall2];
            emitter2.pour(DIRECTED_PARTICLE, 0.2f);
        }
        Actor.addDelayed(new Actor() {
            @Override
            protected boolean act() {
                if(Terrain.isRealWall(Dungeon.level.map[wall1])) {
                    shootRay(wall1, wall2);
                    emitter1.visible = Dungeon.visible[wall1];
                    emitter1.burst(PurpleParticle.BURST, 6);
                }
                if(Terrain.isRealWall(Dungeon.level.map[wall2])) {
                    shootRay(wall2, wall1);
                    emitter2.visible = Dungeon.visible[wall2];
                    emitter2.burst(PurpleParticle.BURST, 6);
                }
                charging=false;
                Actor.remove(this);
                return true;
            }

        }, Actor.TICK);

    }

    private void shootRay(int from, int to){

        Sample.INSTANCE.play(Assets.SND_RAY,0.5f);
        int target = Ballistica.cast(from, to, false, false);

        if (Dungeon.visible[pos] || Dungeon.visible[from] || Dungeon.visible[target])
            NoNameYetPixelDungeon.scene().add( new DeathRay(  DungeonTilemap.tileCenterToWorld( from ), DungeonTilemap.tileCenterToWorld( target ) ) );

        boolean terrainAffected = false;

        for (int i=1; i <= Ballistica.distance ; i++) {

            int pos = Ballistica.trace[i];

            boolean affected = WandOfDisintegration.affectCell(pos);
            if (affected) {
                terrainAffected = true;
            }
            Char ch = Actor.findChar( pos );
            if (ch != null) {
                if (Dungeon.visible[pos]) {
                    CellEmitter.center(pos).burst(PurpleParticle.BURST, Random.IntRange(1, 2));
                }
                hitChar(ch);
            }
        }

        if (terrainAffected) {
            Dungeon.observe();
        }

    }


    private void hitChar(Char ch){
        int power = (6 + Dungeon.chapter() * 4);
        ch.damage( ch.absorb( power, true ),this, Element.UNHOLY_PERIODIC);
    }


    private static final String CHARGING = "charging";

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle(bundle);
        bundle.put( CHARGING, charging );
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle(bundle);
        charging =  bundle.getBoolean( CHARGING );
        if (charging){
            //use an actor as Dungeon.level is not set at this time
            Actor.addDelayed(new Actor() {
                @Override
                public int actingPriority(){
                    return HERO_ACTING_PRIORITY+1;
                }
                @Override
                protected boolean act() {
                    chargeRays(wall1, wall2);
                    Actor.remove(this);
                    return true;
                }
            }, -Actor.TICK);
        }
    }

    private Emitter.Factory DIRECTED_PARTICLE = new Emitter.Factory() {
        @Override
        public void emit(Emitter emitter, int index, float x, float y) {
            PointF point= DungeonTilemap.tileCenterToWorld(DisintegrationTrap.this.pos);

            PurpleParticle s = ((PurpleParticle) emitter.recycle(PurpleParticle.class));
            s.reset(x, y);
            s.speed.set(point.x - x, point.y - y);
            s.speed.normalize().scale(DungeonTilemap.SIZE*2f);

            //offset the particles to be generated near the wall border instead of center
            s.x += s.speed.x/4f;
            s.y += s.speed.y/4f;
        }

        @Override
        public boolean lightMode() {
            return true;
        }
    };

}
