/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.levels;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.DungeonTilemap;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs.Ghost;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.AlarmTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.SummoningTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ChillingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ConfusionTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.PoisonDartTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ShockingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.SpearsTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.TeleportationTrap;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.FlowParticle;
import com.watabou.noosa.Game;
import com.watabou.noosa.Scene;
import com.watabou.noosa.particles.Emitter;
import com.watabou.noosa.particles.PixelParticle;
import com.watabou.utils.ColorMath;
import com.watabou.utils.PointF;
import com.watabou.utils.Random;


public class SewerLevel extends RegularLevel {

	{
		color1 = 0x48763c;
		color2 = 0x59994a;
	}
	
	@Override
	public String tilesTex() {
		return Assets.TILES_SEWERS;
	}
	
	@Override
	public String waterTex() {
		return Assets.WATER_SEWERS;
	}

    protected boolean[] water() {
        return Patch.generate( feeling == Feeling.WATER ? 0.60f : 0.45f, 5 );
    }

    protected boolean[] grass() {
        return Patch.generate( feeling == Feeling.GRASS ? 0.60f : 0.40f, 4 );
    }

	@Override
	protected Class<?>[] trapClasses() {
		return new Class<?>[]{
				SpearsTrap.class, ChillingTrap.class,
				AlarmTrap.class,  ShockingTrap.class, PoisonDartTrap.class,
				ConfusionTrap.class, SummoningTrap.class, TeleportationTrap.class };
	}

	@Override
	protected float[] trapChances() {
		return 	new float[]{
				6, 3,
				2, 2, 2,
				1, 1, 1};
	}

//	@Override
//	protected boolean generateRooms() {
//
//		//split( new Rect( 0, 0, WIDTH - 1, HEIGHT - 1 ) );
//
//
//		split(new Rect(0, 0 , WIDTH/4, HEIGHT - HEIGHT/4));
//		split(new Rect(WIDTH - WIDTH/4, 0 , WIDTH - 1, HEIGHT -1));
//		split(new Rect(WIDTH/4, 0 , WIDTH - 1, HEIGHT /4));
//		split(new Rect(0, HEIGHT - HEIGHT/4 , WIDTH - WIDTH/4, HEIGHT -1));
//
//		//Large room on the middle
//		rooms.add((Room) new Room().set( new Rect(WIDTH/4+1, HEIGHT /4+1, WIDTH - WIDTH/4-1,  HEIGHT -HEIGHT /4-1) ));
//
//
//		/*
//		//Large corridor on the top
//		rooms.add((Room) new Room().set( new Rect(WIDTH - 2, 2, WIDTH - 2,  HEIGHT /4) ));
//
//		//split the lower rooms
//		split(new Rect(0, HEIGHT /4 , WIDTH - 1, HEIGHT -1));
//*/
//		if (rooms.size() < 8) {
//			return false;
//		}
//
//		return true;
//	}

	@Override
	protected void decorate() {
		
		for (int i=0; i < WIDTH; i++) {
			if (map[i] == Terrain.WALL &&  
				map[i + WIDTH] == Terrain.WATER &&
				Random.Int( 4 ) == 0) {
				
				map[i] = Terrain.WALL_DECO;
			}
		}
		
		for (int i=WIDTH; i < LENGTH - WIDTH; i++) {

			if (this.feeling == Feeling.WATER && map[i] == Terrain.WALL &&
					map[i + WIDTH] == Terrain.WATER  && map[i - WIDTH] == Terrain.WATER
					 && Terrain.isWall(map[i - 1]) && Terrain.isWall(map[i + 1])
					&& Random.Int( 6 ) == 0) {

				map[i] = Terrain.WALL_GRATE;
				continue;
			}
			if (map[i] == Terrain.WALL && 
				map[i - WIDTH] == Terrain.WALL && 
				map[i + WIDTH] == Terrain.WATER &&
				Random.Int( 3 ) == 0) {
				
				map[i] = Terrain.WALL_DECO;
			}
		}
		
		for (int i=WIDTH + 1; i < LENGTH - WIDTH - 1; i++) {
			if (map[i] == Terrain.EMPTY) { 
				
				int count = 
					(map[i + 1] == Terrain.WALL ? 1 : 0) + 
					(map[i - 1] == Terrain.WALL ? 1 : 0) + 
					(map[i + WIDTH] == Terrain.WALL ? 1 : 0) +
					(map[i - WIDTH] == Terrain.WALL ? 1 : 0);
				
				if (Random.Int( 16 ) < count * count) {
					map[i] = Terrain.EMPTY_DECO;
				}
			}

			if (map[i] == Terrain.STATUE && map[i + 1] == Terrain.WATER && map[i - 1] == Terrain.WATER
					&& map[i + WIDTH] == Terrain.WATER && map[i - WIDTH] == Terrain.WATER){
				map[i] = Terrain.STATUE2_SP;
			}

		}

//		while (true) {
//			int pos = roomEntrance.random();
//			if (pos != entrance) {
//				map[pos] = Terrain.SIGN;
//				break;
//			}
//		}

	}
	
	@Override
	protected void createMobs() {
		super.createMobs();

		Ghost.Quest.spawn(this);
	}
	
//	@Override
//	protected void createItems() {
//		if (Dungeon.dewVial && Random.Int( 4 - Dungeon.depth ) == 0) {
//			addItemToSpawn( new DewVial() );
//			Dungeon.dewVial = false;
//		}
		
//		super.createItems();
//	}
	
	@Override
	public void addVisuals( Scene scene ) {
		super.addVisuals(scene);
		addVisuals(this, scene);
	}
	
	public static void addVisuals( Level level, Scene scene ) {
		for (int i=0; i < LENGTH; i++) {
			if (level.map[i] == Terrain.WALL_DECO) {
				scene.add( new Sink( i ) );
			}
			if (level.map[i] == Terrain.WALL_GRATE)
				scene.add( new FlowParticle.Flow( i ) );
		}
	}

    @Override
    public String tileName( int tile ) {
        return SewerLevel.tileNames(tile);
    }

    @Override
    public String tileDesc( int tile ) {
        return SewerLevel.tileDescs(tile);
    }
	
//	@Override
	public static String tileNames( int tile ) {
		switch (tile) {
		case Terrain.WATER:
			return "Murky water";
		default:
			return Level.tileNames(tile);
		}
	}

//	@Override
	public static String tileDescs(int tile) {
		switch (tile) {
		case Terrain.EMPTY_DECO:
			return "Wet yellowish moss covers the floor.";
		case Terrain.BOOKSHELF:
			return "The bookshelf is packed with some mouldy books. Maybe there would be something useful in here?";
        case Terrain.SHELF_EMPTY:
            return "The bookshelf is packed with some mouldy books.";
		default:
			return Level.tileDescs(tile);
		}
	}
	
	private static class Sink extends Emitter {
		
		private int pos;
		private float rippleDelay = 0;
		
		private static final Emitter.Factory factory = new Factory() {
			
			@Override
			public void emit( Emitter emitter, int index, float x, float y ) {
				WaterParticle p = (WaterParticle)emitter.recycle( WaterParticle.class );
				p.reset( x, y );
			}
		};
		
		public Sink( int pos ) {
			super();
			
			this.pos = pos;
			
			PointF p = DungeonTilemap.tileCenterToWorld( pos );
			if (Dungeon.isIsometric())
				pos( p.x - 2, p.y + Dungeon.isometricOffset() + 2, 4, 0 );
			else
				pos( p.x - 2, p.y + 1, 4, 0 );
			
			pour( factory, 0.05f );
		}
		
		@Override
		public void update() {
			if (visible = Dungeon.visible[pos] && pos+WIDTH < LENGTH && Dungeon.visible[pos+WIDTH]) {
				
				super.update();
				
				if ((rippleDelay -= Game.elapsed) <= 0) {
					GameScene.ripple( pos + WIDTH ).y -= DungeonTilemap.SIZE / 2;
					rippleDelay = Random.Float( 0.2f, 0.3f );
				}
			}
		}
	}

	public static final class WaterParticle extends PixelParticle {
		
		public WaterParticle() {
			super();
			
			acc.y = 50;
			am = 0.5f;
			
			color( ColorMath.random( 0xb6ccc2, 0x3b6653 ) );
			size( 2 );
		}
		
		public void reset( float x, float y ) {
			revive();
			
			this.x = x;
			this.y = y;
			
			speed.set( Random.Float( -2, +2 ), 0 );
			
			left = lifespan = 0.5f;
		}
	}
}
