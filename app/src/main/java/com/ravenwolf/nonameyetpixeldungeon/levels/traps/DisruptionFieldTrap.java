/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.DisruptionField;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.watabou.noosa.audio.Sample;

public class DisruptionFieldTrap extends Trap{

	{
		color = VIOLET;
		shape = DOTS;
	}

	public String name(){
		return "Disruption field trap";
	}

	public String description() {
		return "When activated, this trap releases highly unstable energy that will spread to nearby spaces." +
				" The energy will burst in a disruptive blast, damaging and disrupting entities caught in the field.";
	}

	@Override
	public void activate() {

		int power = 14 + Dungeon.chapter() * 6;
		if (Dungeon.visible[ pos ]){
			Sample.INSTANCE.play(Assets.SND_MELD);
		}
		GameScene.add(Blob.seed(pos, DisruptionField.SPREAD_STATE, DisruptionField.class));
		DisruptionField blob = (DisruptionField) Dungeon.level.blobs.get(DisruptionField.class);
		if (blob.cur[pos] > 0)
			blob.power[pos] = power;
	}

}
