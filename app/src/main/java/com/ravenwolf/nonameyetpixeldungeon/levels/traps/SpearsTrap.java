/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.DungeonTilemap;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.NoNameYetPixelDungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.MissileSprite;
import com.watabou.noosa.audio.Sample;
import com.watabou.noosa.particles.Emitter;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.PointF;
import com.watabou.utils.Random;

public class SpearsTrap extends SideWallTrap {

	{
		color = GREY;
		
		canBeHidden = false;
		avoidsHallways = true;
	}

	private boolean charging; //stores if the trap was triggered, but not fired yet

	Emitter emitter1;
	Emitter emitter2;

	public String name(){
		return "Spear trap";
	}

	public String description() {
		return "This trap will trigger a hidden mechanism on the walls, shooting spears with deadly force from each sidewall." +
				"\n\nThankfully, the worn mechanism will give you enough time to get out of the way.";
	}

	public void activateAtPos(int pos, int wall1, int wall2) {
		this.pos = pos;
		this.wall1 = wall1;
		this.wall2 = wall2;
		activate();
	}

	@Override
	public void activate() {
		charging=true;
		chargeSpears(wall1, wall2);
	}


	private void chargeSpears(final int wall1, final int wall2){

		if(Terrain.isRealWall(Dungeon.level.map[wall1])) {
			emitter1 = CellEmitter.get(wall1);
			emitter1.visible = Dungeon.visible[wall1];
			emitter1.pour(DIRECTED_PARTICLE, 0.2f);
		}
		if(Terrain.isRealWall(Dungeon.level.map[wall2])) {
			emitter2 = CellEmitter.get(wall2);
			emitter2.visible = Dungeon.visible[wall2];
			emitter2.pour(DIRECTED_PARTICLE, 0.2f);
		}

		Actor.addDelayed(new Actor() {
			private int shoots;

			@Override
			protected boolean act() {

				if(Terrain.isRealWall(Dungeon.level.map[wall1])) {
					launchSpear(wall1, wall2);
					emitter1.visible = Dungeon.visible[wall1];
					emitter1.burst(DIRECTED_PARTICLE, 6);
				}
				if(Terrain.isRealWall(Dungeon.level.map[wall2])) {
					launchSpear(wall2, wall1);
					emitter2.visible = Dungeon.visible[wall2];
					emitter2.burst(DIRECTED_PARTICLE, 6);
				}
				if (shoots == 0){
					Actor.remove(this);
					charging=false;
					return true;
				}
				return false;
			}

			private void launchSpear(final int from, int to){
				final int target = Ballistica.cast(from, to, false, true);
				final Char ch = Actor.findChar(target);
				final Actor toRemove = this;
				if (Dungeon.visible[from]) {
					Sample.INSTANCE.play(Assets.SND_MISS, 0.6f, 0.6f, 1.2f);
				}

				if (Dungeon.visible[pos] || Dungeon.visible[from] || Dungeon.visible[target]) {
					shoots++;
					((MissileSprite) NoNameYetPixelDungeon.scene().recycle(MissileSprite.class)).
						reset(from, target, ItemSpriteSheet.JAVELIN, new Callback() {
							@Override
							public void call() {

								if (ch != null) {
									hitChar(ch);
								}
								shoots--;
								if(shoots == 0) {
									next();
									Actor.remove(toRemove);
									charging=false;
								}
							}
						});
				}else if (ch != null) {
						hitChar(ch);
					}
			}
		},Actor.TICK);
	}

	private void hitChar(Char ch){
		int power = 6 + Dungeon.chapter() * 4;
		int damage = ch.absorb( Random.IntRange( power / 2 , power ));
		Sample.INSTANCE.play(Assets.SND_HIT);
		ch.damage(damage, this, Element.PHYSICAL);
	}


	private static final String CHARGING = "charging";

	@Override
	public void storeInBundle( Bundle bundle ) {
		super.storeInBundle(bundle);
		bundle.put( CHARGING, charging );
	}

	@Override
	public void restoreFromBundle( Bundle bundle ) {
		super.restoreFromBundle(bundle);
		charging =  bundle.getBoolean( CHARGING );
		if (charging){
			//use an actor as Dungeon.level is not set at this time
			Actor.addDelayed(new Actor() {
				@Override
				public int actingPriority(){
					return HERO_ACTING_PRIORITY+1;
				}
				@Override
				protected boolean act() {
					chargeSpears(wall1, wall2);
					Actor.remove(this);
					return true;
				}
			}, -Actor.TICK);
		}
	}




	private Emitter.Factory DIRECTED_PARTICLE = new Emitter.Factory() {
		@Override
		public void emit(Emitter emitter, int index, float x, float y) {
			PointF point= DungeonTilemap.tileCenterToWorld(SpearsTrap.this.pos);

			Speck s = (Speck)emitter.recycle( Speck.class );
			s.reset( index, x, y, Speck.DIRECTED_WOOL );


			s.speed.set(point.x - x, point.y - y);
			s.speed.normalize().scale(DungeonTilemap.SIZE*2f);

			//offset the particles to be generated near the wall border instead of center
			s.x += s.speed.x/4f;
			s.y += s.speed.y/4f;
		}

		/*@Override
		public boolean lightMode() {
			return false;
		}*/
	};

}
