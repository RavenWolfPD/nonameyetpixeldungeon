/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.levels;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.DungeonTilemap;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs.Wandmaker;
import com.ravenwolf.nonameyetpixeldungeon.levels.Room.Type;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.AlarmTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.SummoningTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.BurningTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ChillingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ConfusionTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.GrippingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.PoisonDartTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ShockingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.SpearsTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.TeleportationTrap;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Halo;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.FlameParticle;
import com.watabou.noosa.Scene;
import com.watabou.noosa.particles.Emitter;
import com.watabou.utils.PointF;
import com.watabou.utils.Random;

public class PrisonLevel extends RegularLevel {

	{
		color1 = 0x6a723d;
		color2 = 0x88924c;
	}
	
	@Override
	public String tilesTex() {
/*
		if (Dungeon.GRAVEYARD_OPTION.equals(Dungeon.prisonOption) && Dungeon.depth > Dungeon.PRISON_PATHWAY)
			return Assets.TILES_PRISON_GRAVEYARD;
*/
		return Assets.TILES_PRISON;
	}
	
	@Override
	public String waterTex() {
		return Assets.WATER_PRISON;
	}

    protected boolean[] water() {
        return Patch.generate( feeling == Feeling.WATER ? 0.65f : 0.45f, 4 );
    }

    protected boolean[] grass() {
        return Patch.generate( feeling == Feeling.GRASS ? 0.60f : 0.40f, 3 );
    }

	@Override
	protected Class<?>[] trapClasses() {
		return new Class<?>[]{
				  		PoisonDartTrap.class, SpearsTrap.class, ChillingTrap.class, BurningTrap.class,
						AlarmTrap.class, ShockingTrap.class, GrippingTrap.class,
						ConfusionTrap.class, SummoningTrap.class, TeleportationTrap.class };
	}

	@Override
	protected float[] trapChances() {
		return new float[]{
						6, 4, 4, 4,
						2, 2, 2,
						1, 1, 1};
	}

	@Override
	protected void assignRoomType() {
		super.assignRoomType();

		Wandmaker.Quest.spawn( this, roomEntrance, rooms );

		for (Room r : rooms) {
			if (r.type == Type.TUNNEL) {
				r.type = Type.PASSAGE;
			}
		}
	}
	
	@Override
	protected void decorate() {
		
		for (int i=WIDTH + 1; i < LENGTH - WIDTH - 1; i++) {
			if (map[i] == Terrain.EMPTY) { 
				
				float c = 0.05f;
				if (map[i + 1] == Terrain.WALL && map[i + WIDTH] == Terrain.WALL) {
					c += 0.2f;
				}
				if (map[i - 1] == Terrain.WALL && map[i + WIDTH] == Terrain.WALL) {
					c += 0.2f;
				}
				if (map[i + 1] == Terrain.WALL && map[i - WIDTH] == Terrain.WALL) {
					c += 0.2f;
				}
				if (map[i - 1] == Terrain.WALL && map[i - WIDTH] == Terrain.WALL) {
					c += 0.2f;
				}
				
				if (Random.Float() < c) {
					map[i] = Terrain.EMPTY_DECO;
				}
			}
		}
		
		for (int i=0; i < WIDTH; i++) {
			if (map[i] == Terrain.WALL &&  
				(map[i + WIDTH] == Terrain.EMPTY || map[i + WIDTH] == Terrain.EMPTY_SP) &&
				Random.Int( 6 ) == 0) {
				
				map[i] = Terrain.WALL_DECO;
			}
		}
		
		for (int i=WIDTH; i < LENGTH - WIDTH; i++) {
			if (map[i] == Terrain.WALL && 
				map[i - WIDTH] == Terrain.WALL && 
				(map[i + WIDTH] == Terrain.EMPTY || map[i + WIDTH] == Terrain.EMPTY_SP) &&
				Random.Int( 3 ) == 0) {
				
				map[i] = Terrain.WALL_DECO;
			}
		}

//        while (true) {
//            int pos = roomEntrance.random_top();
//            if (map[pos] == Terrain.WALL) {
//                map[pos] = Terrain.WALL_SIGN;
//                break;
//            }
//        }
	}

    @Override
    public String tileName( int tile ) {
        return PrisonLevel.tileNames(tile);
    }

    @Override
    public String tileDesc( int tile ) {
        return PrisonLevel.tileDescs(tile);
    }
	
//	@Override
	public static String tileNames( int tile ) {
		switch (tile) {
		case Terrain.WATER:
			return "Dark cold water.";
		default:
			return Level.tileNames(tile);
		}
	}
	
//	@Override
	public static String tileDescs(int tile) {
		switch (tile) {
		case Terrain.EMPTY_DECO:
			return "There are old blood stains on the floor.";
		case Terrain.BOOKSHELF:
			return "This is probably a vestige of a prison library. Maybe there would be something useful in here?";
        case Terrain.SHELF_EMPTY:
            return "This is probably a vestige of a prison library.";
		case Terrain.EMPTY_SP2:
			return "Bones litter the floor, what happened here?";
		default:
			return Level.tileDescs(tile);
		}
	}
	
	@Override
	public void addVisuals( Scene scene ) {
		super.addVisuals( scene );
		addVisuals( this, scene );
	}
	
	public static void addVisuals( Level level, Scene scene ) {
		for (int i=0; i < LENGTH; i++) {
			if (level.map[i] == Terrain.WALL_DECO) {
				scene.add( new Torch( i ) );
			}
		}
	}
	
	private static class Torch extends Emitter {
		
		private int pos;
		
		public Torch( int pos ) {
			super();
			
			this.pos = pos;
			
			PointF p = DungeonTilemap.tileCenterToWorld( pos );
			pos( p.x - 1, p.y + Dungeon.isometricOffset() + 3, 2, 0 );
			
			pour( FlameParticle.FACTORY, 0.15f );
			add( new Halo( 12, 0xFFFFCC, 0.2f ).point( p.x, p.y + Dungeon.isometricOffset()) );
		}
		
		@Override
		public void update() {
			if (visible = Dungeon.visible[pos] && pos+WIDTH < LENGTH && Dungeon.visible[pos+WIDTH]) {
				super.update();
			}
		}
	}
}