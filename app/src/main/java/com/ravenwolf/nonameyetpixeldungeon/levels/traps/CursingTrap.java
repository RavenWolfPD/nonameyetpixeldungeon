/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.items.EquipableItem;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.items.armours.Armour;
import com.ravenwolf.nonameyetpixeldungeon.items.rings.Ring;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.Weapon;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ShadowParticle;
import com.watabou.noosa.audio.Sample;

import java.util.ArrayList;
import java.util.Collections;

public class CursingTrap extends Trap {

	{
		color = VIOLET;
		shape = LINES;
	}

	@Override
	public String name(){
		return "Cursing trap";
	}

	@Override
	public String description() {
		return "This trap contains the same malevolent magic found in cursed equipment. Triggering it will curse some items in the immediate area.";
	}

	@Override
	public void activate() {
		if (Dungeon.visible[ pos ]) {
			CellEmitter.get(pos).burst(ShadowParticle.UP, 5);
			Sample.INSTANCE.play(Assets.SND_CURSED);
		}

		Heap heap = Dungeon.level.heaps.get( pos );
		if (heap != null){
			for (Item item : heap.items){
				if (item.isUpgradeable())
					curse(item);
			}
		}

		if (Dungeon.hero.pos == pos ){
			curse(Dungeon.hero);
		}
	}

	public static void curse(Hero hero){

		//items the trap can curse
		ArrayList<Item> canCurse = new ArrayList<>();

		Weapon weapon = hero.belongings.weap1;
		if (weapon != null && !weapon.cursed) {
			canCurse.add(weapon);
		}

		EquipableItem offhand = hero.belongings.weap2;
		if (offhand != null && !offhand.cursed) {
			canCurse.add(weapon);
		}

		Armour armor = hero.belongings.armor;
		if (armor != null && !armor.cursed){
			canCurse.add(armor);
		}

		Ring ring1 = hero.belongings.ring1;
		if (ring1 != null && !ring1.cursed){
			canCurse.add(ring1);
		}

		Ring ring2 = hero.belongings.ring2;
		if (ring2 != null && !ring2.cursed){
			canCurse.add(ring2);
		}


		Collections.shuffle(canCurse);

		if (!canCurse.isEmpty()){
			curse(canCurse.remove(0));
			GLog.n( "Your worn equipment becomes cursed!" );
		}

	}

	private static void curse(Item item){
		item.identify(Item.ENCHANT_KNOWN);

		if (item instanceof Weapon){
			Weapon w = (Weapon) item;
			if (w.enchantment == null){
				w.enchant();
			}
		}
		if (item instanceof Armour){
			Armour a = (Armour) item;
			if (a.glyph == null){
				a.inscribe();
			}
		}

		item.curse();
	}
}
