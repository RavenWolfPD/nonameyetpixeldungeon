/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.levels.painters;

import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.VoidTendril;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs.Wandmaker;
import com.ravenwolf.nonameyetpixeldungeon.items.Generator;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.items.armours.Armour;
import com.ravenwolf.nonameyetpixeldungeon.items.armours.body.BodyArmor;
import com.ravenwolf.nonameyetpixeldungeon.items.keys.IronKey;
import com.ravenwolf.nonameyetpixeldungeon.items.misc.Gold;
import com.ravenwolf.nonameyetpixeldungeon.items.potions.PotionOfLiquidFlame;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.CorpseDust;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.SpellPrism;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.VoidStone;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Room;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.AlarmTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.GrippingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ShockingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.SpearsTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.TeleportationTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ToxicTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.Trap;
import com.watabou.utils.Point;
import com.watabou.utils.Random;

public class PrisonQuestPainter extends Painter {

	public static void paint( Level level, Room room ) {

		switch (Wandmaker.Quest.type) {
			case 0:
			default:
				paintCorpseRoom(level, room);
				break;
			case 1:
				paintShadowRoom(level, room);
				break;
			case 2:
				paintTrapsRoom(level, room);
				break;
		}

	}

	private static void paintShadowRoom( Level level, Room room){
		fill( level, room, Terrain.WALL );
		fill(level, room, 1, Terrain.EMPTY_SP);

		Room.Door entrance = room.entrance();

		Point pot = null;
		if (entrance.x == room.left) {
			pot = new Point( room.right-1, Random.Int( 2 ) == 0 ? room.top + 1 : room.bottom - 1 );
		} else if (entrance.x == room.right) {
			pot = new Point( room.left+1, Random.Int( 2 ) == 0 ? room.top + 1 : room.bottom - 1 );
		} else if (entrance.y == room.top) {
			pot = new Point( Random.Int( 2 ) == 0 ? room.left + 1 : room.right - 1, room.bottom-1 );
		} else if (entrance.y == room.bottom) {
			pot = new Point( Random.Int( 2 ) == 0 ? room.left + 1 : room.right - 1, room.top+1 );
		}
		set( level, pot, Terrain.PEDESTAL );

		level.drop( new VoidStone(), pot.x + pot.y * Level.WIDTH );

		int itemPos;
		do {
			itemPos = room.random();
		} while ( level.map[itemPos] != Terrain.EMPTY_SP);

		Armour armor =null;
		while (!(armor instanceof BodyArmor) ){
			armor = (Armour) Generator.random( Generator.Category.ARMOR );
		}
		Heap heap =level.drop(armor, itemPos);
		heap.type = Heap.Type.BONES;

		for (int i=0; i < 5; i++) {
			int pos;
			do {
				pos = room.random();
			} while ( level.map[pos] == Terrain.PEDESTAL || Actor.findChar(pos) != null);

			VoidTendril lash = new VoidTendril();
			lash.pos = pos;
			lash.state = lash.PASSIVE;
			level.mobs.add(lash);
			Actor.occupyCell(lash);
		}

		entrance.set( Room.Door.Type.LOCKED );
		level.addItemToSpawn( new IronKey() );
	}


	private static void paintTrapsRoom( Level level, Room room){
		fill( level, room, Terrain.WALL );
		//use inactive trap to avoid decorations (water, plants, special tiles) and regular traps
		// on spaces without traps
		fill(level, room, 1, Terrain.INACTIVE_TRAP);

		Room.Door door = room.entrance();

		if (door.x == room.left) {
			set( level, room.right-1 + (room.top+1) *Level.WIDTH, Terrain.STATUE );
			set( level, room.right-1 + (room.bottom-1) *Level.WIDTH, Terrain.STATUE );
			set( level, room.left+1 + door.y *Level.WIDTH, Terrain.EMPTY_DECO );
		} else if (door.x == room.right) {
			set( level, room.left+1 + (room.top + 1) *Level.WIDTH, Terrain.STATUE );
			set( level, room.left+1 + (room.bottom - 1) *Level.WIDTH, Terrain.STATUE );
			set( level, room.right-1 + door.y *Level.WIDTH, Terrain.EMPTY_DECO );
		} else if (door.y == room.top) {
			set( level, room.right-1 + (room.bottom-1) *Level.WIDTH, Terrain.STATUE );
			set( level, room.left+1 + (room.bottom-1) *Level.WIDTH, Terrain.STATUE );
			set( level, door.x + (room.top+1) *Level.WIDTH, Terrain.EMPTY_DECO );
		} else if (door.y == room.bottom) {
			set( level, room.right-1 + (room.top+1) *Level.WIDTH, Terrain.STATUE );
			set( level, room.left+1 + (room.top+1) *Level.WIDTH, Terrain.STATUE );
			set( level, door.x + (room.bottom-1) *Level.WIDTH, Terrain.EMPTY_DECO );
		}

		Room.Door entrance = room.entrance();

		Heap heap;
		int doorPos = door.x + door.y * Level.WIDTH;
		for (int i=0; i < 5; i++) {
			int pos;
			do {
				pos = room.random();
			} while ( level.map[pos] != Terrain.INACTIVE_TRAP
			|| Level.distance(doorPos, pos) <4);
			if (i == 0) {
				heap = level.drop(new SpellPrism(), pos);
				heap.type = Heap.Type.CHEST;
				//temporarily set it as trapped so is no longer eligible for traps
				level.map[pos] = Terrain.SECRET_TRAP;
				Trap trap =new AlarmTrap();
				level.setTrap(trap, pos);
				trap.canBeSearched = false;
				continue;
			}
			switch (Random.Int(6) ){
				case 0 :
					heap = level.drop(new Gold().random(), pos );
					break;
				default:
					Item gold = new Gold().random();
					gold.quantity(gold.quantity/4);
					heap = level.drop( gold, pos );
			}

			heap.type = Heap.Type.CHEST;
			// two teleportation traps
			// and two toxic gas traps
			Trap trap = null;
			if (i < 3)
				trap =new TeleportationTrap();
			else
				trap =new ToxicTrap();
			level.map[pos] = Terrain.SECRET_TRAP;
			level.setTrap(trap, pos);
			trap.canBeSearched = false;
		}

		// add a few more traps that cant be disarmed or searched
		int ntraps = (room.width()-1)*(room.height()-1) - 8;//empty SP, + 2 statues + 5 bones
		ntraps = ntraps - ntraps/3;

		for (int i=0; i < ntraps; i++) {
			int pos;
			do {
				pos = room.random();
			} while ( level.map[pos] != Terrain.INACTIVE_TRAP);

			Trap trap;
			switch (Random.Int(7) ){
				case 0 :
					trap =new GrippingTrap();
					break;
				case 1 :
				case 2 :
					trap =new ShockingTrap();
					break;
				default:
					trap =new SpearsTrap();
			}
			level.map[pos] = Terrain.SECRET_TRAP;
			level.setTrap(trap, pos);
			trap.canBeSearched = false;
			//trap.disarmedByActivation = false;
		}

		entrance.set( Room.Door.Type.LOCKED );
		level.addItemToSpawn( new IronKey() );
	}


	private static void paintCorpseRoom( Level level, Room room){
		fill( level, room, Terrain.WALL );

		fill(level, room, 1, Terrain.EMPTY_SP2);

		Room.Door door = room.entrance();

		Room.Door entrance = room.entrance();

		Heap heap;
		int doorPos = door.x + door.y * Level.WIDTH;
		for (int i=0; i < 4; i++) {
			int pos;
			do {
				pos = room.random();
			} while (  level.heaps.get( pos ) != null
					|| Level.distance(doorPos, pos) <4);
			if (i == 0) {
				heap = level.drop(new CorpseDust(), pos);
				heap.type = Heap.Type.BONES_CURSED;
				continue;
			}
			switch (Random.Int(6) ){
				case 0 :
					heap = level.drop(Generator.random(Generator.Category.THROWING), pos );
					break;
				case 1 :
				case 2 :
					heap = level.drop(Generator.random(Generator.Category.MISC), pos );
					break;
				default:
					Item gold = new Gold().random();
					gold.quantity(gold.quantity/5);
					heap = level.drop( gold, pos );
			}
			heap.type = Heap.Type.BONES;
		}

		entrance.set( Room.Door.Type.BARRICADE );
		level.addItemToSpawn( new PotionOfLiquidFlame() );
	}

}
