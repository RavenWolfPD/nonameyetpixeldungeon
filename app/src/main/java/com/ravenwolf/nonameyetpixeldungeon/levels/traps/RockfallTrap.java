/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Dazed;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.watabou.noosa.Camera;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Random;


public class RockfallTrap extends Trap {

	{
		color = GREY;
		shape = DIAMOND;
		
		canBeHidden = false;
		avoidsHallways = true;
	}

    public String name(){
        return "Rockfall trap";
    }

    public String description() {
        return "This trap is connected to a series of loose rocks above, triggering it will cause them to come crashing down over a large area!";
    }
	
	@Override
	public void activate() {

        Sample.INSTANCE.play(Assets.SND_ROCKS);
        Camera.main.shake(3, 0.07f * 5);

        int power = 10 + Dungeon.chapter() * 6;

        for (int n : Level.NEIGHBOURS9) {
            boulders(pos + n, power);
        }

        for (int n : Level.NEIGHBOURS16) {
            if ( Random.Float() < 0.6f) {
                boulders(pos + n, power);
            }
        }
    }


    private void boulders( int pos, int power ) {

        if( pos < 0 || pos >= Level.LENGTH)
            return;

        if( Level.solid[pos] )
            return;

        if (Dungeon.level.traps.get(pos) != this)
            Dungeon.level.press(pos , null);

        Char ch = Actor.findChar(pos);
        if (ch != null) {

            int dmg = ch.absorb( Random.IntRange( power / 2 , power ));

            ch.damage(dmg, this, Element.PHYSICAL);

            if (ch.isAlive() ) {
                BuffActive.addFromDamage(ch, Dazed.class, dmg*2);
            }
        }

        Heap heap = Dungeon.level.heaps.get(pos);
        if (heap != null) {
            heap.shatter( "Trap" );
        }

        if (Dungeon.visible[pos]) {
            CellEmitter.get(pos).start(Speck.factory(Speck.ROCK), 0.1f, 4 );
        }
    }

}
