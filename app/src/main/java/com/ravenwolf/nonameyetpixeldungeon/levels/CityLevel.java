/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.levels;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.DungeonTilemap;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs.AmbitiousImp;
import com.ravenwolf.nonameyetpixeldungeon.levels.Room.Type;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.SummoningTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.BlazingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ConfusionTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.CursingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.DisintegrationTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.DisruptionFieldTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.FlashingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.GuardianTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.IceBarrierTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.RockfallTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.VoidTendrilsTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ShockingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.TeleportationTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.ToxicTrap;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.watabou.noosa.Scene;
import com.watabou.noosa.particles.Emitter;
import com.watabou.noosa.particles.PixelParticle;
import com.watabou.utils.PointF;
import com.watabou.utils.Random;

public class CityLevel extends RegularLevel {

	{
		color1 = 0x4b6636;
		color2 = 0xf2f2f2;

//        viewDistance = 5;
	}
	
	@Override
	public String tilesTex() {
		return Assets.TILES_CITY;
	}
	
	@Override
	public String waterTex() {
		return Assets.WATER_CITY;
	}

    protected boolean[] water() {
        return Patch.generate( feeling == Feeling.WATER ? 0.65f : 0.45f, 4 );
    }

    protected boolean[] grass() {
        return Patch.generate( feeling == Feeling.GRASS ? 0.60f : 0.40f, 3 );
    }

	@Override
	protected Class<?>[] trapClasses() {
		return new Class<?>[]{
				DisintegrationTrap.class, DisruptionFieldTrap.class, BlazingTrap.class, IceBarrierTrap.class, ToxicTrap.class,
				GuardianTrap.class, ShockingTrap.class, FlashingTrap.class, RockfallTrap.class, VoidTendrilsTrap.class,
				CursingTrap.class, ConfusionTrap.class, SummoningTrap.class, TeleportationTrap.class };
	}

	@Override
	protected float[] trapChances() {
		return new float[]{
				6, 4, 4, 4, 4,
				2, 2, 2, 2, 2,
				1, 1, 1, 1};
	}
	
	@Override
	protected void assignRoomType() {
		super.assignRoomType();
		
		for (Room r : rooms) {
			if (r.type == Type.TUNNEL) {
				r.type = Type.PASSAGE;
			}
		}
	}
	
	@Override
	protected void decorate() {
		
		for (int i=0; i < LENGTH - WIDTH; i++) {
			if (map[i] == Terrain.EMPTY && Random.Int( 10 ) == 0) { 
				map[i] = Terrain.EMPTY_DECO;
			} else if (map[i] == Terrain.WALL && !Terrain.isWall(map[i + WIDTH]) &&
					// no door below
					!Terrain.isAnyDoor(map[i + WIDTH]) && Random.Int( 6 ) == 0) {
				map[i] = Terrain.WALL_DECO;
			}
		}
	}
	
	@Override
	protected void createItems() {
		super.createItems();
		
		AmbitiousImp.Quest.spawn(this, roomEntrance);
	}

    @Override
    public String tileName( int tile ) {
        return CityLevel.tileNames(tile);
    }

    @Override
    public String tileDesc( int tile ) {
        return CityLevel.tileDescs(tile);
    }

//	@Override
	public static String tileNames( int tile ) {
		switch (tile) {
		case Terrain.WATER:
			return "Suspiciously colored water";
		case Terrain.HIGH_GRASS:
			return "High blooming flowers";
		default:
			return Level.tileNames(tile);
		}
	}
	
//	@Override
	public static String tileDescs(int tile) {
		switch (tile) {
		case Terrain.ENTRANCE:
			return "A ramp leads up to the upper depth.";
		case Terrain.EXIT:
			return "A ramp leads down to the lower depth.";
		case Terrain.WALL_DECO:
		case Terrain.EMPTY_DECO:
			return "Several tiles are missing here.";
		case Terrain.EMPTY_SP:
			return "Thick carpet covers the floor.";
		case Terrain.STATUE:
		case Terrain.STATUE_SP:
			return "The statue depicts some dwarf standing in a heroic stance.";
		case Terrain.BOOKSHELF:
            return "The rows of books on different disciplines fill the bookshelf. Maybe, there would be something useful in here?";
        case Terrain.SHELF_EMPTY:
            return "The rows of books on different disciplines fill the bookshelf.";
		default:
			return Level.tileDescs(tile);
		}
	}
	
	@Override
	public void addVisuals( Scene scene ) {
		super.addVisuals( scene );
		addVisuals( this, scene );
	}
	
	public static void addVisuals( Level level, Scene scene ) {
		for (int i=0; i < LENGTH; i++) {
			if (level.map[i] == Terrain.WALL_DECO) {
				scene.add( new Smoke( i ) );
			}
		}
	}
	
	public static class Smoke extends Emitter {
		
		private int pos;
		
		private static final Emitter.Factory factory = new Factory() {
			
			@Override
			public void emit( Emitter emitter, int index, float x, float y ) {
				SmokeParticle p = (SmokeParticle)emitter.recycle( SmokeParticle.class );
				p.reset( x, y );
			}
		};
		
		public Smoke( int pos ) {
			super();
			
			this.pos = pos;
			
			PointF p = DungeonTilemap.tileCenterToWorld( pos );
			pos( p.x - 4, p.y + Dungeon.isometricOffset() - 2, 4, 0 );
			
			pour( factory, 0.2f );
		}
		
		@Override
		public void update() {
			//Fix for imp shop
			if (visible = Dungeon.visible[pos] && pos+WIDTH < LENGTH && Dungeon.visible[pos+WIDTH]) {
				super.update();
			}
		}
	}
	
	public static final class SmokeParticle extends PixelParticle {
		
		public SmokeParticle() {
			super();
			
			color( 0x000000 );
			speed.set( Random.Float( 8 ), -Random.Float( 8 ) );
		}
		
		public void reset( float x, float y ) {
			revive();
			
			this.x = x;
			this.y = y;
			
			left = lifespan = 2f;
		}
		
		@Override
		public void update() {
			super.update();
			float p = left / lifespan;
			am = p > 0.8f ? 1 - p : p * 0.25f;
			size( 8 - p * 4 );
		}
	}
}