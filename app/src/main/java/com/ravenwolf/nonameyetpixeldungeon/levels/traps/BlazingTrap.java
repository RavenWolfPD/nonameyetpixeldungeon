/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Fire;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.FlameParticle;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Random;

public class BlazingTrap extends Trap {

	{
		color = ORANGE;
		shape = DOTS;
	}


    public String name(){
        return "Blazing trap";
    }

    public String description() {
        return "Stepping on this trap will ignite a chemical mixture,setting the surrounding area aflame.";
    }

	@Override
	public void activate() {

	    boolean sighted = false;

        for (int n : Level.NEIGHBOURS9) {
            if (!Dungeon.level.solid[pos + n]) {
                GameScene.add(Blob.seed(pos + n, Random.IntRange(2, 3), Fire.class));
                CellEmitter.get(pos + n).burst(FlameParticle.FACTORY, 5);
                sighted= Dungeon.visible[pos +n];
            }
        }
        if (sighted)
            Sample.INSTANCE.play(Assets.SND_BURNING);
	}
}
