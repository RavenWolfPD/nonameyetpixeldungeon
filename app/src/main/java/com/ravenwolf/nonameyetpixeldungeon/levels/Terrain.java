/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.levels;

public class Terrain {


	public static final int EMPTY			= 0;
	public static final int EMPTY_DECO		= 1;
	public static final int EMPTY_SP		= 2;
	public static final int EMPTY_SP2 		= 3;

	public static final int HIGH_GRASS		= 9;
	public static final int GRASS			= 10;
	public static final int EMBERS			= 11;
	public static final int EMBERS_DOOR		= 12;
	public static final int TRAP			= 13;
	public static final int SECRET_TRAP		= 14;
	public static final int INACTIVE_TRAP	= 15;

	public static final int WALL			= 16;
	public static final int DOOR_ILLUSORY   = 17;
	public static final int WALL_DECO		= 18;
	public static final int WALL_SIGN   	= 19;

	public static final int CHASM			= 25;
	public static final int CHASM_FLOOR		= 26;
	public static final int CHASM_FLOOR_SP	= 27;
	public static final int CHASM_WALL		= 28;
	public static final int CHASM_WATER		= 29;

	public static final int PEDESTAL		= 32;
	public static final int EMPTY_WELL		= 33;
	public static final int WELL			= 34;
	public static final int STATUE			= 35;
	public static final int STATUE_SP		= 36;
	public static final int ALCHEMY			= 37;
	public static final int BARRICADE		= 38;
	public static final int BOOKSHELF		= 39;
	public static final int SHELF_EMPTY 	= 40;
	public static final int SIGN			= 42;

	public static final int DOOR_CLOSED 	= 48;
	public static final int OPEN_DOOR		= 49;
	public static final int LOCKED_DOOR		= 50;

	public static final int LOCKED_EXIT		= 53;
	public static final int UNLOCKED_EXIT	= 54;
	public static final int ENTRANCE		= 55;
	public static final int EXIT			= 56;

	public static final int STATUE2_SP 		= 43;
	public static final int STATUE2			= 44;
	public static final int WALL_GRATE		= 59;

	public static final int WATER_TILES     = 64;
	public static final int WATER		    = 79;

	//FIXME only used on Madusa level. move it to a hazard?
	public static final int STATUE_FROG = 45;
	public static final int STATUE_SHAMAN = 46;
	public static final int STATUE_BRUTE = 47;

	public static final int FLOOR_VARIATION_OFFSET 		= 4;

	/*  ISOMETRIC visual tiles */

	public static final int WALL_ISO			= 80; //+2 for variations
	public static final int WALL_DECO_ISO		= 83; //+1 for variations
	public static final int WALL_SIGN_ISO		= 85;
	public static final int BOOKSHELF_ISO		= 86;
	public static final int BOOKSHELF_EMPTY_ISO	= 87;

	public static final int CLOSED_DOOR_ISO_H		= 96;
	public static final int OPEN_DOOR_ISO_H			= 97;
	public static final int LOCKED_DOOR_ISO_H		= 98;

	public static final int WALL_DOOR_ISO_V 		= 99;
	public static final int CLOSED_DOOR_ISO_V		= 100;
	public static final int OPEN_DOOR_ISO_V			= 101;
	public static final int LOCKED_DOOR_ISO_V		= 102;

	public static final int EMBERS_DOOR_ISO_H		= 103;
	public static final int EMBERS_DOOR_ISO_V		= 104;
	public static final int BARRICADE_ISO 			= 105;

	public static final int UNLOCKED_EXIT_ISO 		= 106;
	public static final int LOCKED_EXIT_ISO 		= 107;
	public static final int WALL_GRATE_WATER_ISO 	= 108;
	public static final int CHASM_WALL_FLAT			= 109;

	public static final int RAISED_WALL_FLAT 		= 112;
	public static final int RAISED_WALL				= 113;
	public static final int RAISED_WALL_DOOR_V		= 114;
	public static final int RAISED_CLOSED_DOOR_H 	= 115;
	public static final int RAISED_OPEN_DOOR_H 		= 116;
	public static final int RAISED_CLOSED_DOOR_V 	= 117;
	public static final int RAISED_LOCKED_DOOR_V 	= 118;
	public static final int RAISED_BARRICADE		= 119;
	public static final int RAISED_EMPTY			= 120;
	public static final int RAISED_EXIT				= 122;
	public static final int RAISED_HIGH_GRASS 		= 123;

	
	public static final int PASSABLE		= 0x01;
	public static final int LOS_BLOCKING	= 0x02;
	public static final int FLAMMABLE       = 0x04;
    public static final int TRAPPED         = 0x08;
    public static final int SOLID			= 0x10;
    public static final int AVOID			= 0x20;
    public static final int LIQUID			= 0x40;
    public static final int PIT				= 0x80;

    public static final int UNSTITCHABLE	= 0x100;
    public static final int IMPORTANT = 0x200;
    public static final int ILLUSORY = 0x400;

	public static final int[] flags = new int[256];
    static {
		flags[CHASM]		= AVOID	| PIT									| UNSTITCHABLE;
		flags[EMPTY]		= PASSABLE;
		flags[GRASS]		= PASSABLE | FLAMMABLE;
		flags[EMPTY_WELL]	= AVOID | IMPORTANT;
		flags[WATER]		= PASSABLE | LIQUID | UNSTITCHABLE;
		flags[WALL]			= LOS_BLOCKING | SOLID 							| UNSTITCHABLE;
		flags[DOOR_CLOSED]	= PASSABLE | LOS_BLOCKING | FLAMMABLE | SOLID	/*| UNSTITCHABLE*/;
		flags[OPEN_DOOR]	= PASSABLE | FLAMMABLE | IMPORTANT  /*| UNSTITCHABLE*/ ;
		flags[ENTRANCE]		= PASSABLE | IMPORTANT;
		flags[EXIT]			= PASSABLE | IMPORTANT;
		flags[EMBERS]		=  flags[EMBERS_DOOR]		= PASSABLE;
		flags[LOCKED_DOOR]	= LOS_BLOCKING | SOLID 							| UNSTITCHABLE;
        flags[PEDESTAL]		= PASSABLE | IMPORTANT | UNSTITCHABLE;
		flags[WALL_DECO]	= flags[WALL];
		flags[BARRICADE]	= FLAMMABLE | SOLID | LOS_BLOCKING;
		flags[EMPTY_SP]		= flags[EMPTY]									| UNSTITCHABLE;
		flags[HIGH_GRASS]	= PASSABLE | LOS_BLOCKING | FLAMMABLE;
		flags[EMPTY_DECO]	= flags[EMPTY];
		flags[LOCKED_EXIT]	= SOLID;
		flags[UNLOCKED_EXIT]= PASSABLE | IMPORTANT;
		flags[SIGN]			= PASSABLE | FLAMMABLE | IMPORTANT;
		flags[WALL_SIGN]	= flags[WALL];
		flags[WELL]			= AVOID | IMPORTANT;
		flags[STATUE]		= SOLID;
		flags[STATUE_SP]	= flags[STATUE] 								| UNSTITCHABLE;
		flags[STATUE2_SP]	= flags[STATUE_SP];
		flags[WALL_GRATE]	= flags[STATUE_SP];
		flags[BOOKSHELF]	= flags[BARRICADE]								/*| UNSTITCHABLE*/;
		flags[SHELF_EMPTY]  = flags[BOOKSHELF];
		flags[ALCHEMY]		= AVOID | PASSABLE | IMPORTANT;

		flags[STATUE2]	= flags[STATUE];
		flags[STATUE_FROG]	= flags[STATUE];
		flags[STATUE_BRUTE]	= flags[STATUE];
		flags[STATUE_SHAMAN]	= flags[STATUE];
		flags[EMPTY_SP2]	= flags[EMPTY];

		flags[CHASM_WALL]		= flags[CHASM];
		flags[CHASM_FLOOR]		= flags[CHASM];
		flags[CHASM_FLOOR_SP]	= flags[CHASM];
		flags[CHASM_WATER]		= flags[CHASM];

		flags[DOOR_ILLUSORY]			= LOS_BLOCKING | FLAMMABLE | SOLID | UNSTITCHABLE | ILLUSORY;
		flags[TRAP]				= AVOID;
		flags[SECRET_TRAP]		= flags[EMPTY] | TRAPPED;
		flags[INACTIVE_TRAP]			= flags[EMPTY];
		
		for (int i=WATER_TILES; i < WATER_TILES + 16; i++) {
			flags[i] = flags[WATER];
		}
	}

    public static int discover( int terr ) {
		switch (terr) {
		case DOOR_ILLUSORY:
			return DOOR_CLOSED;
		case SECRET_TRAP:
			return TRAP;
		default:
			return terr;
		}
	}

	public static boolean isRealWall( int terrain ) {
		return terrain == WALL || terrain == WALL_DECO || terrain == WALL_SIGN;
	}

	public static boolean isWall( int terrain ) {
		return terrain == WALL || terrain == WALL_DECO || terrain == WALL_SIGN
				|| terrain == WALL_GRATE || terrain == DOOR_ILLUSORY
				|| terrain == LOCKED_EXIT || terrain == UNLOCKED_EXIT; //exits count as walls
	}

	public static boolean isOpenDoor( int terrain ) {
    	return terrain == OPEN_DOOR;
	}

	public static boolean isClosedDoor( int terrain ) {
		return terrain == DOOR_CLOSED;
	}

	public static boolean isLockedDoor( int terrain ) {
		return terrain == LOCKED_DOOR;
	}

	public static boolean isAnyDoor( int terrain ) {
		return isOpenDoor(terrain) || isClosedDoor(terrain) || isLockedDoor(terrain) ;
	}

	public static boolean isChapterExit( int terrain ) {
		return terrain == LOCKED_EXIT || terrain == UNLOCKED_EXIT ;
	}

	public static int getEmberTile( int oldTerrain ) {
		if (isAnyDoor(oldTerrain))
			return EMBERS_DOOR;
		else
			return EMBERS;
	}

}
