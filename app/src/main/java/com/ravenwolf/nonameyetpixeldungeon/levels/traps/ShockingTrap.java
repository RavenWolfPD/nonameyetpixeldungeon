/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Electricity;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;

public class ShockingTrap extends Trap {

	{
		color = YELLOW;
		shape = DOTS;
	}

    public String name(){
        return "Shocking trap";
    }

    public String description() {
        return "A mechanism with a large amount of energy stored into it. Triggering this trap will discharge that energy into a field around it.";
    }

	@Override
	public void activate() {

        Blob blob = Blob.seed( pos, 100, Electricity.class );
        GameScene.add( blob );
	}



}
