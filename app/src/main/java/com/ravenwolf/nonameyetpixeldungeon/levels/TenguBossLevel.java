/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.levels;

import com.ravenwolf.nonameyetpixeldungeon.Bones;
import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Mob;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Tengu;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.items.keys.IronKey;
import com.ravenwolf.nonameyetpixeldungeon.items.keys.SkeletonKey;
import com.ravenwolf.nonameyetpixeldungeon.levels.Room.Type;
import com.ravenwolf.nonameyetpixeldungeon.levels.features.Door;
import com.ravenwolf.nonameyetpixeldungeon.levels.painters.Painter;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.GrippingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.Trap;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.watabou.noosa.Scene;
import com.watabou.noosa.audio.Music;
import com.watabou.utils.Bundle;
import com.watabou.utils.Point;
import com.watabou.utils.Random;
import com.watabou.utils.Rect;

public class TenguBossLevel extends RegularLevel {

    private static final int BOSS_ISHIDDEN = 0;
    private static final int BOSS_APPEARED = 1;
    private static final int BOSS_DEFEATED = 2;


	private static final int ENTRANCE_TOP = 17;
	private static final int ENTRANCE_BOTTOM = ENTRANCE_TOP + 7;


	private static final int ARENA_TOP		= 2;
	private static final int ARENA_BOTTOM	= ARENA_TOP +7;
	private static final int ARENA_LEFT = WIDTH / 2 - 4;
	private static final int ARENA_RIGHT = WIDTH / 2 + 4;

    private int progress = 0;

	private Room anteroom;
	private int arenaDoor;
	

	@Override
	protected boolean build() {

		int midColumn = WIDTH / 2;

		roomEntrance = (Room)new Room().set( new Rect(ARENA_LEFT-2, ENTRANCE_TOP, ARENA_RIGHT+2, ENTRANCE_BOTTOM));
		rooms.add( roomEntrance );
		roomEntrance.type = Type.STANDARD;


		anteroom= (Room)new Room().set( new Rect( ARENA_LEFT +1,ARENA_BOTTOM , ARENA_RIGHT -1, ENTRANCE_TOP));
		rooms.add( anteroom );
		anteroom.type = Type.STANDARD;


		//roomExit is tengu arena, but actual exit is on entrance room
		roomExit = (Room)new Room().set( new Rect( ARENA_LEFT, ARENA_TOP, ARENA_RIGHT, ARENA_BOTTOM));
		rooms.add( roomExit );
		roomExit.type = Type.STANDARD;

		arenaDoor=ARENA_BOTTOM * Level.WIDTH +  midColumn;


		paint();

		//Exit
		Painter.fill(this,
				midColumn - 2,
				ENTRANCE_TOP + 3,
				5,
				2,
				Terrain.WALL);
		exit = (ENTRANCE_TOP+4) * Level.WIDTH + midColumn;
		Painter.set( this, exit, Terrain.LOCKED_EXIT );
		Painter.set( this, exit+2, Terrain.WALL_DECO );
		Painter.set( this, exit-2, Terrain.WALL_DECO );

		Painter.fill(this,
				midColumn-1,
				ENTRANCE_TOP +5,
				3,
				1,
				Terrain.EMPTY_SP);

		Painter.set( this, (ENTRANCE_TOP + 3) * Level.WIDTH +  ARENA_LEFT , Terrain.STATUE );
		Painter.set( this, (ENTRANCE_TOP + 3) * Level.WIDTH +  ARENA_RIGHT , Terrain.STATUE );

		//entrnace
		Painter.fill(this,
				midColumn - 1,
				ENTRANCE_BOTTOM,
				3,
				2,
				Terrain.EMPTY);

		entrance = (ENTRANCE_BOTTOM+1) * Level.WIDTH + midColumn;
		Painter.set( this, entrance, Terrain.ENTRANCE );


		//round borders of main room
		Painter.set( this, (ENTRANCE_TOP+1) * Level.WIDTH +  ARENA_LEFT - 1, Terrain.WALL_DECO );
		Painter.set( this, (ENTRANCE_TOP+1) * Level.WIDTH +  ARENA_RIGHT + 1, Terrain.WALL_DECO );
		Painter.set( this, (ENTRANCE_BOTTOM-1) * Level.WIDTH +  ARENA_LEFT - 1, Terrain.WALL );
		Painter.set( this, (ENTRANCE_BOTTOM-1) * Level.WIDTH +  ARENA_RIGHT + 1, Terrain.WALL );


		//hallway entrance
		Painter.set( this, ENTRANCE_TOP * Level.WIDTH +  midColumn, Terrain.DOOR_CLOSED );
		Painter.set( this, ENTRANCE_TOP * Level.WIDTH +  midColumn + 1, Terrain.WALL_DECO );
		Painter.set( this, ENTRANCE_TOP * Level.WIDTH +  midColumn - 1, Terrain.WALL_DECO );
		Painter.set( this, ENTRANCE_TOP * Level.WIDTH +  midColumn + 2, Terrain.WALL_GRATE);
		Painter.set( this, ENTRANCE_TOP * Level.WIDTH +  midColumn - 2, Terrain.WALL_GRATE);


		Painter.set( this, (ARENA_BOTTOM +4) * Level.WIDTH +  midColumn + 2, Terrain.WALL_DECO );
		Painter.set( this, (ARENA_BOTTOM +4) * Level.WIDTH +  midColumn - 2, Terrain.WALL_DECO );


		for (int i=ARENA_BOTTOM +1; i<ARENA_BOTTOM+4; i++){
			Painter.set( this, i * Level.WIDTH + midColumn + 2, Terrain.WALL );
			Painter.set( this, i * Level.WIDTH + midColumn - 2, Terrain.WALL );
		}

		Painter.fill(this,
				anteroom.left + 2,
				anteroom.top + 1,
				anteroom.width() - 3,
				2,
				Terrain.EMPTY_SP);
		Painter.set( this, arenaDoor, Terrain.LOCKED_DOOR );


		return true;
	}

	@Override
	protected void placeTraps() {

	}



	@Override
	protected void decorate() {

		for (int i=WIDTH + 1; i < LENGTH - WIDTH - 1; i++) {
			if (map[i] == Terrain.EMPTY) { 
				
				float c = 0.15f;
				if (map[i + 1] == Terrain.WALL && map[i + WIDTH] == Terrain.WALL) {
					c += 0.2f;
				}
				if (map[i - 1] == Terrain.WALL && map[i + WIDTH] == Terrain.WALL) {
					c += 0.2f;
				}
				if (map[i + 1] == Terrain.WALL && map[i - WIDTH] == Terrain.WALL) {
					c += 0.2f;
				}
				if (map[i - 1] == Terrain.WALL && map[i - WIDTH] == Terrain.WALL) {
					c += 0.2f;
				}
				
				if (Random.Float() < c) {
					map[i] = Terrain.EMPTY_DECO;
				}
			}
		}

		Painter.fill(this,
				roomExit.left + 2,
				roomExit.top + 2,
				roomExit.width() - 3,
				roomExit.height() - 3,
				Terrain.INACTIVE_TRAP);

		for(Point p : roomExit.getPoints()) {
			int cell = pointToCell(p);
			if (map[cell] == Terrain.INACTIVE_TRAP){
				Trap trap = setTrap(new GrippingTrap().reveal(), cell);
				trap.active = false;
			}
		}
	}
	
	@Override
	protected void createMobs() {
	}
	
	public Actor respawner() {
		return null;
	}
	
	@Override
	protected void createItems() {
		int keyPos = anteroom.random();
		while (!passable[keyPos]) {
			keyPos = anteroom.random();
		}
		drop( new IronKey(), keyPos ).type = Heap.Type.CHEST;
		
		Item item = Bones.get();
		if (item != null) {
			int pos;
			do {
				pos = roomEntrance.random();
			} while (pos == entrance || !passable[pos]);
			drop( item, pos ).type = Heap.Type.BONES;
		}
	}

	@Override
	public void press( int cell, Char ch ) {
		
		super.press( cell, ch );
		
		if (ch == Dungeon.hero && progress == BOSS_ISHIDDEN && roomExit.inside( cell )) {

            progress = BOSS_APPEARED;
		
			int pos;
			do {
				pos = roomExit.random();
			} while (pos == cell || Level.distance( pos, ch.pos ) < 3);
			Mob boss = new Tengu();
			boss.state = boss.HUNTING;
			boss.pos = pos;
			GameScene.add( boss, 1f );
			boss.beckon(ch.pos);
			boss.notice();

			press( pos, boss );

			Door.lock(arenaDoor);

            Music.INSTANCE.play( currentTrack(), true );
		}
	}
	
	@Override
	public Heap drop( Item item, int cell ) {
		
		if ( progress == BOSS_APPEARED && item instanceof SkeletonKey) {
            progress = BOSS_DEFEATED;
			set( arenaDoor, Terrain.DOOR_CLOSED);
			GameScene.updateMap( arenaDoor );
			Dungeon.observe();

            Music.INSTANCE.play( currentTrack(), true );
		}
		
		return super.drop( item, cell );
	}
	
	@Override
    public int randomRespawnCell( boolean ignoreTraps, boolean ignoreView ) {

        int cell;

        // FIXME

        if( progress == BOSS_ISHIDDEN ) {
            do {
                cell = Random.Int(LENGTH);
            } while (!mob_passable[cell] || Actor.findChar(cell) != null || roomExit.inside(cell) );
        } else if( progress==BOSS_APPEARED ) {
            do {
                cell = Random.Int(LENGTH);
            } while (!mob_passable[cell] || Actor.findChar(cell) != null || !roomExit.inside(cell) );
        } else {
            do {
                cell = Random.Int(LENGTH);
            } while (!mob_passable[cell] || Actor.findChar(cell) != null );
        }

        return cell;
    }


	@Override
	public String tilesTex() {
		return Assets.TILES_PRISON;
	}

	@Override
	public String waterTex() {
		return Assets.WATER_PRISON;
	}

    @Override
    public String tileName( int tile ) {
        return PrisonLevel.tileNames(tile);
    }

    @Override
    public String tileDesc( int tile ) {
        return PrisonLevel.tileDescs(tile);
    }

	@Override
	public void addVisuals( Scene scene ) {
		PrisonLevel.addVisuals( this, scene );
	}

	protected boolean[] water() {
		return Patch.generate( 0.45f, 5 );
	}

	protected boolean[] grass() {
		return Patch.generate( 0.30f, 4 );
	}

    @Override
    public int nMobs() {
        return 0;
    }

    @Override
    public String currentTrack() {
        return progress == BOSS_APPEARED ? Assets.TRACK_BOSS_LOOP : super.currentTrack();
    }

    private static final String ARENA	    = "arena";
    private static final String DOOR	    = "door";
    private static final String PROGRESS    = "progress";

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle(bundle);
        bundle.put( ARENA, roomExit );
        bundle.put( DOOR, arenaDoor );
        bundle.put( PROGRESS, progress );
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle(bundle);
        roomExit = (Room)bundle.get( ARENA );
        arenaDoor = bundle.getInt( DOOR );
        progress = bundle.getInt( PROGRESS );
    }
}