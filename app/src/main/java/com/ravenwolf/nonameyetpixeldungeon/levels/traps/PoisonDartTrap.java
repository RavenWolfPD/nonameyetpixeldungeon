/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.NoNameYetPixelDungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Poisoned;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.MissileSprite;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Callback;
import com.watabou.utils.Random;

public class PoisonDartTrap extends SideWallTrap {

    {
        color = GREEN;

        canBeHidden = true;
        avoidsHallways = true;
    }

    public String name(){
        return "Poison darts trap";
    }

    public String description() {
        return "This trap will trigger some hidden dart-blowers in the walls, shooting poisoned darts from each sidewall.";
    }


    @Override
    public void activate() {
        shootDarts(wall1, wall2);
    }


    private void shootDarts(final int wall1, final int wall2){

        Actor.add(new Actor() {

            private int shoots;
            @Override
            public int actingPriority(){
                return HERO_ACTING_PRIORITY+1;
            }
            @Override
            protected boolean act() {

                if(Terrain.isRealWall(Dungeon.level.map[wall1])) {
                    shootDart(wall1, wall2);
                }
                if(Terrain.isRealWall(Dungeon.level.map[wall2])) {
                    shootDart(wall2, wall1);
                }
                if (shoots == 0){
                    Actor.remove(this);
                    return true;
                }
                return false;
            }

            private void shootDart(final int from, int to){
                final int target = Ballistica.cast(from, to, false, true);
                final Char ch = Actor.findChar(target);
                final Actor toRemove = this;
                if (Dungeon.visible[from]) {
                    CellEmitter.get(from).burst(Speck.factory(Speck.WOOL), 2);
                    Sample.INSTANCE.play(Assets.SND_MISS, 0.6f, 0.6f, 1.2f);
                }

                if (Dungeon.visible[pos] || Dungeon.visible[from] || Dungeon.visible[target]) {
                    shoots++;
                    ((MissileSprite) NoNameYetPixelDungeon.scene().recycle(MissileSprite.class)).
                            reset(from, target, ItemSpriteSheet.THROWING_DART, new Callback() {
                                @Override
                                public void call() {

                                    if (ch != null) {
                                        hitChar(ch);
                                    }
                                    shoots--;
                                    if(shoots == 0) {
                                        next();
                                        Actor.remove(toRemove);
                                    }
                                }
                            });
                }else if (ch != null) {
                    hitChar(ch);
                }
            }
        });

    }

    private void hitChar(Char ch){
        int power = (4 + Dungeon.chapter() * 4);
        ch.sprite.flash();
        BuffActive.addFromDamage( ch, Poisoned.class, Random.IntRange( power, power * 3/2 ) );
        Sample.INSTANCE.play(Assets.SND_HIT);
    }


}
