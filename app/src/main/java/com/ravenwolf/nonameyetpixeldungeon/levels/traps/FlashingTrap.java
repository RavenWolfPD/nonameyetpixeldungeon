/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */


package com.ravenwolf.nonameyetpixeldungeon.levels.traps;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Bleeding;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Blinded;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Crippled;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Mob;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;

import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Random;

public class FlashingTrap extends Trap {

	{
		color = GREY;
		shape = DOTS;

		avoidsHallways = true;
	}

	public String name(){
		return "Flashing trap";
	}

	public String description() {
		return "On activation, this trap will ignite a potent flashing powder stored within, temporarily blinding, and injuring its victim." +
				"The flash is intense enough to also blind nearby beings.";
	}

	@Override
	public void activate() {

		int power = 10 + Dungeon.chapter() * 5;

		for (int n : Level.NEIGHBOURS9) {
			Char ch = Actor.findChar(pos +n);
			if (ch != null && ch.isAlive()) {
				if (n == 0)
					BuffActive.addFromDamage( ch, Blinded.class, power *3/2);
				BuffActive.addFromDamage( ch, Blinded.class, power );
				int damage = ch.absorb(Random.IntRange(power / 2, power));
				BuffActive.addFromDamage(ch, Bleeding.class, damage * 2);
				//if target is immune to bleeding deal damage instead
				if (ch.buff(Bleeding.class)==null)
					ch.damage(damage, this, Element.PHYSICAL);

				if (ch instanceof Mob) {
					if (((Mob)ch).state == ((Mob)ch).HUNTING) ((Mob)ch).state = ((Mob)ch).WANDERING;
				}
			}
		}

		if (Dungeon.visible[pos]) {
			GameScene.flash(0x80FFFFFF);
			Sample.INSTANCE.play(Assets.SND_RAY,0.5f, 0.5f, 1.5f);
			CellEmitter.get(pos).start( Speck.factory( Speck.LIGHT ), 0.02f, 4  );
		}

	}
}
