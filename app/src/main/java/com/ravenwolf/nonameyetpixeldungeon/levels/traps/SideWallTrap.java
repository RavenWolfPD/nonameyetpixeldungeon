/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.watabou.utils.Bundle;
import com.watabou.utils.Random;

public abstract class SideWallTrap extends Trap {

    protected int wall1;
    protected int wall2;

    @Override
    public void adjustTrap(Level level){

        boolean validHorizontal = false;
        boolean validVertical = false;

        //define trap  orientation (vertical or horizontal)
        int leftWall = Ballistica.cast(pos, pos-1, true, false) -1;
        int rightWall = Ballistica.cast(pos, pos+1, true, false) +1;

        int horizontalDist = Level.distance(leftWall, rightWall);

        if (horizontalDist < 10 && (Terrain.isRealWall(level.map[leftWall]) || Terrain.isRealWall(level.map[rightWall])))
            validHorizontal=true;

        int topWall = Ballistica.cast(pos, pos-Level.WIDTH, true, false)-Level.WIDTH;
        int bottomWall = Ballistica.cast(pos, pos+Level.WIDTH, true, false)+Level.WIDTH;

        int verticalDist = Level.distance(topWall, bottomWall);
        if (verticalDist < 10 && (Terrain.isRealWall(level.map[topWall]) || Terrain.isRealWall(level.map[bottomWall])))
            validVertical=true;

        if (validHorizontal || validVertical){
            if (!validVertical || (validHorizontal && Random.Float() < 0.5f)){
                wall1 = leftWall;
                wall2 = rightWall;
                shape = ARROWS_H;
            }else {
                wall1 = topWall;
                wall2 = bottomWall;
                shape = ARROWS_V;
            }
        } else {
            //remove the trap if not in a valid position
            level.traps.remove(pos);
            level.map[pos] = Terrain.EMPTY;
            level.trapped[pos] = false;
        }
    }


    private static final String SHAPE	= "shape";
    private static final String WALL1	= "wall1";
    private static final String WALL2   = "wall2";

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle(bundle);
        shape = bundle.getInt( SHAPE );
        wall1 = bundle.getInt( WALL1 );
        wall2 = bundle.getInt( WALL2 );
    }

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle(bundle);
        bundle.put( SHAPE, shape );
        bundle.put( WALL1, wall1 );
        bundle.put( WALL2, wall2 );
    }

}
