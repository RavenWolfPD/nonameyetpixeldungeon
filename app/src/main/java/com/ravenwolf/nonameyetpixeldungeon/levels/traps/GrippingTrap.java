/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Bleeding;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Ensnared;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Wound;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Random;

public class GrippingTrap extends Trap {

	{
		color = GREY;
		shape = LINES;

		disarmedByActivation = false;
		avoidsHallways = true;
	}

    public String name(){
        return "Gripping trap";
    }

    public String description() {
        return "This trap latches onto the feet of whoever trigger it, damaging them and holding it in place until wear of." +
                "\n\nDue to its simple nature, this trap can activate many times without breaking.";
    }

	@Override
	public void activate() {

        Char ch = Actor.findChar( pos );

        if (ch != null) {
            Sample.INSTANCE.play(Assets.SND_HIT);
            Wound.hit(ch);
            if (ch.isAlive() && !ch.flying) {
                BuffActive.add(ch, Bleeding.class, 3);
                //if target is immune to bleeding deal damage instead
                if (ch.buff(Bleeding.class)==null) {
                    int power = 6 + Dungeon.chapter() * 4;
                    ch.damage(Random.IntRange(power / 2, power), this, Element.PHYSICAL);
                }
                BuffActive.add(ch, Ensnared.class, 3);
            }

        } else {
            Wound.hit(pos);
        }
    }
}
