/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Dazed;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Mob;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.items.scrolls.ScrollOfPhaseWarp;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfBlink;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.watabou.noosa.audio.Sample;


public class



TeleportationTrap extends Trap {

	{
		color = TEAL;
		shape = DOTS;
	}

    public String name(){
        return "Teleportation trap";
    }

    public String description() {
        return "Whenever this trap is triggered, everything around it will be teleported to random locations on this floor.";
    }

	@Override
	public void activate() {

        int destination;
        if (Dungeon.visible[pos])
            Sample.INSTANCE.play( Assets.SND_TELEPORT );
        for (int i : Level.NEIGHBOURS9) {
            boolean warped = false;
            int pos = this.pos + i;
            Char ch = Actor.findChar(pos);
            if (ch != null) {
                do  {
                    destination = Dungeon.level.randomRespawnCell(true, true);
                } while (destination == -1);
                CharmOfBlink.appear(ch, destination);
                Dungeon.level.press(destination, ch);
                Dungeon.observe();
                BuffActive.add(ch, Dazed.class, 6);
                warped= true;

            }
            if (ch instanceof Mob && ((Mob) ch).state == ((Mob) ch).HUNTING) {
                ((Mob) ch).state = ((Mob) ch).WANDERING;
            }

            Heap heap = Dungeon.level.heaps.get(pos);
            if (heap != null && heap.type == Heap.Type.HEAP) {
                int cell;
                do  {
                    cell = Dungeon.level.randomRespawnCell(false, true);
                } while (cell == -1);

                Item item = heap.pickUp();
                Dungeon.level.drop(item, cell);

                warped= true;
            }

            if (warped && Dungeon.visible[pos]) {
                CellEmitter.get(pos).start( Speck.factory( Speck.LIGHT ), 0.2f, 3  );
            }
        }
    }

}
