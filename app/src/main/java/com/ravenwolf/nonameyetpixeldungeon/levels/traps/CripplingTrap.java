/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Burning;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Chilled;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Crippled;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Frozen;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Mob;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Wound;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.IceBlockSprite;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Random;

public class CripplingTrap extends SideWallTrap {

    {
        color = RED;

        canBeHidden = true;
        avoidsHallways = true;
    }

    public String name(){
        return "Ice barrier trap";
    }

    public String description() {
        return "When activated, this trap will send out waves of a powerful chemical " +
                "that will freeze everything in its path, creating a solid barrier of ice.";
    }


    @Override
    public void activate() {
        spawnWall(wall1, wall2);
    }


    private void spawnWall(int from, int to){

        Ballistica.cast(from, to, false, false);
        for (int i=0; i < Ballistica.distance ; i++) {
            int pos = Ballistica.trace[i];
            Wound.hit(pos);
            Char ch = Actor.findChar( pos );
            if (ch != null )
                hitChar(ch);

        }
    }

    private int power(){
        return 6 + Dungeon.chapter() * 4;
    }

    private void hitChar(Char ch){
        int power =  power();
        BuffActive.addFromDamage( ch, Crippled.class, power );
        Sample.INSTANCE.play(Assets.SND_HIT);
    }



}
