/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.levels;

import com.ravenwolf.nonameyetpixeldungeon.Bones;
import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.YogDzewa;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.items.keys.SkeletonKey;
import com.ravenwolf.nonameyetpixeldungeon.levels.painters.Painter;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.FlameParticle;
import com.watabou.noosa.Scene;
import com.watabou.noosa.audio.Music;
import com.watabou.utils.Bundle;
import com.watabou.utils.Random;

public class YogBossLevel extends Level {
	
	{
		color1 = 0x801500;
		color2 = 0xa68521;

//		viewDistance = 4;
	}

	private static final int CENTER		= WIDTH / 2;

	private static final int ROOM_LEFT		= CENTER - 1;
	private static final int ROOM_RIGHT		= CENTER + 1;
	private static final int ROOM_TOP		= HEIGHT / 2 - 2;
	private static final int ROOM_BOTTOM	= HEIGHT / 2;


	public static final int[] ALTARS =
			{
					CENTER + WIDTH * 5,

					CENTER + WIDTH * 15 + 10,
					CENTER + WIDTH * 15 - 10,

					//CENTER + WIDTH * 25 + 8,
					//CENTER + WIDTH * 25 - 8,
			};
	
	private int stairs = -1;
	private boolean enteredArena = false;
	private boolean keyDropped = false;
	
	@Override
	public String tilesTex() {
		return Assets.TILES_HALLS;
	}
	
	@Override
	public String waterTex() {
		return Assets.WATER_HALLS;
	}
	
	private static final String STAIRS	= "stairs";
	private static final String ENTERED	= "entered";
	private static final String DROPPED	= "dropped";
	
	@Override
	public void storeInBundle( Bundle bundle ) {
		super.storeInBundle( bundle );
		bundle.put( STAIRS, stairs );
		bundle.put( ENTERED, enteredArena );
		bundle.put( DROPPED, keyDropped );
	}
	
	@Override
	public void restoreFromBundle( Bundle bundle ) {
		super.restoreFromBundle(bundle);
		stairs = bundle.getInt( STAIRS );
		enteredArena = bundle.getBoolean( ENTERED );
		keyDropped = bundle.getBoolean( DROPPED );
	}
	
	@Override
	protected boolean build() {

		int laneHeight = HEIGHT/2;

		Painter.fill( this, ROOM_LEFT-3, 2, 9, laneHeight + 2, Terrain.EMPTY );


		Painter.fill( this, 5, 6, 7, laneHeight, Terrain.EMPTY );
		Painter.fill( this, WIDTH - 11, 6, 7, laneHeight, Terrain.EMPTY );
		Painter.fill( this, 3, 10, WIDTH-5, 9, Terrain.EMPTY );


		Painter.fill( this, ROOM_LEFT-5, 5, 13, 1, Terrain.EMPTY );

		Painter.fill( this, ROOM_LEFT-1, ROOM_BOTTOM+4, 5, 1, Terrain.WALL );

		exit = WIDTH / 2 + WIDTH ;

		for (int i=0; i < LENGTH; i++) {

			if (map[i] == Terrain.EMPTY && Random.Int( 16 ) == 0) {
				map[i] = Terrain.WALL_DECO;
			}

		}


		for( int altar : ALTARS) {
			//map[ altar ] = Terrain.EMPTY_SP;
			for (int n :Level.NEIGHBOURS9){
				map[ altar+n ] = Terrain.EMPTY_SP;
			}
		}

		map[exit] = Terrain.LOCKED_EXIT;
		
		Painter.fill( this, ROOM_LEFT - 1, ROOM_TOP - 1, 
			ROOM_RIGHT - ROOM_LEFT + 3, ROOM_BOTTOM - ROOM_TOP + 3, Terrain.WALL );
		Painter.fill( this, ROOM_LEFT, ROOM_TOP, 
			ROOM_RIGHT - ROOM_LEFT + 1, ROOM_BOTTOM - ROOM_TOP + 1, Terrain.EMPTY );
		
		entrance = Random.Int( ROOM_LEFT + 1, ROOM_RIGHT - 1 ) + 
			Random.Int( ROOM_TOP + 1, ROOM_BOTTOM - 1 ) * WIDTH;
		map[entrance] = Terrain.ENTRANCE;
		
		boolean[] patch = Patch.generate( 0.45f, 6 );
		for (int i=0; i < LENGTH; i++) {
			if (map[i] == Terrain.EMPTY && patch[i]) {
				map[i] = Terrain.WATER;
			}
		}
		
		return true;
	}
	
	@Override
	protected void decorate() {

		for (int i=0; i < LENGTH; i++) {

			if (map[i] == Terrain.EMPTY && Random.Int( 10 ) == 0) {
				map[i] = Terrain.EMPTY_DECO;
			}
		}
	}
	
	@Override
	protected void createMobs() {	
	}
	
	public Actor respawner() {
		return null;
	}
	
	@Override
	protected void createItems() {
		Item item = Bones.get();
		if (item != null) {
			int pos;
			do {
				pos = Random.IntRange( ROOM_LEFT, ROOM_RIGHT ) + Random.IntRange( ROOM_TOP + 1, ROOM_BOTTOM ) * WIDTH;
			} while (pos == entrance || map[pos] == Terrain.SIGN);
			drop( item, pos ).type = Heap.Type.BONES;
		}
	}
	
	@Override
    public int randomRespawnCell( boolean ignoreTraps, boolean ignoreView ) {

        int cell;

        if( !enteredArena ) {
            do {
                cell = super.randomRespawnCell( ignoreTraps, ignoreView );
            } while ( !outsideEntranceRoom(cell) );
        } else {

            cell = super.randomRespawnCell( ignoreTraps, ignoreView );

        }

        return cell;
    }
	
	@Override
	public void press( int cell, Char hero ) {
		
		super.press( cell, hero );
		
		if (!enteredArena && hero == Dungeon.hero && cell != entrance) {
			
			enteredArena = true;
			
			for (int i=ROOM_LEFT-1; i <= ROOM_RIGHT + 1; i++) {
				doMagic( (ROOM_TOP - 1) * WIDTH + i );
				doMagic( (ROOM_BOTTOM + 1) * WIDTH + i );
			}

			for (int i=ROOM_TOP; i < ROOM_BOTTOM + 1; i++) {
				doMagic( i * WIDTH + ROOM_LEFT - 1 );
				doMagic( i * WIDTH + ROOM_RIGHT + 1 );
			}

			doMagic( entrance );
			GameScene.updateMap();

			Dungeon.observe();

			YogDzewa boss = new YogDzewa();
			boss.pos =ALTARS[0];
			GameScene.add( boss );

			stairs = entrance;
			entrance = -1;

            Music.INSTANCE.play( currentTrack(), true );
		}
	}
	
	private void doMagic( int cell ) {
		set( cell, Terrain.EMPTY_SP );
		CellEmitter.get( cell ).start(FlameParticle.FACTORY, 0.1f, 3);
	}
	
	@Override
	public Heap drop( Item item, int cell ) {
		
		if (!keyDropped && item instanceof SkeletonKey) {
			keyDropped = true;
			
			entrance = stairs;
			set( entrance, Terrain.ENTRANCE );
			GameScene.updateMap( entrance );

            Music.INSTANCE.play( currentTrack(), true );
		}
		
		return super.drop( item, cell );
	}

    private boolean outsideEntranceRoom(int cell) {
        int cx = cell % WIDTH;
        int cy = cell / WIDTH;
        return cx < ROOM_LEFT - 1 || cx > ROOM_RIGHT + 1 || cy < ROOM_TOP - 1 || cy > ROOM_BOTTOM + 1;
    }

    @Override
    public String tileName( int tile ) {
        return HallsLevel.tileNames(tile);
    }

    @Override
    public String tileDesc( int tile ) {
        return HallsLevel.tileDescs(tile);
    }

	@Override
	public void addVisuals( Scene scene ) {
		HallsLevel.addVisuals( this, scene );
	}

    public String currentTrack() {
        return enteredArena && !keyDropped ? Assets.TRACK_FINAL_LOOP : super.currentTrack();
    }
}
