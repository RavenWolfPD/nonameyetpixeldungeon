/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Burning;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Chilled;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Frozen;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Mob;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.IceBlockSprite;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Random;

public class IceBarrierTrap extends SideWallTrap {

    {
        color = WHITE;

        canBeHidden = true;
        avoidsHallways = true;
    }

    public String name(){
        return "Ice barrier trap";
    }

    public String description() {
        return "When activated, this trap will send out waves of a powerful chemical " +
                "that will freeze everything in its path, creating a solid barrier of ice.";
    }


    @Override
    public void activate() {
        //CreateWalls(wall1, wall2);
        spawnWall(pos, wall1);
        spawnWall(pos, wall2);
    }

    private void spawnWall(int from, int to){

        Ballistica.cast(from, to, false, false);
        int power =  power();
        for (int i=0; i < Ballistica.distance ; i++) {

            int pos = Ballistica.trace[i];
            Char ch = Actor.findChar( pos );
            if (ch != null && !(ch instanceof IceBlock)) {
                hitChar(ch);
            }else{
                IceBlock.spawnAt(  power, pos );
            }
            Heap heap = Dungeon.level.heaps.get( i );
            if (heap != null) {
                heap.freeze(  power );
            }
        }
    }

/*
    private void CreateWalls(final int wall1, final int wall2){

        Actor.add(new Actor() {

            @Override
            public int actingPriority(){
                return HERO_ACTING_PRIORITY+1;
            }
            @Override
            protected boolean act() {
                spawnWall(pos, wall1);
                spawnWall(pos, wall2);

                Actor.remove(this);
                return true;
            }

            private void spawnWall(final int from, int to){

                   Ballistica.cast(from, to, false, false);
                    int power =  power();
                    for (int i=0; i < Ballistica.distance ; i++) {

                        int pos = Ballistica.trace[i];
                        Char ch = Actor.findChar( pos );
                        if (ch != null && !(ch instanceof IceBlock)) {
                             hitChar(ch);
                        }else{
                            IceBlock.spawnAt(  power, pos );
                        }
                        Heap heap = Dungeon.level.heaps.get( i );
                        if (heap != null) {
                            heap.freeze(  power );
                        }
                    }
            }
        });

    }*/

    private int power(){
        return 6 + Dungeon.chapter() * 4;
    }

    private void hitChar(Char ch){
        int power =  power();
        BuffActive.addFromDamage( ch, Chilled.class, power );
        BuffActive.addFromDamage( ch, Frozen.class, power );
        Sample.INSTANCE.play(Assets.SND_HIT);
    }


    public static class IceBlock extends Mob {

        public IceBlock(){

            name = "ice block";
            spriteClass = IceBlockSprite.class;

            resistances.put( Element.Flame.class, Element.Resist.PARTIAL );
            resistances.put( Element.Shock.class, Element.Resist.PARTIAL);
            resistances.put( Element.Acid.class, Element.Resist.PARTIAL);
            resistances.put( Element.Physical.class, Element.Resist.PARTIAL);

            resistances.put( Element.Frost.class, Element.Resist.IMMUNE );
            resistances.put( Element.Body.class, Element.Resist.IMMUNE );
            resistances.put( Element.Mind.class, Element.Resist.IMMUNE );
            resistances.put( Element.Dispel.class, Element.Resist.IMMUNE );

            hostile = false;
        }

        @Override
        protected boolean canBeBackstabed(){
            return false;
        }

        @Override
        public boolean add( Buff buff ) {
            if (buff instanceof Burning) {
                damage(Random.NormalIntRange(HT / 4, HT / 5), null, Element.FLAME);
            }
            return false;
        }

        @Override
        public boolean mindVision() {
            return false;
        }

        @Override
        public boolean immovable() {
            return true;
        }

        @Override
        public boolean isMagical() {
            return false;
        }

        @Override
        public boolean isSolid() {
            return true;
        }

        @Override
        protected boolean getCloser(int target) {
            return true;
        }

        @Override
        protected boolean getFurther(int target) {
            return true;
        }

        @Override
        public int viewDistance() {
            return 0;
        };

        @Override
        protected boolean act() {

            if( Dungeon.level.feeling != Level.Feeling.PERMAFROST && --HP <= 0 ){
                die( null );
                return true;
            }

            for (int i : Level.NEIGHBOURS8) {
                int cell = pos + i;
                Char c = Actor.findChar(cell);
                if (c != null && !c.moving && !(c instanceof IceBlock)
                && c.buff(Frozen.class) == null ) {
                    Chilled chilled = c.buff(Chilled.class);
                    //prevent stocking to much chill when surrounding
                    if (chilled == null || chilled.getDuration() < 3)
                        BuffActive.add(c, Chilled.class, 1);
                }
            }

            state = PASSIVE;

            return super.act();
        }
        @Override
        public void damage( int dmg, Object src, Element type ) {
            super.damage(dmg, src, type);
            //if it should die next turn due to aging, make the attack kill it
            // to proc the shattered on die effect
            if (HP == 1) {
                die( src );;
            }
        }

        @Override
        public Char chooseEnemy() {
            return null;
        }

        public void adjustStats( int level ) {
            HT = level * 3/2;
            armorClass = 0;

            minDamage = level/2;
            maxDamage = level*2/3;

            accuracy = 0;
            dexterity = 0;
        }

        public static IceBlock spawnAt( int level, int pos ){

            // cannot spawn on walls, chasms or already occupied tiles
            if ( !Level.solid[pos] && !Level.chasm[pos] && Actor.findChar( pos ) == null ){

                IceBlock block = create(Dungeon.level, level, pos );


                GameScene.add( block, TICK );
                Dungeon.level.press( block.pos, block );
                block.sprite.spawn();

                return block;

            } else {

                return null;

            }
        }

        public static IceBlock create(Level level, int power, int pos ){
            IceBlock block = new IceBlock();
            block.adjustStats( power );
            block.HP = block.HT;

            block.enemySeen = true;
            block.state = block.PASSIVE;

            block.pos = pos;

            level.losBlockHigh[pos]=true;
            level.losBlockHigh[pos]=true;

            return block;
        }

        @Override
        public void die( Object cause, Element dmg ) {
            super.die( cause, dmg );

            Dungeon.level.losBlockHigh[pos]=false;
            Dungeon.level.losBlockHigh[pos]=Dungeon.level.map[pos] == Terrain.HIGH_GRASS;

            if ( cause != null) {
                if ( Dungeon.visible[pos] ) {
                    sprite.kill();
                    Sample.INSTANCE.play( Assets.SND_SHATTER, 1, 1, 0.5f );
                    CellEmitter.get( pos ).burst( Speck.factory( Speck.ICESHARD ), 16 );
                }

                for (int i : Level.NEIGHBOURS8) {
                    int cell = pos + i;
                    Char c = Actor.findChar(cell);
                    if (c != null && !(c instanceof IceBlock))
                        c.damage(c.absorb(damageRoll(), false), this, Element.FROST);
                }
            }
        }

        @Override
        public String description() {
            return "A solid block of ice, you can get cold if you stand near it without moving. " +
                    "It will melt eventually, but if is damaged enough, it will break apart and shatter, damaging nearby beings.";
        }
    }
}
