/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.FrigidVapours;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Chilled;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Splash;
import com.watabou.noosa.audio.Sample;

public class ChillingTrap extends Trap{

	{
		color = WHITE;
		shape = DOTS;
	}

	public String name(){
		return "Chilling trap";
	}

	public String description() {
		return "When activated, chemicals in this trap will rapidly freeze the air around its location.";
	}

	@Override
	public void activate() {
		if (Dungeon.visible[ pos ]){
			Splash.at( pos, 0xFFB2D6FF, 6);
			Sample.INSTANCE.play( Assets.SND_SHATTER );
		}
		GameScene.add( Blob.seed( pos, 250, FrigidVapours.class ) );
		for (int n : Level.NEIGHBOURS9) {
			Char ch = Actor.findChar(pos +n);
			if (ch != null) {
				BuffActive.addFromDamage( ch, Chilled.class, 3 );
			}
		}
	}



}
