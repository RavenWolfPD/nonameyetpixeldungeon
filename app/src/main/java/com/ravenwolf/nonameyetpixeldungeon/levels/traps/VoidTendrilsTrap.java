/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.VoidTendril;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.BArray;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ShadowParticle;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.PathFinder;
import com.watabou.utils.Random;

import java.util.ArrayList;

public class VoidTendrilsTrap extends Trap {
	
	{
		color = VIOLET;
		shape = STARS;
	}

	public String name(){
		return "Void rift trap";
	}

	public String description() {
		return "Built from dark magic, this trap will spawn fragments of void in the shape of sharp tendrils.";
	}

	@Override
	public void activate() {
		
		if (Dungeon.visible[pos]){
			Sample.INSTANCE.play( Assets.SND_LIGHTNING );
            CellEmitter.get(pos).burst( ShadowParticle.UP, Random.Int( 3, 5 ) );
		}
        ArrayList<Integer> nearby = new ArrayList<>();
		PathFinder.buildDistanceMap( pos, BArray.not( Dungeon.level.solid, null ), 3 );
		for (int i = 0; i < PathFinder.distance.length; i++) {
			if (PathFinder.distance[i] < Integer.MAX_VALUE && Dungeon.level.passable[i]) {
                if ( Actor.findChar(i) == null) {
                    nearby.add(i);
                }
			}
		}

		int summons = Math.min(Random.Int(2,4), nearby.size());
		for  (int i = 0; i < summons; i++){
            Integer pos =Random.element(nearby);
            VoidTendril.spawnAt(pos);
            nearby.remove(pos);
        }

	}



}
