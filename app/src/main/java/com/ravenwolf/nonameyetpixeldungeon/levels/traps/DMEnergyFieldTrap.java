/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.levels.traps;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.DM300;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.SparkParticle;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Random;

public class DMEnergyFieldTrap extends Trap {

	{
		color = YELLOW;
		shape = CROSS;

		disarmedByActivation = false;
        canBeHidden = false;
	}

    public String name(){
        return "Power cell";
    }

    public String description() {
        return "This power cell can provide energy to different machines, however, it will cause an electric shock " +
                "to any other being that step into it.";
    }

	@Override
	public void activate() {
        Char ch = Actor.findChar( pos );
        if (ch != null) {
            if (!(ch instanceof DM300)) {
                ch.damage( damageRoll(), this, Element.SHOCK );
            }
            if (Dungeon.visible[pos]){
                Sample.INSTANCE.play( Assets.SND_LIGHTNING );
                CellEmitter.get(pos).burst( SparkParticle.FACTORY, Random.Int( 3, 5 ) );
            }
        }
    }

    static public int damageRoll(){
       return Random.IntRange(1 + Dungeon.chapter() * 2, 2 + Dungeon.chapter() * 3);
    }

}
