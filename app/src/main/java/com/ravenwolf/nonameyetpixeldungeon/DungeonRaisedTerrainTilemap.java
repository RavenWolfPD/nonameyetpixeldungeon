/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon;

import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;


public class DungeonRaisedTerrainTilemap extends BaseTilemap {


	public DungeonRaisedTerrainTilemap() {
		super(Dungeon.level.tilesTex(),DungeonTilemap.tileSet());
		map(  Dungeon.level.map, Level.WIDTH );

	}

	public void map( int[] data, int cols ) {
		int[] raisedData = new int[Dungeon.level.LENGTH];

		for (int i=0; i < Dungeon.level.LENGTH; i++) {
			//default raised is empty
			raisedData[i] = Terrain.RAISED_EMPTY;

			if (data[i] == Terrain.HIGH_GRASS)
				raisedData[i] = Terrain.RAISED_HIGH_GRASS;

		}
		super.map(raisedData, cols);
	}


	public void updateRaisedTile( int pos) {
		if (pos < Level.WIDTH)
			return;
		boolean updateCurrentTile = false;
		int currentRaisedTile = Terrain.RAISED_EMPTY;

		if (data[pos] == Terrain.RAISED_HIGH_GRASS && Dungeon.level.map[pos] != Terrain.HIGH_GRASS){
			updateCurrentTile = true;
			currentRaisedTile = Terrain.RAISED_EMPTY;
		}

		if (updateCurrentTile) {
			data[pos]=currentRaisedTile;
			updated.union(pos % Level.WIDTH, pos / Level.WIDTH);
		}
	}

}
