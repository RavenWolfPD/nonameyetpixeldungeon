/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon;


import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.watabou.noosa.Image;
import com.watabou.noosa.tweeners.AlphaTweener;

public class DungeonIsometricTilemap extends BaseTilemap {

	public DungeonIsometricTilemap() {
		super(Dungeon.level.tilesTex(),DungeonTilemap.tileSet());
		map( Dungeon.level.map, Level.WIDTH );

	}


	public void map( int[] data, int cols ) {
		int[] isometricTerrain = new int[Dungeon.level.LENGTH];

		for (int i=0; i < Dungeon.level.LENGTH ; i++) {
			//default is current map tile
			isometricTerrain[i] = Terrain.RAISED_EMPTY;

			//last row are always flat walls
			if (i >= Level.LENGTH - Level.WIDTH){
				isometricTerrain[i] = Terrain.RAISED_WALL_FLAT;
				continue;
			}

			isometricTerrain[i] = getTile(data, cols, i);
			//DOORS
			if (data[i] == Terrain.DOOR_CLOSED) {
				if (!Terrain.isWall(data[i + cols])) {
					isometricTerrain[i] = Terrain.CLOSED_DOOR_ISO_H;
				} else {
					isometricTerrain[i - cols] = Terrain.WALL_DOOR_ISO_V;
					isometricTerrain[i] = Terrain.CLOSED_DOOR_ISO_V;
				}
			}
			if (data[i] == Terrain.LOCKED_DOOR) {
				if (!Terrain.isWall(data[i + cols])) {
					isometricTerrain[i] = Terrain.LOCKED_DOOR_ISO_H;
				} else {
					isometricTerrain[i - cols] = Terrain.WALL_DOOR_ISO_V;
					isometricTerrain[i] = Terrain.LOCKED_DOOR_ISO_V;
				}
			}
			if (data[i] == Terrain.OPEN_DOOR) {
				if (!Terrain.isWall(data[i + cols])) {
					isometricTerrain[i] = Terrain.OPEN_DOOR_ISO_H;
				} else {
					isometricTerrain[i - cols] = Terrain.WALL_DOOR_ISO_V;
					isometricTerrain[i] = Terrain.OPEN_DOOR_ISO_V;
				}
			}
			if (data[i] == Terrain.EMBERS_DOOR) {
				if (!Terrain.isWall(data[i + cols])) {
					isometricTerrain[i] = Terrain.EMBERS_DOOR_ISO_H;
				} else {
					isometricTerrain[i - cols] = Terrain.WALL_DOOR_ISO_V;
					isometricTerrain[i] = Terrain.EMBERS_DOOR_ISO_V;
				}
			}

			//FIXME should be moved to other place
			//floor tiles variance
			switch (data[i]) {
				case Terrain.EMPTY:
				case Terrain.TRAP:
				case Terrain.SECRET_TRAP:
				case Terrain.INACTIVE_TRAP:
					isometricTerrain[i] = Terrain.EMPTY + getTileVariationWitRare(i) * Terrain.FLOOR_VARIATION_OFFSET;
					break;
				case Terrain.EMPTY_SP:
				case Terrain.EMPTY_DECO:
					isometricTerrain[i] = data[i] + getTileVariation(i) * Terrain.FLOOR_VARIATION_OFFSET;
					break;
			}

		}
		super.map(isometricTerrain, cols);

	}



	public int getTile(int[] data, int cols, int i){

		//last row are always flat walls
		if (i >= Level.LENGTH - Level.WIDTH){
			return Terrain.RAISED_WALL_FLAT;
		}

		int tile = Terrain.RAISED_EMPTY;
		switch (data[i]){
			case Terrain.WALL :
				if (Terrain.isAnyDoor(data[i + cols]) || data[i + cols] == Terrain.EMBERS_DOOR)
					tile = Terrain.WALL_DOOR_ISO_V;
				else if (!Terrain.isWall(data[i + cols]) && !Terrain.isAnyDoor(data[i + cols]))
					tile = Terrain.WALL_ISO + getTileVariationWitRare(i);
				else
					tile = Terrain.RAISED_WALL_FLAT;
				break;
			case Terrain.WALL_DECO :
				if (!Terrain.isWall(data[i + cols]) && !Terrain.isAnyDoor(data[i + cols]))
					tile = Terrain.WALL_DECO_ISO + getTileVariation(i);
				else
					tile = Terrain.RAISED_WALL_FLAT;
				break;
			case Terrain.WALL_GRATE:
				if (!Terrain.isWall(data[i + cols]) && !Terrain.isAnyDoor(data[i + cols]))
					tile = Terrain.WALL_GRATE_WATER_ISO;
				break;
			case Terrain.WALL_SIGN :
				if (!Terrain.isWall(data[i + cols]) && !Terrain.isAnyDoor(data[i + cols]))
					tile = Terrain.WALL_SIGN_ISO;
				break;
			case Terrain.CHASM_WALL :
				tile = Terrain.CHASM_WALL_FLAT;
				break;
			case Terrain.DOOR_ILLUSORY :
				if (!Terrain.isWall(data[i + cols]) && !Terrain.isAnyDoor(data[i + cols]))
					tile = Terrain.WALL_ISO;
				else
					tile = Terrain.RAISED_WALL_FLAT;
				break;
			case Terrain.LOCKED_EXIT :
				tile = Terrain.LOCKED_EXIT_ISO;
				break;
			case Terrain.UNLOCKED_EXIT :
				tile = Terrain.UNLOCKED_EXIT_ISO;
				break;
			case Terrain.BARRICADE :
				tile = Terrain.BARRICADE_ISO;
				break;
			case Terrain.BOOKSHELF:
				tile = Terrain.BOOKSHELF_ISO;
				break;
			case Terrain.SHELF_EMPTY:
				tile = Terrain.BOOKSHELF_EMPTY_ISO;
				break;
		}

		return tile;

	}


	public int[] getData(){
		return data;
	}


	public void updateIsometricTile(int pos ) {

		int[] map=Dungeon.level.map;
		int width=Level.WIDTH;

		int lowerTile = pos + width;

		if (pos >= Level.LENGTH - width)
			return;
		boolean updateUpperTile = false;
		boolean updateTile = true;
		switch (map[pos]){
			case Terrain.DOOR_CLOSED :
				if (!Terrain.isWall(map[lowerTile])) {
					data[pos] = Terrain.CLOSED_DOOR_ISO_H;
				} else {
					data[pos] = Terrain.CLOSED_DOOR_ISO_V;
				}
				//discover illusory door
				if (Terrain.isWall(map[pos - width])){
					updateUpperTile =true;
					data[pos - width] = Terrain.WALL_DOOR_ISO_V;
				}
				break;
			case Terrain.OPEN_DOOR :
				if (!Terrain.isWall(map[lowerTile])) {
					data[pos] = Terrain.OPEN_DOOR_ISO_H;
				} else {
					data[pos] = Terrain.OPEN_DOOR_ISO_V;
				}
				//discover illusory door
				if (Terrain.isWall(map[pos - width])){
					updateUpperTile =true;
					data[pos - width] = Terrain.WALL_DOOR_ISO_V;
				}
				break;
			case Terrain.LOCKED_DOOR :
				if (!Terrain.isWall(map[lowerTile])) {
					data[pos] = Terrain.LOCKED_DOOR_ISO_H;
				} else {
					data[pos] = Terrain.LOCKED_DOOR_ISO_V;
				}
				break;
			case Terrain.EMBERS_DOOR:
				if (!Terrain.isWall(map[lowerTile])) {
					data[pos] = Terrain.EMBERS_DOOR_ISO_H;
				} else {
					data[pos] = Terrain.EMBERS_DOOR_ISO_V;
				}
				break;
			case Terrain.EMBERS:
				data[pos] =Terrain.RAISED_EMPTY;
				break;
			case Terrain.LOCKED_EXIT :
				data[pos] = Terrain.LOCKED_EXIT_ISO; break;
			case Terrain.UNLOCKED_EXIT :
				data[pos] = Terrain.UNLOCKED_EXIT_ISO; break;

			//FIXME
			case Terrain.EMPTY:
			case Terrain.TRAP:
			case Terrain.SECRET_TRAP:
			case Terrain.INACTIVE_TRAP:
			case Terrain.EMPTY_DECO:
			case Terrain.EMPTY_SP:
				//Special case for other terrain turning into floor
				// like enter yog arena or defeating the cave boss level
				if(data[pos] > Terrain.INACTIVE_TRAP)
					data[pos] = getTile(map, width, pos);
				else
					updateTile = false;
				break;
			default:
				//updateTile = false;
				data[pos] = getTile(map, width, pos);
		}



		if (updateTile)
			updated.union(pos % Level.WIDTH, pos / Level.WIDTH);
		if (updateUpperTile) {
			int upperTile = pos - width;
			updated.union(upperTile % Level.WIDTH, upperTile / Level.WIDTH);
		}
	}


	public void discover( int pos, int oldValue ) {

		//FIXME
		int oldIsoTile = oldValue;
		if (oldValue > Terrain.INACTIVE_TRAP)
		 oldIsoTile = getTile(Dungeon.level.map, Level.WIDTH, oldValue);
		final Image tile = DungeonTilemap.tile( oldIsoTile );
		tile.point( tileToWorld( pos ) );

		// For bright mode
		tile.rm = tile.gm = tile.bm = rm;
		tile.ra = tile.ga = tile.ba = ra;
		GameScene.visualOverTerrain( tile );

		GameScene.visualOverTerrain( new AlphaTweener( tile, 0, 0.6f ) {
			protected void onComplete() {
				tile.killAndErase();
				killAndErase();
			}
		} );
	}

}
