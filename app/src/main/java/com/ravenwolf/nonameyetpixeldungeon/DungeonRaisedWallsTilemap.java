/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon;

import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;

public class DungeonRaisedWallsTilemap extends BaseTilemap {


	public DungeonRaisedWallsTilemap() {
		super(Dungeon.level.tilesTex(),DungeonTilemap.tileSet());
		map(  Dungeon.level.map, Level.WIDTH );

	}


	public void map( int[] data, int cols ) {
		int[] raisedData = new int[Dungeon.level.LENGTH];
		//first column is always empty (walls)
		for (int i=0; i <Dungeon.level.WIDTH ; i++) {
			raisedData[i] = Terrain.RAISED_EMPTY;
		}

		for (int i=Dungeon.level.WIDTH; i < Dungeon.level.LENGTH ; i++) {
			int upperTile= i - cols;
			//default raised is empty
			raisedData[i] = Terrain.RAISED_EMPTY;

			if (Terrain.isWall(data[i]) && !Terrain.isWall(data[upperTile])) {
				raisedData[upperTile] = Terrain.isAnyDoor(data[upperTile]) ? Terrain.RAISED_WALL_DOOR_V :Terrain.RAISED_WALL;
			}
			if (data[i] == Terrain.DOOR_CLOSED) {
				if (!Terrain.isWall(data[upperTile])) {
					raisedData[upperTile] =Terrain.RAISED_CLOSED_DOOR_H;
				} else {
					raisedData[upperTile] =Terrain.RAISED_CLOSED_DOOR_V;
				}
			}
			if (data[i] == Terrain.LOCKED_DOOR) {
				if (!Terrain.isWall(data[i + cols])) {
					raisedData[upperTile] =Terrain.RAISED_CLOSED_DOOR_H;
				} else {
					raisedData[upperTile] =Terrain.RAISED_LOCKED_DOOR_V;
				}
			}
			if (data[i] == Terrain.OPEN_DOOR) {
				if (!Terrain.isWall(data[i + cols])) {
					raisedData[upperTile] =Terrain.RAISED_OPEN_DOOR_H;
				}
			}
			if (Terrain.isChapterExit(data[i])) {
				raisedData[i] =Terrain.RAISED_EXIT;
				raisedData[upperTile] =Terrain.RAISED_WALL_FLAT;
			}


			if (data[i] == Terrain.BARRICADE)
				raisedData[upperTile] = Terrain.RAISED_BARRICADE;
/*
			//lower walls blocker
			if (Terrain.isWall(data[i]) && i+Level.WIDTH < Level.LENGTH && !Dungeon.level.visited[i+Level.WIDTH] && !Dungeon.level.mapped[i+Level.WIDTH])
				raisedData[i] = Terrain.WALL_FLAT;
*/
		}
		super.map(raisedData, cols);
	}


	public void updateRaisedTile( int pos , int currentIsometricTile) {

		if (!Level.insideMap(pos))
			return;

		int upperRaisedCell =pos - Level.WIDTH;

		boolean updateCurrentTile = false;

		boolean updateUpperRaisedTile = true;
		int upperRaisedTile = data[upperRaisedCell];
		int currentRaisedTile = data[pos];
		//vertical doors need to update the current raised tile in case they are discovered
		/*switch (currentIsometricTile){
			case Terrain.CLOSED_DOOR_ISO_H:
			case Terrain.LOCKED_DOOR_ISO_H:
				upperRaisedTile = Terrain.RAISED_CLOSED_DOOR_H;break;
			case Terrain.OPEN_DOOR_ISO_H:
				upperRaisedTile = Terrain.RAISED_OPEN_DOOR_H;break;
			case Terrain.CLOSED_DOOR_ISO_V:
				upperRaisedTile = Terrain.RAISED_CLOSED_DOOR_V;
				currentRaisedTile = Terrain.RAISED_WALL_DOOR_V;
				updateCurrentTile=true;
				break;
			case Terrain.LOCKED_DOOR_ISO_V:
				upperRaisedTile =Terrain.RAISED_LOCKED_DOOR_V;
				currentRaisedTile = Terrain.RAISED_WALL_DOOR_V;
				updateCurrentTile=true;
				break;
			case Terrain.OPEN_DOOR_ISO_V:
			case Terrain.EMBERS_DOOR_ISO_V:
				upperRaisedTile = Terrain.RAISED_EMPTY;
				currentRaisedTile = Terrain.RAISED_WALL_DOOR_V;
				updateCurrentTile=true;
				break;
			case Terrain.EMBERS_DOOR_ISO_H:
				upperRaisedTile = Terrain.RAISED_EMPTY;
				break;
			default:
				updateUpperRaisedTile=false;
		}
*/

		int[] map=Dungeon.level.map;
		int width=Level.WIDTH;
		int lowerTile = pos + width;

		switch (map[pos]){
			case Terrain.DOOR_CLOSED :
				if (!Terrain.isWall(map[lowerTile])) {
					upperRaisedTile = Terrain.RAISED_CLOSED_DOOR_H;
				} else {
					upperRaisedTile = Terrain.RAISED_CLOSED_DOOR_V;
					currentRaisedTile = Terrain.RAISED_WALL_DOOR_V;
					updateCurrentTile=true;
				}
				break;
			case Terrain.OPEN_DOOR :
			case Terrain.EMBERS_DOOR:
				if (!Terrain.isWall(map[lowerTile])) {
					upperRaisedTile = Terrain.RAISED_OPEN_DOOR_H;
				}else{
					upperRaisedTile = Terrain.RAISED_EMPTY;
				}
				break;
			case Terrain.LOCKED_DOOR :
				if (!Terrain.isWall(map[lowerTile])) {
					upperRaisedTile = Terrain.RAISED_CLOSED_DOOR_H;
				} else {
					upperRaisedTile = Terrain.RAISED_LOCKED_DOOR_V;
					currentRaisedTile = Terrain.RAISED_WALL_DOOR_V;
					updateCurrentTile = true;
				}
				break;

			case Terrain.EMBERS:
				upperRaisedTile = Terrain.RAISED_EMPTY;
				break;
			default:
				if (data[upperRaisedCell] == Terrain.RAISED_WALL && !Terrain.isWall(map[pos]))
					upperRaisedTile = Terrain.RAISED_EMPTY;
				else if (Terrain.isWall(map[pos]) && !Terrain.isWall(map[upperRaisedCell])) {
					upperRaisedTile = Terrain.isAnyDoor(map[upperRaisedCell]) ? Terrain.RAISED_WALL_DOOR_V :Terrain.RAISED_WALL;
				}
		}

		if (data[upperRaisedCell] == Terrain.RAISED_BARRICADE && Dungeon.level.map[pos] == Terrain.EMBERS){
			updateUpperRaisedTile = true;
			upperRaisedTile = Terrain.RAISED_EMPTY;
		}

		/*
		//update wall blockers
		if (data[pos] == Terrain.WALL_FLAT && pos+Level.WIDTH < Level.LENGTH && Dungeon.level.visited[pos+Level.WIDTH] ){
			updateCurrentTile = true;
			currentRaisedTile = Terrain.RAISED_EMPTY;
		}
		//ugly solution to update walls when seeing lower terrain tile
		if (data[upperRaisedCell] == Terrain.WALL_FLAT && pos+Level.WIDTH < Level.LENGTH && Dungeon.level.visited[pos+Level.WIDTH] ){
			updateUpperRaisedTile = true;
			upperRaisedTile = Terrain.RAISED_EMPTY;
		}

		//update wall blockers whe discovering door
		if (data[pos] == Terrain.WALL_FLAT && Terrain.isAnyDoor(Dungeon.level.map[pos]) ){
			updateCurrentTile = true;
			currentRaisedTile = Terrain.RAISED_EMPTY;
		}
		*/
/*

		//update wall blockers (a flat wall is drawn over every wall to avoid knowing
		// if is an isometric wall before visiting the lower tile to the wall)
		if (data[pos] == Terrain.WALL_FLAT ){
			if (pos + Level.WIDTH < Level.LENGTH
					&& (Dungeon.level.visited[pos + Level.WIDTH] || Dungeon.level.mapped[pos+Level.WIDTH])
					//prevent removing flat wall over exit as it should be raised
					&& !Terrain.isChapterExit(Dungeon.level.map[pos + Level.WIDTH])) {
				updateCurrentTile = true;
				currentRaisedTile = Terrain.RAISED_EMPTY;
			}

			//update wall blockers when discovering door
			if (Terrain.isAnyDoor(Dungeon.level.map[pos])) {
				updateCurrentTile = true;
				currentRaisedTile = Terrain.RAISED_EMPTY;
			}
		}

		//ugly solution to update walls when seeing lower terrain tile
		if (data[upperRaisedCell] == Terrain.WALL_FLAT && (Dungeon.level.visited[pos] ||  Dungeon.level.mapped[pos])
			//prevent removing flat wall over exit as it should be raised
			 && !Terrain.isChapterExit(Dungeon.level.map[pos])) {
			updateUpperRaisedTile = true;
			upperRaisedTile = Terrain.RAISED_EMPTY;
		}
*/
		if (updateUpperRaisedTile) {
			data[upperRaisedCell]=upperRaisedTile;
			updated.union(upperRaisedCell % Level.WIDTH, upperRaisedCell / Level.WIDTH);
		}
		if (updateCurrentTile) {
			data[pos]=currentRaisedTile;
			updated.union(pos % Level.WIDTH, pos / Level.WIDTH);
		}

	}

}
