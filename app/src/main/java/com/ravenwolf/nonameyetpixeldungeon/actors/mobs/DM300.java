/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Badges;
import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.DungeonTilemap;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.Statistics;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.DelayedBoulders;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.bonuses.Bonus;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.bonuses.Shielding;
import com.ravenwolf.nonameyetpixeldungeon.items.keys.GoldenKey;
import com.ravenwolf.nonameyetpixeldungeon.items.misc.Gold;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfDisintegration;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.DMEnergyFieldTrap;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.BArray;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.BlastWave;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.DeathRay;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Lightning;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.MagicMissile;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ElmoParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.PurpleParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.SparkParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.CharSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.DM300Sprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.BuffIndicator;
import com.watabou.noosa.Camera;
import com.watabou.noosa.audio.Sample;
import com.watabou.noosa.particles.Emitter;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.PathFinder;
import com.watabou.utils.Random;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class DM300 extends MobHealthy {

    protected int breaks = 0;
    //charging = 0 mean is done charging laser
    public int charging = -1;
    private int specialCD =10;
   // private int meleeCooldown =8;
   // private static int MELEE_CHARGING = 100;

    private Emitter blastwaveCharingEmitter = null;
    private Emitter overChargeEmitter = null;

    public DM300() {

        super( 4, 20, true );

        // 450

        dexterity /= 2; // yes, we divide it again
        armorClass *= 2; // and yes, we multiply it back
        //reduce max damage a bit
        maxDamage -= tier;

        name = Dungeon.depth == Statistics.deepestFloor ? "DM-300" : "DM-400";
        spriteClass = DM300Sprite.class;

        loot = Gold.class;
        lootChance = 4f;

        HUNTING = new Hunting();

        resistances.put(Element.Flame.class, Element.Resist.PARTIAL );
        resistances.put(Element.Frost.class, Element.Resist.PARTIAL );
        resistances.put(Element.Unholy.class, Element.Resist.PARTIAL );
        //resist domination
        resistances.put(Element.Dispel.class, Element.Resist.PARTIAL );

        resistances.put(Element.Mind.class, Element.Resist.IMMUNE );
        resistances.put(Element.Body.class, Element.Resist.IMMUNE );
    }

    @Override
    public boolean isSolid() {
        return true;
    }

    @Override
    public float awareness(){
        return 1.0f;
    }

    //lower penalty for ranged attacks
    public int getRangePenaltyFactor() {
        return 8;
    }


    @Override
    protected float healthValueModifier() {
        return 0.25f;
    }

    @Override
    public HashMap<Class<? extends Element>, Float> resistances() {
        HashMap<Class<? extends Element>, Float> resistances = super.resistances();

        if( charging!= -1 ){
            resistances.put( Element.Physical.class, Element.Resist.PARTIAL );
        }

        return resistances;
    }

    protected int nextStepTo( int target ) {
        if (!enemySeen)
            return super.nextStepTo(target);
        //DM dont avoid traps when chasing enemy
        flying = true;
       int next=super.nextStepTo(target);
        flying = false;
        return next;
    }

    @Override
    public void move( int step ) {
        super.move( step );

        if( Dungeon.level.traps.get(step) instanceof DMEnergyFieldTrap) {
            GLog.n( name+" absorb energy!" );
            sprite.emitter().burst( ElmoParticle.FACTORY, 5 );
            BuffActive.add(this, Shielding.class,  Random.IntRange(6, 8));
        }

        if (buff(Overcharged.class) != null) {
            PathFinder.buildDistanceMap(pos, BArray.not(Dungeon.level.solid, null), 3);
            for (int i = 0; i < PathFinder.distance.length; i++) {
                if (PathFinder.distance[i] < Integer.MAX_VALUE) {
                    if (Dungeon.level.traps.get(i) instanceof DMEnergyFieldTrap
                    && ( PathFinder.distance[i] == 1 || i == Ballistica.cast(pos, i, false, false)) ){

                        sprite.parent.add(new Lightning(new int[]{step, i}, 2, null));
                        sprite.centerEmitter().burst(SparkParticle.FACTORY, Random.Int(3, 5));
                        int charPos =  Ballistica.cast(step, i, false, true);
                        Char ch = Actor.findChar(charPos);
                        if (ch != null) {
                            ch.damage(DMEnergyFieldTrap.damageRoll(), this, Element.SHOCK_PERIODIC);
                        }
                    }
                }
            }
        }

    }

    @Override
    public int defenseProc( Char enemy, int damage,  boolean blocked ) {
        if (! Level.adjacent(pos, enemy.pos))
            specialCD--;
        else if (buff(Overcharged.class) != null) {
            enemy.damage(Random.IntRange(damage / 4, damage / 3), this, Element.SHOCK);
            sprite.parent.add(new Lightning(new int[]{pos, enemy.pos}, 2, null));
        }

        return super.defenseProc(enemy,damage,blocked);
    }


    @Override
    public boolean act() {

        if (charging > 0) {
            charging--;
            spend(TICK);
            return true;
        }
/*
        if (meleeCooldown == MELEE_CHARGING){
            meleeCooldown = Random.Int(6, 8);
            spend(TICK);
            blastwaveAttack(damageRoll()*3/2);
            if (blastwaveCharingEmitter != null)
                blastwaveCharingEmitter.kill();
            return true;
        }
*/
        if( charging != 0 && 3 - breaks > 4 * HP / HT ) {
            breaks++;
            BuffActive.add(this, Overcharged.class, 6 + breaks * Random.IntRange(8, 10));
            overChargeEmitter = sprite.emitter();
            overChargeEmitter.pour(SparkParticle.FACTORY, 0.08f);
            if (Dungeon.visible[pos]) {
                GLog.n( name+" is overcharged!" );
            }
            sprite.idle();
            spend( TICK );
            return true;
        }

        if (enemySeen && specialCD > 0) {
            specialCD--;
        }

        return super.act();
    }

//    private void blastwaveAttack(int power){
//        //first knock enemies at two range
//        for (int i : Level.NEIGHBOURS16) {
//            if( Level.insideMap(pos + i)) {
//                Char n = Actor.findChar(pos + i);
//                //has path to the enemy (no wall or other mob in between)
//                if (n != null && n.pos == Ballistica.cast(pos, n.pos, false, false)) {
//                    int dmg = enemy.absorb(power/2);
//                    enemy.defenseProc(this, dmg, false);
//                    enemy.damage(dmg , this, Element.PHYSICAL );
//                    n.knockBack(pos, power / 2, 1);
//                }
//            }
//        }
//
//        Char n;
//        for (int i : Level.NEIGHBOURS8) {
//            n = Actor.findChar(pos + i);
//            if (n!=null) {
//                int dmg = enemy.absorb(power);
//                enemy.defenseProc(this, dmg, false);
//                enemy.damage(dmg , this, Element.PHYSICAL );
//                n.knockBack(pos, power, 2);
//            }
//        }
//        Camera.main.shake(2, 0.5f);
//        Sample.INSTANCE.play( Assets.SND_BLAST, 1.0f, 1.0f, 0.8f );
//        BlastWave.createAtPos( pos );
//    }


    @Override
    public void remove( Buff buff ) {

        if( buff instanceof Overcharged ) {
            sprite.showStatus( CharSprite.NEUTRAL, "..." );
            GLog.i(name+" is not overcharged anymore.");
            if (overChargeEmitter != null)
                overChargeEmitter.kill();
        }

        super.remove(buff);
    }

    @Override
    protected boolean canAttack( Char enemy ) {
        return super.canAttack( enemy ) || charging != -1 || specialCD <= 0 &&
                Ballistica.cast(pos, enemy.pos, false, true) == enemy.pos ;
    }

    protected boolean doAttack( final Char enemy ) {
        boolean adjacent = Level.adjacent(pos, enemy.pos);
        //charge blastwave
//        if (meleeCooldown <= 0 && adjacent){
//            Sample.INSTANCE.play(Assets.SND_BADGE,1,1,0.5f);
//            meleeCooldown = MELEE_CHARGING;
//            spend(TICK);
//            sprite.turnTo(pos, enemy.pos);
//            blastwaveCharingEmitter = sprite.emitter();
//            blastwaveCharingEmitter.pour(Speck.factory(Speck.WOOL),0.04f);
//            return true;
//        }

        //attack at melee
        if (specialCD > 0 || adjacent && charging == -1) {
           // meleeCooldown--;
            return super.doAttack(enemy);
        }

        //cast avalanche if enemy at range
        if (charging == -1 && (Random.Int(3)==0) || buff(Overcharged.class) != null){
            specialCD = Random.Int(8,12);
            sprite.turnTo(pos, enemy.pos);
            spend(TICK);
            MagicMissile.earth( sprite.parent, pos, enemy.pos,  new Callback() {
                @Override
                public void call(){
                    int power = 6 + Dungeon.chapter() * 6;
                    //add delayed it so it takes a turn to activate
                    //this delay is only for the first time is added to the level
                    GameScene.add(Blob.seed(enemy.pos, power, DelayedBoulders.class), Actor.TICK);
                    HashSet<Integer> avoid =new HashSet<>(Arrays.asList(Random.Int(1,9),Random.Int(1,9) ));
                    int i=0;
                    for (int n : Level.NEIGHBOURS8) {
                        int cell = enemy.pos + n;
                        if(avoid.contains(i++) || Level.solid[cell] )
                            continue;

                        GameScene.add(Blob.seed(cell, power, DelayedBoulders.class));
                    }
                    for (int n : Level.NEIGHBOURS16) {
                        int cell = enemy.pos + n;
                        if ( !Level.solid[cell] && cell != pos && Level.insideMap(cell) && Random.Float() < 0.6f) {
                            GameScene.add(Blob.seed(cell, power, DelayedBoulders.class));
                        }
                    }

                    Camera.main.shake( 2, 0.1f );
                    next();
                }
            });

            Sample.INSTANCE.play(Assets.SND_MELD,1,1,0.5f);
            Dungeon.hero.interrupt();
            return false;
        }

        //charge laser
        if (charging == -1){
            Dungeon.hero.interrupt();
            Sample.INSTANCE.play(Assets.SND_BEACON,1,1,0.5f);
            charging = 2;
            ((DM300Sprite)sprite).charge(enemy.pos);
            spend(TICK);
            return true;
        }

        final int enemyPos = enemy.pos;
        boolean visible = Dungeon.visible[pos] || Dungeon.visible[enemyPos];

        //cast laser attack
        if (visible) {
            Dungeon.visible[pos] = true;
            sprite.cast(enemyPos, new Callback() {
                @Override
                public void call() {
                    onRangedAttack(enemyPos);
                }
            });
        } else {
            cast(enemy);
        }

        if (enemy == Dungeon.hero) {
            noticed = true;
        }

        spend(TICK);
        return !visible;
    }


    @Override
    protected void onRangedAttack( int cell ) {

        Ballistica.castToMaxDist( pos, cell, 10);
        cell=Ballistica.trace[Ballistica.distance];

        Sample.INSTANCE.play(Assets.SND_RAY);

        sprite.parent.add( new DeathRay( DungeonTilemap.tileCenterToWorld( pos ), DungeonTilemap.tileCenterToWorld( cell ) ) );

        onCastComplete();
        super.onRangedAttack( cell );
    }

    @Override
    public boolean cast( Char enemy ) {

        specialCD = Random.Int(8,12);;
        charging = -1;

        boolean terrainAffected = false;

        for (int i=1; i <= Ballistica.distance ; i++) {

            int pos = Ballistica.trace[i];

            boolean affected = WandOfDisintegration.affectCell(pos);
            if (affected) {
                terrainAffected = true;
            }

            Char ch = Actor.findChar( pos );

            if (ch != null) {
                if (castBolt(ch , damageRoll(), false, Element.ENERGY  )){
                    if (Dungeon.visible[pos]) {
                        CellEmitter.center(pos).burst(PurpleParticle.BURST, Random.IntRange(1, 2));
                    }
                }
            }
            if (Dungeon.level.map[pos] == Terrain.STATUE2){
                CellEmitter.center(pos).burst(PurpleParticle.BURST, Random.IntRange(2, 4));
                Level.set(pos, Terrain.EMPTY_DECO);
                GameScene.updateMap(pos);
                Dungeon.observe();
                CellEmitter.get( pos ).start(Speck.factory(Speck.ROCK), 0.07f, 6 );
            }
        }
        if (terrainAffected) {
            Dungeon.observe();
        }

        return true;
    }

	
	@Override
	public void die( Object cause, Element dmg ) {
		
		super.die( cause, dmg );
		
		GameScene.bossSlain();
		Dungeon.level.drop( new GoldenKey(), pos ).sprite.drop();
        Sample.INSTANCE.play(Assets.SND_DEGRADE,1,1,1f);

        Badges.validateBossSlain();
		
		yell( "Mission failed. Shutting down." );
	}
	
	@Override
	public void notice() {
		super.notice();
        if( enemySeen ) {
            Sample.INSTANCE.play(Assets.SND_BEACON,0.5f);
            yell("Unauthorised personnel detected.");
        }
	}
	
	@Override
	public String description() {
		return
			"This machine was created by the Dwarves several centuries ago. Later, Dwarves started to replace machines with " +
			"golems, elementals and even demons. Eventually it led their civilization to the decline. The DM-300 and similar " +
			"machines were typically used for construction and mining, and in some cases, for city defense."

               + (hasBuff(Overcharged.class) ? "+/n/n When Overcharged DM will shock anything attacking at melee range " +
                    "and when moving it will create arcs of electricity towards nearby power cells":"") ;
	}


    private class Hunting extends Mob.Hunting {

        @Override
        public boolean act( boolean enemyInFOV, boolean justAlerted ) {
            if (!enemyInFOV && charging==0){

                specialCD = Random.Int(8,12);;
                charging = -1;
                sprite.idle();
                spend(TICK);
                Sample.INSTANCE.play(Assets.SND_DEGRADE,1,1,2f);
                if (Dungeon.visible[pos]) {
                    CellEmitter.get(pos).burst(Speck.factory(Speck.WOOL), 4);
                }
                return true;
            }

            return super.act(enemyInFOV, justAlerted);
        }

    }

    private static final String BREAKS	= "breaks";
    private static final String CHARGING	= "charging";
    private static final String SPECIAL_CD = "specialCooldown";

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle(bundle);
        bundle.put( BREAKS, breaks );
        bundle.put( CHARGING, charging );
        bundle.put(SPECIAL_CD, specialCD);
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle(bundle);
        breaks = bundle.getInt( BREAKS );
        charging = bundle.getInt( CHARGING );
        specialCD = bundle.getInt(SPECIAL_CD);
    }

    public static class Overcharged extends Bonus {

        @Override
        public String toString() {
            return "Overcharged";
        }

        @Override
        public String statusMessage() {
            return "overcharged";
        }

        @Override
        public int icon() {
            return BuffIndicator.SHOCKED;
        }
    }

}