/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Ensnared;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.BArray;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.MagicMissile;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.TentacleSprite;
import com.watabou.utils.Callback;
import com.watabou.utils.PathFinder;
import com.watabou.utils.Random;

import java.util.ArrayList;

public class Tentacle extends MobRanged {

	{
		name = "tentacle";
		spriteClass = TentacleSprite.class;
	}

	protected boolean damagedThisTurn=false;

	protected int turnsNoAttacking;

	public Tentacle() {

		super( Dungeon.chapter(), Dungeon.depth/3 + Dungeon.chapter() * 2, false );

		resistances.put(Element.Unholy.class, Element.Resist.PARTIAL);
		resistances.put(Element.Body.class, Element.Resist.PARTIAL);
		resistances.put(Element.Mind.class, Element.Resist.IMMUNE);

		PASSIVE = new Guarding();
		state = PASSIVE;
	}


	@Override
	public boolean immovable() {
		return true;
	}

	@Override
	protected boolean getCloser(int target) {
		return true;
	}

	@Override
	protected boolean getFurther(int target) {
		return true;
	}

	@Override
	public int viewDistance() {
		return 3;
	}

	@Override
	public Element damageType() {
		return Element.ENERGY;
	}

	@Override
	public boolean add(Buff buff) {
		if (buff instanceof Ensnared)
			return false;
		else
			return super.add(buff);
	}

	@Override
	protected boolean act() {
		state = PASSIVE;
		if (turnsNoAttacking > 10){
			turnsNoAttacking = 0;
			for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
				if (mob instanceof YogDzewa) {
					Mob Yog =mob;
					return blink(Yog.pos);
				}
			}
		}
		return super.act();
	}

	@Override
	public void damage( int dmg, Object src, Element type ) {
		super.damage( dmg, src, type );
		damagedThisTurn = true;
	}

	@Override
	protected boolean canAttack( Char enemy ) {
		return super.canAttack( enemy ) || Level.distance( pos, enemy.pos ) <= 3 &&
				Ballistica.cast(pos, enemy.pos, false, true) == enemy.pos && !isCharmedBy( enemy );
	}

	@Override
	protected void onRangedAttack( int cell ) {

		MagicMissile.poison(sprite.parent, pos, cell,
				new Callback() {
					@Override
					public void call() {
						onAttackComplete();
					}
				});

		super.onRangedAttack( cell );
	}

	@SuppressWarnings("unchecked")
	public static Tentacle spawnAt(int pos) {

		// cannot spawn on walls, chasms or already occupied tiles
		if (!Level.solid[pos] && !Level.chasm[pos] && Actor.findChar(pos) == null) {

			Tentacle lash = new Tentacle();

			lash.pos = pos;
			lash.enemySeen = true;
			lash.state = lash.PASSIVE;

			GameScene.add(lash, 0f);
			Dungeon.level.press(lash.pos, lash);

			lash.sprite.spawn();

			return lash;

		} else {

			return null;

		}
	}

	public void knockBack(int sourcePos, int damage, int amount, final Callback callback) {
		if (callback != null)
			callback.call();
		return;
	}

	//Blinks nearby target position
	public boolean blink(int target) {
		return blink(target,3);
	}

	//Blinks nearby target position
	public boolean blink(int target, int dist) {

		ArrayList<Integer> nearby = new ArrayList<>();
		PathFinder.buildDistanceMap( target, BArray.not( Dungeon.level.solid, null ), dist );
		for (int i = 0; i < PathFinder.distance.length; i++) {
			if (PathFinder.distance[i] < Integer.MAX_VALUE && Dungeon.level.passable[i]) {
				if ( Actor.findChar(i) == null) {
					nearby.add(i);
				}
			}
		}

		//if no position available, wait one turn
		if (nearby.isEmpty()) {
			spend( TICK );
			return true;
		}


		Integer newPos =Random.element(nearby);
		((TentacleSprite)sprite).blink(pos, newPos);
		//move( newPos, false );
		spend( 1 / moveSpeed() );
		return false;
	}


	private class Guarding extends Passive {

		@Override
		public boolean act( boolean enemyInFOV, boolean justAlerted ){

			if (enemyInFOV && canAttack( enemy ) ) {
				damagedThisTurn=false;
				return doAttack( enemy );
			} else {
				//reset enemy if cannot attack
				resetEnemy();
				spend( TICK );
				turnsNoAttacking++;
				if (damagedThisTurn)
					turnsNoAttacking+=3;
				damagedThisTurn=false;
				return true;
			}
		}

		@Override
		public String status(){
			return "guarding";
		}
	}

	@Override
	public String description() {
		return
				"A fleshy tentacle from another dimension. " +
				"It is mottled with strange, pulsating growths that seem to throb and beat with a life of their own.";
	}


}
