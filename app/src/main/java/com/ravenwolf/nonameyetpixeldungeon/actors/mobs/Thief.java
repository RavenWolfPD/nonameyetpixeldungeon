/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.HeroClass;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs.Ghost;
import com.ravenwolf.nonameyetpixeldungeon.items.misc.Gold;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.throwing.Knives;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.MissileSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ThiefSprite;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.Random;

public class Thief extends MobPrecise {

    private int rangedCD = 0;

    public Thief() {

        super(2);

        name = "mugger";
        spriteClass = ThiefSprite.class;

        loot = Gold.class;
        lootChance = 0.25f;
    }

    public String getTribe() {
        return TRIBE_LONER;
    }

    @Override
    public int damageRoll() {
        return isRanged() ? super.damageRoll() * 4 / 5 : super.damageRoll();
    }

    @Override
    protected boolean canAttack( Char enemy ) {
        return super.canAttack( enemy ) || rangedCD==0 && Level.distance( pos, enemy.pos ) <= 3 &&
                Ballistica.cast(pos, enemy.pos, false, true) == enemy.pos && !isCharmedBy( enemy );
    }

    @Override
    public boolean act() {
        if (rangedCD >0)
            rangedCD--;
        return super.act();
    }

    @Override
    protected void onRangedAttack(int cell) {
        ((MissileSprite) sprite.parent.recycle(MissileSprite.class)).
                reset(pos, cell, ItemSpriteSheet.THROWING_KNIFE, new Callback() {
                    @Override
                    public void call() {
                        onAttackComplete();
                    }
                });
        rangedCD = Random.Int(5, 7);
        super.onRangedAttack(cell);
    }


    @Override
    public String description() {

        return "The Sewers always been hiding place for all sorts of cutthroats and outlaws. "

                + (Dungeon.hero.heroClass == HeroClass.WARRIOR ?
                "Usually armed with different manners of daggers and knives, these cowards rely on dirty tactics instead of skill and strength." : "")

                + (Dungeon.hero.heroClass == HeroClass.SCHOLAR ?
                "It would be better to exercise caution when dealing with their kind, as lone old man down there can look like an easy prey through the eyes of greed." : "")

                + (Dungeon.hero.heroClass == HeroClass.BRIGAND ?
                "Looks like it's the time to show 'ese rookies who's da boss here. After all, their 'ill-begotten gains' can help you on your 'noble quest', isn't it?" : "")

                + (Dungeon.hero.heroClass == HeroClass.ACOLYTE ?
                "What leads them down the path of banditry? Greed, misfortune, or something more sinister? It doesn't really matter now, however." : "")

                ;
    }

    private static final String RANGED_CD = "rangedCD";
    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle(bundle);
        bundle.put(RANGED_CD, rangedCD);
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle(bundle);
        rangedCD = bundle.getInt(RANGED_CD);
    }

}