/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Badges;
import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.Statistics;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.ConfusionGas;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.CorrosiveGas;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Electricity;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Fire;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.FrigidVapours;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Miasma;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Web;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.bonuses.MindVision;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Blinded;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Dazed;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Debuff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Ensnared;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.special.PinCushion;
import com.ravenwolf.nonameyetpixeldungeon.items.keys.SkeletonKey;
import com.ravenwolf.nonameyetpixeldungeon.items.misc.Gold;
import com.ravenwolf.nonameyetpixeldungeon.items.misc.TomeOfMastery;
import com.ravenwolf.nonameyetpixeldungeon.items.scrolls.ScrollOfClairvoyance;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.GrippingTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.SpearsTrap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.Trap;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.CharSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.MissileSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.TenguSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.HealthIndicator;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.TagAttack;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Bundlable;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.Random;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class Tengu extends MobRanged {

	private static final int JUMP_DELAY = 8;

    private int timeToJump = 0;
    private int amountOfJumps = 0;
    protected int breaks = 0;
    protected boolean clonePhase = false;
    protected int clonesDuration = 0;
    protected boolean dart=false;
    protected boolean damagedThisTurn=false;
    protected ArrayList<Trap> activeSidewallTraps = new ArrayList<>();

    private static Tengu originalTengu() {
        for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
            if (mob instanceof Tengu) {
                return (Tengu)mob;
            }
        }

        return null;
    }

    public Tengu() {

        super( 3, 15, true );

        name = Dungeon.depth == Statistics.deepestFloor ? "Tengu" : "memory of Tengu";
        spriteClass = TenguSprite.class;

        loot = Gold.class;
        lootChance = 4f;

        resistances.put(Element.Mind.class, Element.Resist.PARTIAL);
        resistances.put(Element.Body.class, Element.Resist.PARTIAL);
    }

    @Override
    public int dexterity() {
        return clonePhase ? 0 : super.dexterity();
    }

    @Override
    protected float healthValueModifier() { return 0.25f; }


    @Override
    protected void onRangedAttack( int cell ) {

        if (Random.Float() > 0.15f || clonePhase) {
            ((MissileSprite) sprite.parent.recycle(MissileSprite.class)).
                    reset(pos, cell, ItemSpriteSheet.THROWING_STAR, new Callback() {
                        @Override
                        public void call() {
                            onAttackComplete();
                        }
                    });
        }else {
            dart = true;
            ((MissileSprite) sprite.parent.recycle(MissileSprite.class)).
                    reset(pos, cell, ItemSpriteSheet.THROWING_DART, new Callback() {
                        @Override
                        public void call() {
                            onAttackComplete();
                        }
                    });
        }

        super.onRangedAttack(cell);
    }

    @Override
    public int attackProc(Char enemy, int damage, boolean blocked ) {
        if ( dart) {
            damage/=3;
            if (!blocked) {
                BuffActive.add(enemy, Dazed.class, 1);
            }
        }

        return damage;
    }

    public boolean attack( Char enemy, int damageRoll ){
        boolean hit = super.attack(enemy, damageRoll);
        dart=false;
        return hit;
    }
	
	@Override
	protected boolean getCloser( int target ) {
		if (!rooted && Level.fieldOfView[target]) {
			jump(false, true);
			return true;
		} else {
			return super.getCloser( target );
		}
	}

	@Override
	protected boolean doAttack( Char enemy ) {

		timeToJump++;
		if (timeToJump >= JUMP_DELAY ) {
			jump(false, true);
			return true;
		} else {
			return super.doAttack(enemy);
		}
	}

    private void jumpClones(boolean clearClones) {

        HealthIndicator.instance.target(null);
        Debuff.removeAll( this );
        for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
            if (mob instanceof TenguClone) {
                TenguClone clone=(TenguClone) mob;
                if (clearClones)
                    clone.revealed=0;
                //clone.updateAlpha();
                else if (clone.revealed>0)
                    clone.revealed--;
                clone.updateAlpha();
                clone.jump();
            }
            if (!(mob instanceof Tengu)){
                mob.resetEnemy();
            }
        }
        TagAttack.target(null);
        TagAttack.updateState();
    }

    @Override
    public float attackDelay(){
	    float delay = super.attackDelay();
        if (clonePhase && Dungeon.hero.isAlive()
         && Dungeon.hero.cooldown() < 1) {
            return Dungeon.hero.cooldown();
        }
        return delay;

    }

    @Override
    public void onAttackComplete(){
        if (clonePhase) {
            //super.onAttackComplete();
            attack(enemy);
            //jumpClones();
            jump(false, false);
            next();
        }else super.onAttackComplete();
    }

    @Override
    public void damage( int dmg, Object src, Element type ) {

        if (HP <= 0) {
            return;
        }

        super.damage( dmg, src, type );

        if (clonePhase) {
            damagedThisTurn = true;
        }

    }

    @Override
    public int defenseProc( Char enemy, int damage, boolean blocked ) {
        timeToJump++;
        if (breaks == 3)
            timeToJump++;
        return super.defenseProc(enemy, damage,blocked);
    }

    @Override
    public boolean add( Buff buff ) {
        if (clonePhase && !(buff instanceof PinCushion))
            return false;
        else
            return super.add(buff);
    }

    @Override
    public boolean act() {

        activeSidewallTraps.clear();
        if (clonePhase){

            clonesDuration--;
            if (clonesDuration <1) {
                killClones();
            }else
                if (damagedThisTurn) {
                    damagedThisTurn=false;
                    jump(true, true);
                    return true;
                }
            damagedThisTurn=false;
            return super.act();
        }

        if( 3 - breaks > 4 * HP / HT && !clonePhase ) {
            breaks++;
            if (breaks!=3){
                clonePhase=true;
                int numberOfClones=2;
                clonesDuration =breaks*2+3;
                for (int i = 0 ; i < numberOfClones ; i++) {
                    TenguClone clone = new TenguClone();
                    clone.pos = pos;
                    clone.state=clone.HUNTING;
                    clone.enemy=enemy;
                    clone.enemySeen=enemySeen;
                    clone.alerted=alerted;
                    Dungeon.level.press(clone.pos, clone);
                    GameScene.add(clone);
                    clone.notice();
                }
                jump(false, true);
            }
            return true;

        }

        return super.act();
    }
	
	private void jump(boolean clearClones, boolean spendTime) {

        timeToJump = 0;
        Buff.detach(this, PinCushion.class);
        Buff.detach(this, Ensnared.class);

        if (clonePhase)
            jumpClones(clearClones);

        int newPos;

        do{
            newPos = Dungeon.level.randomRespawnCell( false, true );
        } while( Level.adjacent( pos, newPos ) ||
                ( enemy != null && Level.distance( newPos, enemy.pos ) < 3) );

        sprite.move( pos, newPos );
        move( newPos, false );
        clearBlobs(newPos);

        if (!clonePhase && Dungeon.hero.isAlive()) {
            spawnTraps();
            amountOfJumps++;
            if ((breaks == 0 || breaks == 3) && amountOfJumps%2==0){
                GameScene.flash(0x80FFFFFF);
                Sample.INSTANCE.play(Assets.SND_RAY,0.5f, 0.5f, 1.5f);
                CellEmitter.get(Dungeon.hero.pos).start( Speck.factory( Speck.LIGHT ), 0.02f, 4  );
                BuffActive.add(Dungeon.hero, Blinded.class, 3);
            }else if (breaks != 0 ) {
                HashSet<Integer> verticalTraps = new HashSet();
                HashSet<Integer> horizontalTraps = new HashSet();
                for (int i = 0; i < 4; i++) {
                    int trapPos;
                    do {
                        trapPos = Dungeon.level.randomRespawnCell(false, true);
                    } while (trapPos / Level.WIDTH == newPos / Level.WIDTH || trapPos % Level.WIDTH == newPos % Level.WIDTH ||
                            //cant be in th middle of the room (door pos)
                            trapPos % Level.WIDTH == Level.WIDTH/2 ||
                            //cant be on same spot as other trap
                            horizontalTraps.contains(trapPos / Level.WIDTH) || verticalTraps.contains(trapPos % Level.WIDTH));

                    if (Random.Int(2) == 0) {
                        verticalTraps.add(trapPos % Level.WIDTH);
                        chargeVerticalSpear(trapPos);
                    } else {
                        horizontalTraps.add(trapPos / Level.WIDTH);
                        chargeHorizontalSpear(trapPos);
                    }
                }
                //wait one additional round until traps are fired
                spend(TICK);
                timeToJump++;
            }
        }

        if (Dungeon.visible[newPos] && Dungeon.hero.buff(Blinded.class) != null) {
            CellEmitter.get( newPos ).burst( Speck.factory( Speck.WOOL ), 6 );
            Sample.INSTANCE.play( Assets.SND_PUFF );
        }

		if (spendTime) {
		    if (clonePhase && Dungeon.hero.isAlive())
		        //always act after the hero
                spend( Dungeon.hero.cooldown());
		    else
                spend(1 / moveSpeed());
        }

	}

	private void spawnTraps(){
        disarmTraps();
        for (int i = 0; i < 6 + breaks * 2; i++) {
            int trapPos;
            do {
                trapPos = Dungeon.level.randomRespawnCell(false, true);
            } while (Dungeon.level.map[trapPos] != Terrain.INACTIVE_TRAP || Actor.findChar(trapPos) != null);

            if (Dungeon.level.map[trapPos] == Terrain.INACTIVE_TRAP) {

                Level.set(trapPos, Terrain.TRAP);
                Dungeon.level.setTrap(new GrippingTrap().reveal(), trapPos);
                ScrollOfClairvoyance.discover(trapPos);
            }
        }
    }

	private void disarmTraps(){
        for (Trap t :Dungeon.level.traps.values()){
            if (t.active)
             t.disarm();
        }
    }
	
	@Override
	public void notice() {
		super.notice();
        if( enemySeen ) {
            spawnTraps();
            yell( "Gotcha, " + Dungeon.hero.heroClass.title() + "!" );
        }
	}

	private void chargeVerticalSpear(int pos){
        SpearsTrap trap = new SpearsTrap();
        int topWall = Ballistica.cast(pos, pos-Level.WIDTH, true, false)-Level.WIDTH;
        int bottomWall = Ballistica.cast(pos, pos+Level.WIDTH, true, false)+Level.WIDTH;
        trap.activateAtPos(pos, topWall, bottomWall);
        activeSidewallTraps.add(trap);
    }

    private void chargeHorizontalSpear(int pos){
        SpearsTrap trap = new SpearsTrap();
        int leftWall = Ballistica.cast(pos, pos-1, true, false) -1;
        int rightWall = Ballistica.cast(pos, pos+1, true, false) +1;
        trap.activateAtPos(pos, leftWall, rightWall);
        activeSidewallTraps.add(trap);
    }


	private static void clearBlobs(int pos){
        Blob[] blobs = {
                Dungeon.level.blobs.get( Fire.class ),
                Dungeon.level.blobs.get( CorrosiveGas.class ),
                Dungeon.level.blobs.get( ConfusionGas.class ),
                Dungeon.level.blobs.get( FrigidVapours.class ),
                Dungeon.level.blobs.get( Web.class ),
                Dungeon.level.blobs.get( Electricity.class ),
                Dungeon.level.blobs.get( Miasma.class ),
        };

        for (Blob blob : blobs) {

            if (blob == null) {
                continue;
            }

            int value = blob.cur[pos];
            if (value > 0) {
                blob.cur[pos] -= value;
                blob.volume -= value;
            }
        }

    }

    @Override
    public float awareness(){
        return 2.0f;
    }

    @Override
    public void die( Object cause, Element dmg ) {

//		Badges.Badge badgeToCheck = null;
//		switch (Dungeon.hero.heroClass) {
//		case WARRIOR:
//			badgeToCheck = Badge.MASTERY_WARRIOR;
//			break;
//		case SCHOLAR:
//			badgeToCheck = Badge.MASTERY_SCHOLAR;
//			break;
//		case BRIGAND:
//			badgeToCheck = Badge.MASTERY_BRIGAND;
//			break;
//		case ACOLYTE:
//			badgeToCheck = Badge.MASTERY_ACOLYTE;
//			break;
//		}
//		if (!Badges.isUnlocked( badgeToCheck )) {
//			Dungeon.bonus.drop( new TomeOfMastery(), pos ).sprite.drop();
//		}

        disarmTraps();

        Dungeon.level.drop( new TomeOfMastery(), pos ).sprite.drop();

        GameScene.bossSlain();
        Dungeon.level.drop( new SkeletonKey(), pos ).sprite.drop();
        super.die( cause, dmg );

        Badges.validateBossSlain();

        killClones();

        yell( "Free at last..." );
    }

    public void killClones(){
        for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
            if (mob instanceof TenguClone) {
                mob.die(null);
            }
        }
        clonePhase=false;
        damagedThisTurn=false;
    }
	
	@Override
	public String description() {
		return
			"Tengu are members of the ancient assassins clan, which is also called Tengu. " +
			"These assassins are noted for extensive use of shurikens, traps and illusions.";/*+
            "Rumors tell that this prisoner was one of the most dangerous assassin of the clan, "+
            "you can tell by the look in his eyes that its sanity has been lost long ago...";*/

	}

    private static final String JUMPS_AMOUNT	= "jumpsAmount";
    private static final String TIME_TO_JUMP	= "timeToJump";
    private static final String BREAKS	= "breaks";
    private static final String CLONES	= "clones";
    private static final String CLONES_DURATION = "clonesDuration";
    private static final String ACTIVE_SIDEWALL_TRAPS = "activeSidewallTraps";


    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle(bundle);
        bundle.put( BREAKS, breaks );
        bundle.put( JUMPS_AMOUNT, timeToJump );
        bundle.put( TIME_TO_JUMP, amountOfJumps );
        bundle.put( CLONES, clonePhase );
        bundle.put(CLONES_DURATION, clonesDuration);
        bundle.put( ACTIVE_SIDEWALL_TRAPS, activeSidewallTraps );
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle(bundle);
        breaks = bundle.getInt( BREAKS );
        clonePhase = bundle.getBoolean( CLONES );
        timeToJump = bundle.getInt( TIME_TO_JUMP );
        timeToJump = bundle.getInt( JUMPS_AMOUNT );
        clonesDuration = bundle.getInt(CLONES_DURATION);
        Collection<Bundlable>  collection = bundle.getCollection( ACTIVE_SIDEWALL_TRAPS );
        for (Bundlable p : collection) {
            Trap trap = (Trap)p;
            activeSidewallTraps.add(trap);
        }

    }

    public static class TenguClone extends MobRanged {

        public int revealed=0;
        public TenguClone() {
            super(3,0,false );

            HT=originalTengu().HT;
            HP=originalTengu().HP;
            name = Dungeon.depth == Statistics.deepestFloor ? "Tengu" : "memory of Tengu";
            spriteClass = TenguSprite.class;
            armorClass=0;

            resistances.put(Element.Mind.class, Element.Resist.IMMUNE);
            resistances.put(Element.Body.class, Element.Resist.IMMUNE);

        }

        private static final String REVEALED	= "revealed";

        @Override
        public int dexterity() {
            return 0;
        }

        public boolean ignoresMissiles(){
            return true;
        }

        @Override
        public void storeInBundle( Bundle bundle ) {
            super.storeInBundle(bundle);
            bundle.put( REVEALED, revealed );
        }

        @Override
        public void restoreFromBundle( Bundle bundle ) {
            super.restoreFromBundle(bundle);
            revealed = bundle.getInt( REVEALED );
        }

        public void updateAlpha(){
            if (revealed>0 || Dungeon.hero!=null && Dungeon.hero.hasBuff(MindVision.class) ){
                sprite.alpha(0.4f);
            }else
                sprite.alpha(1);
        }

        public void reveal(){
            revealed=2;
            updateAlpha();
        }

        @Override
        public boolean act() {
            spend( TICK );
            return true;
        }

        @Override
        public void damage( int dmg, Object src, Element type ) {
            reveal();
        }

        @Override
        public boolean add( Buff buff ) {
            if (buff.awakensMobs())
                reveal();
            return false;
        }

        @Override
        public boolean isMagical() {
            return true;
        }


        public void jump() {

            int newPos;
            Char enemy=originalTengu().enemy;
            do{
                newPos = Dungeon.level.randomRespawnCell( false, true );
            } while( Level.adjacent( pos, newPos ) ||
                    ( enemy != null && Level.adjacent( newPos, enemy.pos ) ) );

            HP=originalTengu().HP;
            sprite.move( pos, newPos );
            move( newPos, false );

            clearBlobs(newPos);

            if (Dungeon.visible[newPos]) {
                CellEmitter.get( newPos ).burst( Speck.factory( Speck.WOOL ), 6 );
            }

        }

        @Override
        public void die( Object cause, Element dmg ) {
            if (Dungeon.visible[pos])
                CellEmitter.get( pos ).burst( Speck.factory( Speck.WOOL ), 6 );
            destroy();
            sprite.kill();
        }

        @Override
        public String description() {
            Tengu tengu =originalTengu();
            return tengu.description()+ (revealed > 0 ? "/n/nClearly, this one is a mirror image of the real one. ":"");
        }
    }
}
