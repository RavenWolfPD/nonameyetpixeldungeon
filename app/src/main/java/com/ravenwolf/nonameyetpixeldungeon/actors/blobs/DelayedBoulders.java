/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.blobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Chilled;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Dazed;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Disrupted;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Mob;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.BArray;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.BlobEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.MagicMissile;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ElmoParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.PurpleParticle;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Bundle;
import com.watabou.utils.Random;

import java.util.Arrays;


public class DelayedBoulders extends Blob {


    public DelayedBoulders() {
        super();
        name = "Falling boulder";
    }

    @Override
    public void spend( float time ) {
        super.spend( time );
    }

    @Override
    public int actingPriority(){
        return CHAR_ACTING_PRIORITY + 1;
    }

    @Override
    protected void evolve() {

        Char ch;
        boolean heard=false;

        //Drop Boulders
        for (int i = 0; i < LENGTH; i++) {
            if (cur[i] >0) {

                if ((ch = Actor.findChar(i)) != null) {
                    int effect = cur[i];
                    ch.damage(ch.absorb(Random.Int(effect *2/3, effect), false), this, Element.PHYSICAL);
                    if (ch.isAlive() ) {
                        BuffActive.add(ch, Dazed.class, 3);
                    }
                }

                Heap heap = Dungeon.level.heaps.get(i);
                if (heap != null) {
                    heap.shatter( "Boulders" );
                }
                Dungeon.level.press(i, null);

                if (Dungeon.visible[i]) {
                    CellEmitter.get(i).start( Speck.factory(Speck.ROCK), 0.1f, 4 );
                    heard = true;
                }
                cur[i] = 0;
            }
        }
        if (heard)
            Sample.INSTANCE.play(Assets.SND_ROCKS,0.5f, 0.5f, 1.5f);
    }


    @Override
    public void use( BlobEmitter emitter ) {
        super.use( emitter );
        emitter.pour(MagicMissile.EarthParticle.FALLING, 0.2f );
    }

    @Override
    public String tileDesc() {
        return "Loose rocks are tumbling down from the ceiling here, it looks like its about to collapse!";
    }

}