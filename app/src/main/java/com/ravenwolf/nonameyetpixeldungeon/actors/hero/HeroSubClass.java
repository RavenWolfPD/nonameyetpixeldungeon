/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.hero;

import com.watabou.utils.Bundle;

public enum HeroSubClass {

	NONE( null, null ),
	

	KNIGHT( "knight",
			"The _Knight_ is a sturdy combatant that excels using defencive stances and counter attacks. " +
					"While guarding the knight will have 25% more chance to hit and armor provided by shield. " +
					"Additionally, counter attacks will deal 25% extra damage. " +
					//"Guard duration is increased by one turn and counter attacks deal 25% extra damage. " +
					"\n_Relentless assault bonus:_ perform an additional strike and activate guard." ),
	BERSERKER( "berserker",
			"The _Berserker_ is a fierce warrior that will enters a state of frenzy when receiving large wounds, " +
					"increasing its physical damage. Chances and grow rate or Frenzy rises the lower the health of the hero." +
					"\n_Relentless assault bonus:_ the last strike will deal 33% additional damage and will damage enemies around the main target." ),

	CONJURER( "conjurer",
			"_Conjurers_ are masters of controlling arcane sources and magical beings. When zapping wands or charms " +
					"the conjurer will imbue itself and their allies with a protective barrier based on spent charges. " +
					"They also receive a 10% bonus in willpower." +
					"\n_Overload bonus:_ increase overload duration, and boost the efficiency of rings by 20% when overloaded." ),
	BATTLEMAGE( "battlemage",
			"The _Battlemage_ specialize on combining wands zaps with physical attacks. Enemies struck by wand zaps will be marked, " +
					" revealing them and reducing their defencive capabilities. The battlemage can also empower its wands with momentum generated by melee attacks, " +
					"increasing their damage by 10% per combo point." +
					"\n_Overload bonus:_ while overloaded melee attacks will deal bonus energy damage." ),

	ASSASSIN( "assassin",
					   "_Assassins_ are masters of stealth and guile, skilled at killing in shadows both with melee and ranged weapons. " +
							   "Its sneak attacks will deal 25% more damage and wont alert nearby enemies. " +
							   "\n_Shadow strike bonus:_ deal additional damage based on enemy missing health." ),
	SLAYER( "slayer",
						"Due to its lightning reflexes the _Slayer_, have chances to expose enemies after successfully dodging a melee strike, " +
						"and its evasion wont reduced when surrounded or near impassable terrain. " +
								"Combo points will also increase critical chance of his attacks." +
						"\n_Shadow strike bonus:_ strike up to four enemies near the main target dealing 66% weapon damage." ),

	SNIPER( "ranger",
			"_Rangers_ specializes almost exclusively on ranged combat, they can see further in the darkness of the dungeon, becoming very deadly at distance. " +
					"Their sight range is increased by 1 tile, and have 10% more chances to score critical hits with ranged weapons." +
					" \n_Spectral blades bonus:_ hurls 2 additional blades and every enemy in the area can be struck twice." ),
	AMAZON( "amazon",
/*			"The _Amazon_ is a versatile warrior that can deadly bot in melee and ranged combat. Taking advantage of shields and defencive tactics. " +
					"While guarding and not moving, the amazon will automatically attack any enemy in reach of its melee weapon." +
					"and when equipping a shield will activate guard stance" +
					" \n_Spectral blades bonus:_ While marked, enemies will receive 33% more damage from melee attacks." );*/
			"The _Amazon_ is a versatile warrior that can quickly switch from ranged to melee combat. Taking advantage of shields and defencive tactics. " +
					"Guard duration is increased by one turn, and when equipping a shield the amazon will automatically activate guard." +
					" \n_Spectral blades bonus:_ While marked, enemies will receive 33% more damage from melee attacks." );
	
	private String title;
	private String desc;
	
	HeroSubClass(String title, String desc) {
		this.title = title;
		this.desc = desc;
	}
	
	public String title() {
		return title;
	}
	
	public String desc() {
		return desc;
	}
	
	private static final String SUBCLASS	= "subClass";
	
	public void storeInBundle( Bundle bundle ) {
		bundle.put( SUBCLASS, toString() );
	}
	
	public static HeroSubClass restoreInBundle( Bundle bundle ) {
		String value = bundle.getString( SUBCLASS );
		try {
			return valueOf( value );
		} catch (Exception e) {
			return NONE;
		}
	}
	
}
