/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.Journal;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.FetidRat;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.GnollTrickster;
import com.ravenwolf.nonameyetpixeldungeon.items.Generator;
import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.items.armours.Armour;
import com.ravenwolf.nonameyetpixeldungeon.items.armours.body.BodyArmorCloth;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.DriedRose;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.RatSkull;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.Weapon;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.throwing.ThrowingWeapon;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.SewerLevel;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.Utils;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.GhostSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.windows.WndQuest;
import com.ravenwolf.nonameyetpixeldungeon.visuals.windows.WndSadGhost;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Bundle;
import com.watabou.utils.Random;

public class Ghost extends NPC {

	{
		name = "sad ghost";
		spriteClass = GhostSprite.class;
		
		flying = true;
		
		state = WANDERING;
	}


	private static String[][] QUEST_TXT = {

			{
				"Hello %s... Once I was like you - strong and confident... But I was slain by a foul beast... I can't leave this place... Not until I have my revenge... Slay the _fetid rat,_ that has taken my life...\n\nIt stalks this floor... Spreading filth everywhere... _Beware its cloud of stink and corrosive bite..._",
				"Hello %s... Once I was like you - strong and confident... And now I'm dead... I can't leave this place... Not until I have my _dried rose_... A _gnoll trickster_ stole it from my body...\n\nIt is not like the other gnolls... It is very sneaky... _Beware its ranged attacks..._",
			},
			{
				"Please... Help me... Slay the abomination...",
				"Please... Help me... Recover the rose...",
			}
	};


	
	public Ghost() {
		super();

		Sample.INSTANCE.load(Assets.SND_GHOST);
	}

	@Override
	public boolean fallIfDieOnChasm() {
		return false;
	}

	@Override
	public boolean invulnerable(){
		return true;
	}

	@Override
	protected boolean act() {
		if (Dungeon.hero != null && (Dungeon.hero.belongings.getItem( DriedRose.class ) != null)
		|| Dungeon.hero.belongings.getItem( RatSkull.class ) != null){
			target = Dungeon.hero.pos;
		}
		return super.act();
	}

    private void flee() {
        int newPos = -1;
        for (int i=0; i < 10; i++) {
            newPos = Dungeon.level.randomRespawnCell();
            if (newPos != -1) {
                break;
            }
        }
        if (newPos != -1) {

            Actor.freeCell( pos );

            CellEmitter.get( pos ).start( Speck.factory( Speck.LIGHT ), 0.2f, 3 );
            pos = newPos;
            sprite.place( pos );
            sprite.visible = Dungeon.visible[pos];
        }
    }
	

    @Override
    public boolean isMagical() {
        return true;
    }

    @Override
    public boolean immovable() {
        return true;
    }
	
	@Override
	public float moveSpeed() {
		return 0.5f;
	}
	
	@Override
	protected Char chooseEnemy() {
		return null;
	}
	
	@Override
    public void damage( int dmg, Object src, Element type ) {
        flee();
    }
	
	@Override
	public boolean add( Buff buff ) {
        flee();
        return false;
    }
	
	@Override
	public boolean reset() {
		return true;
	}
	
	@Override
	public void interact() {
		sprite.turnTo( pos, Dungeon.hero.pos );
		
		Sample.INSTANCE.play( Assets.SND_GHOST );
		
		if (Quest.given) {
			
			Item item = Quest.type == 0 ?
				Dungeon.hero.belongings.getItem( RatSkull.class ) :
				Dungeon.hero.belongings.getItem( DriedRose.class );	
			if (item != null) {
				GameScene.show( new WndSadGhost( this, item ) );
			} else {
				GameScene.show( new WndQuest( this,  QUEST_TXT[1][Quest.type] ) );

                flee();
			}
			
		} else {
			GameScene.show( new WndQuest( this, Utils.format(QUEST_TXT[0][Quest.type], Dungeon.hero.className())));
			Quest.given = true;

			int mobPos = -1;
			do {
				mobPos = Dungeon.level.randomRespawnCell(false, false);
			} while (mobPos == -1);

			if ( Quest.type == 0){
				FetidRat rat = new FetidRat();
				rat.pos = mobPos;
				GameScene.add(rat);
			}
			else {
				GnollTrickster gnoll = new GnollTrickster();
				gnoll.pos = mobPos;
				GameScene.add(gnoll);
			}
			
			Journal.add( Journal.Feature.GHOST );
		}
	}


	
	@Override
	public String description() {
		return 
			"The ghost is barely visible. It looks like a shapeless " +
			"spot of faint light with a sorrowful face.";
	}
	

	public static class Quest {

		private static boolean spawned;
		public static int type;
		private static boolean given;
		private static boolean completed;
		private static int depth;

		public static Weapon weapon;
		public static Armour armor;
		
		public static void reset() {
			spawned = false;
            completed = false;
			weapon = null;
			armor = null;
		}
		
		private static final String NODE		= "sadGhost";
		
		private static final String SPAWNED		= "spawned";
		private static final String TYPE		= "type";
        private static final String GIVEN		= "given";
        private static final String COMPLETED	= "completed";
        private static final String DEPTH		= "depth";
		private static final String WEAPON		= "weapon";
		private static final String ARMOR		= "armor";
		
		public static void storeInBundle( Bundle bundle ) {
			
			Bundle node = new Bundle();
			
			node.put( SPAWNED, spawned );
			
			if (spawned) {

				node.put( TYPE, type );
				node.put( GIVEN, given );
				node.put( DEPTH, depth );
                node.put( COMPLETED, completed );

				node.put( WEAPON, weapon );
				node.put( ARMOR, armor );
			}
			
			bundle.put( NODE, node );
		}
		
		public static void restoreFromBundle( Bundle bundle ) {
			
			Bundle node = bundle.getBundle( NODE );
			
			if (!node.isNull() && (spawned = node.getBoolean( SPAWNED ))) {

				type	= node.getInt( TYPE );
				given	= node.getBoolean( GIVEN );
				depth	= node.getInt( DEPTH );
                completed	= node.getBoolean( COMPLETED );
                given	= node.getBoolean( GIVEN );

				weapon	= (Weapon)node.get( WEAPON );
				armor	= (Armour)node.get( ARMOR );
			} else {
				reset();
			}
		}
		
		public static void spawn( SewerLevel level ) {
			if (!spawned && Dungeon.depth > 1 && Random.Int( 6 - Dungeon.depth ) == 0 ) {
				
				Ghost ghost = new Ghost();
				do {
					ghost.pos = level.randomRespawnCell();
				} while (ghost.pos == -1 );
				level.mobs.add( ghost );
				Actor.occupyCell( ghost );
				
				spawned = true;

				completed = false;
				given = false;
				depth = Dungeon.depth;

				type = depth < 4 ? 0: 1;

				do {
					weapon = (Weapon) Generator.random(Generator.Category.WEAPON);
				} while (weapon instanceof ThrowingWeapon || weapon.lootChapter()+weapon.bonus < 2 || weapon.lootChapter()+weapon.bonus > 4/* || weapon.isCursed()*/);

				do {
					armor = (Armour)Generator.random( Generator.Category.ARMOR );
				} while (armor instanceof BodyArmorCloth || armor.lootChapter()+armor.bonus < 2 || armor.lootChapter()+armor.bonus > 4/* || armor.isCursed()*/ );

				weapon.identify();
				weapon.cursed=false;
				armor.identify();
				armor.cursed=false;

			}
		}

		
		public static void complete() {
			weapon = null;
			armor = null;

            completed = true;
			
			Journal.remove( Journal.Feature.GHOST );
		}

        public static boolean isCompleted() {
            return completed;
        }
	}

}
