/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Withered;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.SwarmSprite;
import com.watabou.utils.Random;

public class CarrionSwarm extends MobEvasive {

    public CarrionSwarm() {

        super( 5 );

        name = "carrion eater";
        spriteClass = SwarmSprite.class;

        flying = true;

        resistances.put(Element.Body.class, Element.Resist.PARTIAL);

	}

    @Override
    public String description() {
        return
                "The deadly swarm of flies buzzes angrily. " +
                "Their vile bite is infamous for spreading weakening diseases.";
    }

    @Override
    public int attackProc( Char enemy, int damage, boolean blocked ) {

        if( !blocked && Random.Int( 8 ) < tier ) {
            BuffActive.addFromDamage( enemy, Withered.class, damage * 2 );
        }

        return damage;
    }
}
