/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.actors.hero;


import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.special.skills.*;
import com.watabou.utils.Bundle;

public enum HeroSkill {

    NONE( null, null ),

    HEROIC_LEAP( "Heroic leap",
            "Leap towards a targeted location, slamming down to damage and stun all neighbouring enemies. " +
                    "Damage dealt is based on your max health.", HeroicLeap.class,1 ),
    WAR_CRY( "War cry",
            "Bellow a mighty war cry, alerting nearby enemies and inducing a state of wild fury that "+
                    "significantly increase your attack damage. The fury increases when receiving damage. ", WarCry.class,  0 ),
    RELENTLESS_ASSAULT( "Relentless assault",
            "A devastating attack with your melee weapon that hit an enemy several times in a row with exceptional accuracy and speed. " +
                    "While this attack cannot miss, the damage dealt will be reduced based on weapon's penalty.", Fury.class, 2 ),

    MOLTEN_EARTH( "Arcane fire",
            "Conjure the powers of arcane elements, immobilizing and burning nearby enemies with magical fire."
                    ,MolterEarthSkill.class,9 ),
    OVERLOAD( "Overload",
            "Imbue yourself with arcane energy, recharging and increasing power of your currently equipped wand or charm for a short period."
            , OverloadSkill.class,11 ),
    ARCANE_ORB( "Arcane Orb",
            "Summons an orb of pure energy that will chase enemies releasing waves of arcane energy that damage any surrounding foe. It can trigger enchants and glyphs " +
                    "of its owner weapon and armors when fighting. Its power is based on chapter and hero level." , SummonArcaneOrbSkill.class,10 ),

    SMOKE_BOMB( "Smoke bomb",
            "Throws down a smoke bomb making you invisible for some turns and blinking to target position. Nearby enemies will be blinded. " ,SmokeBomb.class,6),
    SHADOW_STRIKE( "Shadow strike",
            "Phases out into the shadows to perform a devastating sneak attack on an enemy. Damage dealt will be reduced based on weapon's penalty." +
                    "" ,ShadowStrike.class,8),
    CLOAK_OF_SHADOWS( "Cloak of shadow",
                    "Summon a dense fog that will follow you providing a constant concealment. You are able to see over it, but it will block vision for every other creature." , CloakOfShadowsSkill.class,7),

    SPECTRAL_BLADES( "Spectral blades",
            "Hurls a fan of spectral blades that damage and reveal enemies in a cone area. Revealed enemies will have its evasion and armor reduced at half." ,SpectralBladesSkill.class,3),
    REPEL( "Repel",
            "Release a blast of energy, pushing away any neighbouring enemy and gaining extraordinary speed for a short time.",ValkyrieSkill.class,4 ),
    SPIRIT_WOLF( "Spirit wolf",
                         "Summons a Spirit wolf familiar, that will chase and attack enemies. Due to its ethereal nature it will not block projectiles. It stats and health are based on chapter and hero level.", SummonSpiritWolfSkill.class,5 );
    /*
    THORN_SPITTER( "Thorn spitter",
                           "Summons a Thorn spitter, that will spit poisoned spikes at nearby enemies. Its power is based on chapter and hero level, receiving additional health " +
                           "when created on grass-covered areas.", SummonThornspitterSkill.class,5
    VALKYRIE__OLD( "Valkyrie",
                      "Call the blessing of the valkyries, receiving a protective shield and the ability to fly. Upon receiving the bless, neighbouring enemies will be pushed back.",ValkyrieSkill.class,4 ),

*/
    private String title;
    private String desc;
    private int icon;
    private Class<? extends BuffSkill> skillClass;

    HeroSkill(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }

    HeroSkill(String title, String desc, Class<? extends BuffSkill> skillClass, int icon) {
        this.title = title;
        this.desc = desc;
        this.skillClass= skillClass;
        this.icon= icon;
    }

    public String title() {
        return title;
    }

    public String desc() {
        return desc;
    }

    public int icon() {
        return icon;
    }

    public Class<? extends BuffSkill> skillClass() {
        return skillClass;
    }

    private static final String SKILL	= "skill";

    public void storeInBundle( Bundle bundle ) {
        bundle.put( SKILL, toString() );
    }

    public static HeroSkill restoreInBundle(Bundle bundle ) {
        String value = bundle.getString( SKILL );
        try {
            return valueOf( value );
        } catch (Exception e) {
            return NONE;
        }
    }
}
