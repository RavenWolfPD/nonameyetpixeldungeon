/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Fire;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Burning;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Chilled;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Ensnared;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.FellFire;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.BlazingElementalSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.CharSprite;
import com.watabou.utils.Random;

public class BlazingElemental extends MobPrecise {

    private static String TXT_ENSNARED = "Elemental burns its snares!";

    public BlazingElemental() {

        super( 16 );

        name = "fell elemental";
        spriteClass = BlazingElementalSprite.class;

        flying = true;
        armorClass = 0;

        resistances.put( Element.Shock.class, Element.Resist.PARTIAL );
        resistances.put( Element.Acid.class, Element.Resist.PARTIAL );

        resistances.put( Element.Body.class, Element.Resist.IMMUNE );
        resistances.put( Element.Flame.class, Element.Resist.IMMUNE );
        resistances.put( Element.Frost.class, Element.Resist.IMMUNE );

    }

    public boolean isMagical() {
        return true;
    }

    @Override
    public boolean isEthereal() {
        return true;
    }

    @Override
    public Element damageType() {
        return Element.ENERGY;
    }


    @Override
    protected boolean canAttack( Char enemy ) {
        return super.canAttack( enemy );
    }


    @Override
    public void damage( int dmg, Object src, Element type ) {

        if ( type == Element.FLAME || type == Element.FROST ) {

            if (HP < HT) {
                int reg = Math.min( dmg / 2, HT - HP );

                if (reg > 0) {
                    HP += reg;
                    sprite.showStatus(CharSprite.POSITIVE, Integer.toString(reg));
                    sprite.emitter().burst(Speck.factory(Speck.HEALING), (int) Math.sqrt(reg));
                }
            }

        } else {

            super.damage(dmg, src, type);

        }
    }
	
	@Override
	public boolean add( Buff buff ) {
        if (buff instanceof Ensnared) {
            if(Dungeon.visible[pos] ) {
                GLog.w( TXT_ENSNARED );
            }
            return false;
        } else {
            return super.add( buff );
        }
	}


    @Override
    public int defenseProc( Char enemy, int damage,  boolean blocked ) {

        if (Level.adjacent(enemy.pos, pos)) {
            BuffActive.addFromDamage(enemy, FellFire.class,damage/2);
        }
        return super.defenseProc(enemy,damage,blocked);
    }

	@Override
	public String description() {
		return
                "This kind of elementals are composed of spectral fire, a magical element that cannot be extinguished by regular means." +
                "Facing these magical entities at close distance is not a cleaver idea, as they splash their enemies when receiving damage. " +
                "Elementals are too chaotic in their nature to be controlled by even the most powerful demonologist.";

	}

}
