/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.buffs.special;

import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffReactive;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.BuffIndicator;

public class Focused extends BuffReactive {

    @Override
    public int icon() {
        return BuffIndicator.FOCUSED;
    }

    @Override
    public String toString() {
        return "Focused";
    }

//    @Override
//    public String statusMessage() { return "focused"; }
/*
    @Override
    public boolean attachTo( Char target ) {

        //Buff.detach( target, Combo.class);
        //Buff.detach( target, Guard.class );

        return super.attachTo( target );
    }
*/
    @Override
    public String description() {
        return "You spent a turn to evaluate your enemies and prepare for your next move. Your next attack will " +
                "be 50% more precise than usual. And you have 33% more chances to dodge or block incoming attacks";
    }


}
