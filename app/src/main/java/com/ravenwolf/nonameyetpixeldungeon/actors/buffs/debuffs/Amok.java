/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs;

import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Bestiary;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Mob;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.CharSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.BuffIndicator;
import com.watabou.utils.Bundle;

public class Amok extends Debuff {

    @Override
    public Element buffType() {
        return Element.MIND;
    }

    @Override
    public String toString() {
        return "Amok";
    }

    @Override
    public String statusMessage() { return "amok"; }

    @Override
    public String playerMessage() { return "You are amoked!"; }

    @Override
    public int icon() {
        return BuffIndicator.SOUL_LINK;
    }

    @Override
    public void applyVisual() {
        target.sprite.add( CharSprite.State.AMOKED );
    }

    @Override
    public void removeVisual() {
        target.sprite.remove( CharSprite.State.AMOKED );
    }

    @Override
    public String description() {
        return "Amok causes a state of great rage and confusion in its target." +
                " When a creature is amoked, they will attack whatever is near them, or even themselves.";
    }


}
