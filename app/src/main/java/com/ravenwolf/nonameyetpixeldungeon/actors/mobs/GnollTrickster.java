/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Dazed;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Debuff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Poisoned;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.HeroClass;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.DriedRose;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.CharSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.GnollTricksterSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.MissileSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.HealthIndicator;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.Random;

public class GnollTrickster extends MobRanged {

    private boolean dartAttack;
    private int blinks;

    private static final int ATTACK_RANGE = 6;

    public GnollTrickster() {

        super( 5 );
        minDamage+=tier;
        maxDamage+=tier;
        HP=HT+=tier*6;
        armorClass += tier;

        state = WANDERING;

		name = "gnoll trickster";
		spriteClass = GnollTricksterSprite.class;
        resistances.put(Element.Body.class, Element.Resist.PARTIAL);

       /* switch( Dungeon.chapter() ) {
            case 1:
                loot = Random.oneOf( Javelins.class, PoisonDarts.class, Knives.class);
                lootChance = 0.5f;
                break;
            case 2:
                loot = Random.oneOf(Bolas.class, Boomerangs.class, Shurikens.class);
                lootChance = 0.5f;
                break;
            case 3:
                loot = Random.oneOf(Harpoons.class, MoonGlaive.class, Tomahawks.class);
                lootChance = 0.5f;
                break;
            default:
                loot = Random.oneOf(Chakrams.class, Hammers.class);
                lootChance = 0.5f;
                break;
        }*/

	}

    @Override
    public int minAC() {
        return super.minAC()+1;
    }

    @Override
    public String getTribe() {
        return TRIBE_GNOLL;
    }

    @Override
    public float awareness(){
        return super.awareness() *  1.25f;
    }


    @Override
    public void die( Object cause, Element dmg ) {
        super.die( cause, dmg );
        Dungeon.level.drop( new DriedRose(), pos ).sprite.drop();
    }

    @Override
    protected void onRangedAttack(final int cell ) {
        if (Random.Float() > 0.66f ) {
            dartAttack = true;
            ((MissileSprite) sprite.parent.recycle(MissileSprite.class)).
                    reset(pos, cell, ItemSpriteSheet.THROWING_DART, new Callback() {
                        @Override
                        public void call() {
                            onAttackComplete();
                        }
                    });
        }else {
            ((MissileSprite) sprite.parent.recycle(MissileSprite.class)).
                    reset(pos, cell, ItemSpriteSheet.BOOMERANG, new Callback() {
                        @Override
                        public void call() {
                            onAttackComplete();
                            ((MissileSprite) sprite.parent.recycle(MissileSprite.class)).
                                    reset(cell, pos, ItemSpriteSheet.BOOMERANG, null);
                        }
                    });
        }

        super.onRangedAttack( cell );
    }

    @Override
    public int accuracy(){
        return super.accuracy() * (dartAttack ? 2 : 1);
    }

    @Override
    protected boolean getCloser( int target ) {
        if (state == HUNTING && (enemySeen || enemy != null && detected( enemy )) && Level.distance(pos, enemy.pos) < ATTACK_RANGE) {
            return getFurther( target );
        } else {
            return super.getCloser( target );
        }
    }

    @Override
    protected boolean isInAttackRange( Char enemy ) {
        return  ((!Level.adjacent( pos, enemy.pos )
                //or cannot run away
                || Dungeon.flee(this, pos, enemy.pos, Level.mob_passable, Level.fieldOfView) ==-1) &&
                Ballistica.cast( pos, enemy.pos, false, true ) == enemy.pos)
                && Ballistica.distance <= ATTACK_RANGE;
    }

    @Override
    public int attackProc(Char enemy, int damage, boolean blocked ) {
        if ( !blocked && !Level.adjacent(pos, enemy.pos)) {
            if (dartAttack) {
                BuffActive.add(enemy, Poisoned.class, 2);
                damage/=2;
            } else {
                if( Random.Int( 5 ) == 0 )
                    BuffActive.add(enemy, Dazed.class, 2);
            }
        }

        return damage;
    }

    public boolean attack( Char enemy, int damageRoll ){
        boolean hit = super.attack(enemy, damageRoll);
        dartAttack=false;
        return hit;
    }

    @Override
    public boolean act() {
        if ( HP <= HT / (2 + blinks) ) {

            blinks++;
            if (Dungeon.visible[pos])
                CellEmitter.get(pos).burst(Speck.factory(Speck.WOOL), 4);

            Debuff.removeAll( this );

            if (HealthIndicator.instance.target() == this)
                HealthIndicator.instance.target(null);

            if (blinks == 2){
                sprite.showStatus( CharSprite.NEGATIVE, TXT_RAGE );
                Dungeon.level.drop( new DriedRose(), pos ).sprite.drop();
                destroy();
                sprite.killAndErase();
                // dont use die to prevent showing defeat msg
                /*Dungeon.hero.earnExp(EXP);
                Dungeon.level.mobs.remove(this);
                Actor.remove(this);
                Actor.freeCell(pos);
                pos=0;
                HP = 0;
                sprite.killAndErase();*/
                return true;
            }

            int newPos = -1;
            do {
                newPos = Dungeon.level.randomRespawnCell(true, true);
            } while (newPos == -1);
            pos = newPos;
            //Recovers 25% HP
            HP+=HT/4;
            move( newPos, false );
            spend(TICK);
            return true;
        }

        return super.act();
    }


	@Override
	public String description() {

        return "This gnoll seems ver different from the ones you encounter before. Its eyes, hidden beneath the worn hood, reveal some evil cunning. " +
                "It carries a boomerang in its hands and you can see some darts hidden under its cloak";
	}

    private static final String BLINKS = "blinks";
    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle(bundle);
        bundle.put(BLINKS, blinks);
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle(bundle);
        blinks = bundle.getInt(BLINKS);
    }

}
