/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.Journal;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.CorpseDust;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.VoidStone;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.SpellPrism;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfWarden;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfBlastWave;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfShadows;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfThorns;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.Wand;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfAvalanche;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfBlink;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfDomination;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfDisintegration;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfFirebolt;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfFreezing;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfLightning;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfMagicMissile;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfAcidSpray;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfSanctuary;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfDecay;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfSmiting;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Room;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.Utils;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.WandmakerSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.windows.WndQuest;
import com.ravenwolf.nonameyetpixeldungeon.visuals.windows.WndWandmaker;
import com.watabou.utils.Bundle;
import com.watabou.utils.Random;
import com.watabou.utils.Rect;

import java.util.Collection;

public class Wandmaker extends NPC {

	{	
		name = "old wandmaker";
		spriteClass = WandmakerSprite.class;
	}

	private static final String TXT_INTRO	=
			"Oh, what a pleasant surprise to meet a decent person in such place!\n" +
			"Being a magic user, I'm quite able to defend myself against local monsters, but I'm getting lost in no time, it's very embarrassing. " +
			"I came here for a rare material, probably you could help me? I would be happy to pay for your service with one of my best wands.";

	private static String[][] QUEST_TXT = {
			{
				"I'm looking for some _corpse dust_. It can be gathered from skeletal remains and there is an ample number of them in the dungeon. There should be a barricaded room around here somewhere, I'm sure some dust will turn up there. Do be careful though, the curse the dust carries is quite potent, get back to me and I'll cleanse it for you.",
				"I'm looking for a _void stone_. A prisoner tried to use it to escape from here, but lack to knowledge to control it and was consumed by the void instead. Try to get rid of the void spawns before picking the stone, as they will follow the stone whenever it goes.",
				"I'm looking for a _spell prism_. It should be stored in a secured in a treasury vault here. Beware of the traps inside that chamber, as they are specifically designed to keep looters out, they only trigger when stepped and cannot be detected or defused like other traps here.",
			},
			{
				"Any luck with corpse dust, %s? Look for some barricades.",
				"Any luck with the void stone, %s? No? Don't worry, I'm not in a hurry.",
				"Any luck with the spell prism, %s? be careful where you step in.",
			}
	};

	
	@Override
	protected boolean act() {
		throwItem();
		return super.act();
	}

    @Override
    public boolean immovable() {
        return true;
    }

	@Override
	public boolean invulnerable(){
		return true;
	}
	
	@Override
    public void damage( int dmg, Object src, Element type ) {
	}
	
	@Override
    public boolean add( Buff buff ) {
        return false;
    }
	
	@Override
	public boolean reset() {
		return true;
	}
	
	@Override
	public void interact() {
		
		sprite.turnTo( pos, Dungeon.hero.pos );
		if (Quest.given) {

			Item item;
			switch (Quest.type) {
				case 0:
				default:
					item = Dungeon.hero.belongings.getItem(CorpseDust.class);
					break;
				case 1:
					item = Dungeon.hero.belongings.getItem(VoidStone.class);
					break;
				case 2:
					item = Dungeon.hero.belongings.getItem(SpellPrism.class);
					break;
			}
			if (item != null) {
				GameScene.show( new WndWandmaker( this, item ) );
			} else {
				tell( QUEST_TXT[1][Quest.type], Dungeon.hero.className() );
			}
			
		} else {
			GameScene.show(new WndQuest(Wandmaker.this,  TXT_INTRO){
				@Override
				public void hide() {
					super.hide();
					tell( QUEST_TXT[0][Quest.type]);
				}
			});
			Quest.given = true;
			Journal.add( Journal.Feature.WANDMAKER );
		}
	}
	
	private void tell( String format, Object...args ) {
		GameScene.show( new WndQuest( this, Utils.format( format, args ) ) );
	}
	
	@Override
	public String description() {
		return 
			"This old but hale gentleman wears a slightly confused " +
			"expression. He is protected by a magic shield.";
	}
	
	public static class Quest {
		
		private static boolean spawned;
		private static boolean completed;
		public static int type;
		// 0 = corpse dust quest
		// 1 = voidstone quest
		// 2 = spell prism quest

		private static boolean given;
		
		public static Wand wand1;
		public static Wand wand2;
		
		public static void reset() {
			spawned = false;

			wand1 = null;
			wand2 = null;
		}
		
		private static final String NODE		= "wandmaker";
		private static final String TYPE		= "type";
		
		private static final String SPAWNED		= "spawned";
		private static final String COMPLETED	= "completed";
		private static final String GIVEN		= "given";
		private static final String WAND1		= "wand1";
		private static final String WAND2		= "wand2";
		
		public static void storeInBundle( Bundle bundle ) {
			
			Bundle node = new Bundle();
			
			node.put( SPAWNED, spawned );
			
			if (spawned) {

				node.put( TYPE, type );
				node.put( COMPLETED, completed );

				node.put(GIVEN, given );
				
				node.put( WAND1, wand1 );
				node.put( WAND2, wand2 );
			}
			
			bundle.put( NODE, node );
		}
		
		public static void restoreFromBundle( Bundle bundle ) {

			Bundle node = bundle.getBundle( NODE );
			
			if (!node.isNull() && (spawned = node.getBoolean( SPAWNED ))) {

				type = node.getInt( TYPE );
                completed = node.getBoolean( COMPLETED );
				given = node.getBoolean( GIVEN );
				
				wand1 = (Wand)node.get( WAND1 );
				wand2 = (Wand)node.get( WAND2 );

			} else {
				reset();
			}
		}

		public static void spawn(Level level, Room room, Collection<Room> rooms ) {
			if (!spawned && Dungeon.depth > 7 && Random.Int( 12 - Dungeon.depth ) == 0) {

				Room prisonQuest =getQuestRoom(rooms, 6);

				if (prisonQuest == null)
					return;

				Wandmaker npc = new Wandmaker();
				//random corner
				npc.pos = (Random.Int(2)==0 ? room.left+1 :  room.right-1) + (Random.Int(2)==0 ? room.top+1 :  room.bottom-1) * Level.WIDTH;
				level.mobs.add( npc );
				Actor.occupyCell( npc );

				prisonQuest.type = Room.Type.PRISON_QUEST;

				spawned = true;
				type = Random.Int(3);

				given = false;
				setReward();
			}
		}

		private static Room getQuestRoom(Collection<Room> rooms, int minLength){
			//first check if there is a standard room with the needed criteria
			for (Room r : rooms) {
				if (r.type == Room.Type.STANDARD && r.connected.size() == 1
						&& r.width() > 4 && r.width() > 4)	{
					//check where rooms intersect, we want a room with at least 6 length
					//form the entrance to th opposite wall
					Room other =(Room)r.connected.keySet().toArray()[0];
					Rect i = r.intersect( other );
					if (i.width() == 0 && r.width() < minLength) {
						continue;
					} else if (r.height() < minLength) {
						continue;
					}
					return r;
				}
			}

			//try to find an unused room that fit the criteria
			for (Room r : rooms) {
				if (r.type == Room.Type.NULL & r.neighbours.size() > 1
						&& r.width() > 4 && r.width() > 4) {
					for (Room neighbour : r.neighbours) {
						if ((neighbour.type == Room.Type.STANDARD || neighbour.type == Room.Type.EXIT) && neighbour.neighbours.size() > 1){
							//check where rooms intersect, we want a room with at least 6 width
							//form the entrance to th opposite wall
							Rect i = r.intersect( neighbour );
							if (i.width() == 0 && r.width() < minLength) {
								continue;
							} else if (r.height() < minLength) {
								continue;
							}
							if (neighbour.neighbours.contains(r)) {
								neighbour.connect(r);
								r.connect(neighbour);
								return r;
							}
						}
					}
				}
			}
			//lastly try to replace a special room
			for (Room r : rooms) {
				if ((r.type == Room.Type.ARMORY || r.type == Room.Type.POOL
						|| r.type == Room.Type.VAULT || r.type == Room.Type.CRYPT)
						&& r.width() > 4 && r.width() > 4) {
					//check where rooms intersect, we want a room with at least 6 width
					//form the entrance to th opposite wall
					Room other =(Room)r.connected.keySet().toArray()[0];
					Rect i = r.intersect( other );
					if (i.width() == 0 && r.width() < minLength) {
						continue;
					} else if (r.height() < minLength) {
						continue;
					}
					return r;
				}
			}
			return null;
		}

		private static void setReward() {
			switch (Random.Int(7)) {
				case 0:
					wand1 = new WandOfAvalanche();
					break;
				case 1:
					wand1 = new WandOfDisintegration();
					break;
				case 2:
					wand1 = new WandOfFirebolt();
					break;
				case 3:
					wand1 = new WandOfLightning();
					break;
				case 4:
					wand1 = new WandOfSmiting();
					break;
				case 5:
					wand1 = new WandOfMagicMissile();
					break;
				case 6:
					wand1 = new WandOfAcidSpray();
					break;
				case 7:
					wand1 = new WandOfFreezing();
					break;
			}
			wand1.random().uncurse().upgrade();

			switch (Random.Int(7)) {
				case 0:
					wand2 = new CharmOfDomination();
					break;
				case 1:
					wand2 = new CharmOfBlink();
					break;
				case 2:
					wand2 = new CharmOfThorns();
					break;
				case 3:
					wand2 = new CharmOfShadows();
					break;
				case 4:
					wand2 = new CharmOfDecay();
					break;
				case 5:
					wand2 = new CharmOfSanctuary();
					break;
				case 6:
					wand2 = new CharmOfWarden();
					break;
				case 7:
					wand2 = new CharmOfBlastWave();
					break;
			}
			wand2.random().uncurse().upgrade();
		}


		public static void complete() {
			wand1 = null;
			wand2 = null;

            completed = true;

			Journal.remove( Journal.Feature.WANDMAKER );
		}

        public static boolean isCompleted() {
            return completed;
        }

		public static boolean hasSpawned() {
			return spawned;
		}
	}

}
