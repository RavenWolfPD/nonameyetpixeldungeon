/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Journal;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Wraith;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ElmoParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ShopkeeperEnchantressSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.windows.WndEnchantress;
import com.watabou.utils.Random;

public class Enchantress extends NPCSupplier {

    private static final String TXT_GREETINGS = "Hey, want to buy some magic?";
    private static final String TXT_OUT_OF_STOCK = "I can't conjure more scrolls right now, my spellbook is recharging ";

    private static String[][] LINES = {

            {
                    "Hey, cut it off.",
                    "Don't do that.",
                    "Stop it! Now!",
            },
            {
                    "You will regret that.",
            },
            {
                    "The dark arts will make you pay",
                    "That's not a smart move",
            },
    };


	{
		name = "enchantress";
		spriteClass = ShopkeeperEnchantressSprite.class;
	}

    public static void spawn(Level level ){
        Enchantress npc = new Enchantress();
        do {
            npc.pos = level.randomRespawnCell();
        } while (npc.pos == -1 || level.heaps.get( npc.pos ) != null || !level.NPCSafePos(npc.pos));

        npc.stock = 2 + Random.Int(3);
        level.mobs.add(npc);
        Actor.occupyCell( npc );
    }


    protected Journal.Feature feature(){
        return Journal.Feature.YOUNG_ENCHANTRESS;
    }

    protected String greetingsText(){
        return TXT_GREETINGS;
    }

    protected String[][] lines(){
        return LINES;
    }


	public void runAway() {
	    super.runAway();
        Wraith.spawnAround(pos, 3);
        CellEmitter.get(pos).start( ElmoParticle.FACTORY, 0.02f, 40 );
	}

	@Override
	public void interact() {
        sprite.turnTo( pos, Dungeon.hero.pos );
        if (outOfStock() )
            tell( TXT_OUT_OF_STOCK );
        else
            GameScene.show( new WndEnchantress( this ));
	}


    @Override
    public String description() {
        return
                "This seems a dangerous place for a young enchantress, but you can sense that the book she is holding " +
                    "have a tremendous power that protects her from any harm";
    }

}
