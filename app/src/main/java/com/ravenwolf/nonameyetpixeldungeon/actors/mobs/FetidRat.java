/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.CorrosiveGas;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Bleeding;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Corrosion;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.RatSkull;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.AlbinoSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.FetidRatSprite;
import com.watabou.utils.Random;

public class FetidRat extends MobEvasive {

	{
		name = "fetid rat";
		spriteClass = FetidRatSprite.class;
	}

	public FetidRat() {
		super( Dungeon.chapter(), 4, false );
		minDamage += tier;
		maxDamage += tier;
		HP=HT+=tier * 8;
		armorClass += tier;
		state = WANDERING;

		resistances.put(Element.Acid.class, Element.Resist.PARTIAL);
		resistances.put(Element.Body.class, Element.Resist.PARTIAL);
	}

	@Override
	public int minAC() {
		return super.minAC()+1;
	}

	@Override
	public void die( Object cause, Element dmg ) {
		super.die( cause, dmg );

		Dungeon.level.drop( new RatSkull(), pos ).sprite.drop();
	}

	@Override
	public String getTribe() {
		return TRIBE_BEAST;
	}

	@Override
	public int attackProc(Char enemy, int damage, boolean blocked ) {
		if( !blocked && Random.Int( 5 ) == 0 ) {
			BuffActive.addFromDamage( enemy, Corrosion.class, damage*2 );
		}
		return damage;
	}


	@Override
	public int defenseProc( Char enemy, int damage,  boolean blocked ) {
		GameScene.add( Blob.seed( pos, 20, CorrosiveGas.class ) );
		return super.defenseProc(enemy, damage, blocked);
	}

	@Override
	public boolean add( Buff buff ) {
		if (buff instanceof Corrosion)
			return false;
		return super.add(buff);

	}

	@Override
	public String description() {
		return "Something is clearly wrong with this rat. Its greasy black fur and rotting skin are very different from the healthy rats you've seen previously. Its pale green eyes make it seem especially menacing." +
				"\n\nThe rat carries a cloud of horrible stench with it, it's overpoweringly strong up close.\n\nDark ooze dribbles from the rat's mouth, it eats through the floor but seems to dissolve in water.. " ;
	}
}
