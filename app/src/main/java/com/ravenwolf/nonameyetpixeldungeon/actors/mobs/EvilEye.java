/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.DungeonTilemap;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.WandOfDisintegration;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.DeathRay;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.PurpleParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.EyeSprite;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Random;

public class EvilEye extends MobRanged {

    public EvilEye() {

        super( 11 );

		name = "evil eye";
		spriteClass = EyeSprite.class;
		
		flying = true;
        //loot = new MysteryMeat();
        //lootChance = 0.35f;

        resistances.put(Element.Energy.class, Element.Resist.PARTIAL);

	}

    public String getTribe() {
        return TRIBE_LONER;
    }

    @Override
	protected boolean getCloser( int target ) {
		if (state == HUNTING && HP <= HT/2 && (enemySeen || enemy != null && detected( enemy ))) {
			return getFurther( target );
		} else {
			return super.getCloser( target );
		}
	}

    @Override
    protected boolean isInAttackRange( Char enemy ) {
        return (Level.adjacent( pos, enemy.pos ) && HP > HT/2) || (!Level.adjacent( pos, enemy.pos ) &&
                Ballistica.cast( pos, enemy.pos, false, false ) == enemy.pos);
    }

    @Override
    protected void onRangedAttack( int cell ) {

        Sample.INSTANCE.play(Assets.SND_RAY);

        sprite.parent.add( new DeathRay( sprite.center(), DungeonTilemap.tileCenterToWorld( cell ) ) );

        onCastComplete();

        super.onRangedAttack( cell );

    }

    @Override
    public boolean cast( Char enemy ) {

        boolean terrainAffected = false;

        for (int i=1; i <= Ballistica.distance ; i++) {

            int pos = Ballistica.trace[i];

            boolean affected = WandOfDisintegration.affectCell( pos);
            if (affected) {
                terrainAffected = true;
            }

            Char ch = Actor.findChar( pos );

            if (ch != null) {
                if (castBolt(ch , damageRoll(), false, Element.ENERGY  )){
                    if (Dungeon.visible[pos]) {
                        CellEmitter.center(pos).burst(PurpleParticle.BURST, Random.IntRange(1, 2));
                    }

                }
            }
        }

        if (terrainAffected) {
            Dungeon.observe();
        }

        return true;
    }

	
	@Override
	public String description() {
		return
			"One of this creature's other names is \"orb of hatred\", because when it sees an enemy, " +
			"it uses its deathgaze recklessly, often ignoring its allies and wounding them.";
	}
}
