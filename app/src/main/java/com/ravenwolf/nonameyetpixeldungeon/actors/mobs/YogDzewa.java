/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.Statistics;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.ConfusionGas;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.CorrosiveGas;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Darkness;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.DisruptionField;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Fire;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.bonuses.Shielding;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Burning;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Chilled;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Corrosion;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Dazed;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Debuff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Shocked;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.special.PinCushion;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.special.SoulLink;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.special.UnholyArmor;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.items.keys.SkeletonKey;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.CharmOfBlink;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.YogBossLevel;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.BArray;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.BlastWave;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.MagicMissile;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ElmoParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ShadowParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.BurningFistSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.CharSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.RottingFistSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.YogSprite;
import com.watabou.noosa.Camera;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.PathFinder;
import com.watabou.utils.Random;

import java.util.ArrayList;
import java.util.HashMap;

public class YogDzewa extends MobHealthy {

    private int attackCD = 10;
    private int specialCD = -20; ///special value for initial state
    protected int breaks = 0;
    protected int phases = 2;
    //private ArrayList<YogAspect> aspects;
    private YogAspect currentAspect;

	{
		name = Dungeon.depth == Statistics.deepestFloor ? "Yog-Dzewa" : "echo of Yog-Dzewa";
		spriteClass = YogSprite.class;
	}

    public YogDzewa() {
        super(6, 40, true);
        maxDamage -= tier;

        PASSIVE = new Guarding();
        state = PASSIVE;
        //aspects= randomizeAspects(new YogShadowAspect(), new YogChaosAspect(), new YogDestructionAspect());
        currentAspect = new YogPassiveAspect();

        resistances.put( Element.Mind.class, Element.Resist.IMMUNE );
        resistances.put( Element.Body.class, Element.Resist.IMMUNE );
        resistances.put( Element.Dispel.class, Element.Resist.IMMUNE );
    }


    private static YogDzewa getYog() {
        for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
            if (mob instanceof YogDzewa) {
                return (YogDzewa)mob;
            }
        }
        return null;
    }

	private static final String TXT_DESC =
		"Yog-Dzewa is an Old God, a powerful entity from the realms of chaos. A century ago, the ancient dwarves " +
		"barely won the war against its army of demons, but were unable to kill the god itself. Instead, they then " +
		"imprisoned it in the halls below their city, believing it to be too weak to rise ever again.";

    @Override
    protected boolean canBeBackstabed(){
        return false;
    }

    @Override
    public boolean ignoreDarkness() {
        return true;
    }

    @Override
    public float awareness(){
        return 2.0f;
    }

    @Override
    protected float healthValueModifier() { return 0.25f; }

    @Override
    public boolean immovable(){
        return true;
    }


	private int[] getThrones(){
       return YogBossLevel.ALTARS;
    }

    public void initPhase() {
        HP = HT;
        breaks = 0;
        if (phases ==1)
            currentAspect= new YogDestructionAspect();
        else
            currentAspect=  Random.oneOf(new YogShadowAspect(), new YogChaosAspect());
        //currentAspect= Random.element(aspects);
        //aspects.remove(currentAspect);
        Buff.detach(this, UnholyArmor.class);
        currentAspect.init();
    }


    public  ArrayList<YogAspect>  randomizeAspects(YogAspect ...aspects  ) {
        ArrayList<YogAspect> availableAspects=new ArrayList<>();

        while (aspects.length != availableAspects.size()) {
            YogAspect next = Random.element(aspects);
            if (!availableAspects.contains(next))
                availableAspects.add(next);
        }
        return availableAspects;
    }


    private void spawnVoidTendrils(int amount, int maxDist){
        ArrayList<Integer> nearby = new ArrayList<>();
        PathFinder.buildDistanceMap( pos, BArray.not( Dungeon.level.solid, null ), maxDist );
        for (int i = 0; i < PathFinder.distance.length; i++) {
            if (PathFinder.distance[i] < Integer.MAX_VALUE && Dungeon.level.passable[i]) {
                if ( Actor.findChar(i) == null) {
                    nearby.add(i);
                }
            }
        }

        int summons = Math.min(amount, nearby.size());
        for  (int i = 0; i < summons; i++){
            Integer pos =Random.element(nearby);
            VoidTendril mob = VoidTendril.spawnAt(pos);
            mob.spend(TICK);
            //damaging tentacles damage yog
            SoulLink soullink = Buff.affect(mob, SoulLink.class);
            soullink.object = this.id();
            nearby.remove(pos);
        }
    }

    private void spawnTentacles(int amount, int maxDist){
        ArrayList<Integer> nearby = new ArrayList<>();
        PathFinder.buildDistanceMap( pos, BArray.not( Dungeon.level.solid, null ), maxDist );
        for (int i = 0; i < PathFinder.distance.length; i++) {
            if (PathFinder.distance[i] < Integer.MAX_VALUE && Dungeon.level.passable[i]) {
                if ( Actor.findChar(i) == null) {
                    nearby.add(i);
                }
            }
        }

        int summons = Math.min(amount, nearby.size());
        for  (int i = 0; i < summons; i++){
            Integer pos =Random.element(nearby);
            Tentacle mob =Tentacle.spawnAt(pos);
            mob.spend(TICK);

            //damaging tentacles damage yog
            SoulLink soullink = Buff.affect(mob, SoulLink.class);
            soullink.object = this.id();
            nearby.remove(pos);
        }
    }

	public void spawnFists() {
		RottingFist fist1 = new RottingFist();
		BurningFist fist2 = new BurningFist();
		
		do {
			fist1.pos = pos + Level.NEIGHBOURS8[Random.Int( 8 )];
			fist2.pos = pos + Level.NEIGHBOURS8[Random.Int( 8 )];
		} while (!Level.passable[fist1.pos] || !Level.passable[fist2.pos] || fist1.pos == fist2.pos);
		
		GameScene.add( fist1, TICK);
		GameScene.add( fist2, TICK );
        SoulLink soullink = Buff.affect(fist1, SoulLink.class);
        soullink.object = this.id();
        soullink = Buff.affect(fist2, SoulLink.class);
        soullink.object = this.id();
	}


    @Override
    public int defenseProc( Char enemy, int damage,  boolean blocked ) {

        currentAspect.onDefenseProc(damage, enemy);
        return super.defenseProc(enemy,damage,blocked);
    }

    @Override
    public boolean add( Buff buff ) {

        if (buff instanceof Corrosion || buff instanceof Burning || buff instanceof Chilled
                || buff instanceof Shocked)
            return false;

        return super.add( buff );
    }

    protected void throwItem() {
        Heap heap = Dungeon.level.heaps.get( pos );
        if (heap != null) {
            int n;
            do {
                n = pos + Level.NEIGHBOURS8[Random.Int( 8 )];
            } while (!Level.passable[n] && !Level.avoid[n]);
            Dungeon.level.drop( heap.pickUp(), n ).sprite.drop( pos );
        }
    }

    @Override
    protected boolean act() {
        throwItem();

        if (attackCD > 0)
            attackCD--;
        if (specialCD > 0)
            specialCD--;

        state = PASSIVE;
        return super.act();
    }
	
	@Override
	public void beckon( int cell ) {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void die( Object cause, Element dmg ) {

        phases--;

		for (Mob mob : (Iterable<Mob>)Dungeon.level.mobs.clone()) {
			if (mob instanceof BurningFist || mob instanceof RottingFist) {
				mob.die( cause, null );
			}
            if (mob instanceof VoidTendril || mob instanceof Tentacle) {
                mob.die( cause, null );
            }
		}

		if (phases==0) {
            GameScene.bossSlain();
            Dungeon.level.drop(new SkeletonKey(), pos).sprite.drop();
            super.die(cause, dmg);

            yell("...");
        } else {
            blink();
            initPhase();
        }
	}

	
	@Override
	public String description() {
		return TXT_DESC;
			
	}

    @Override
    public HashMap<Class<? extends Element>, Float> resistances() {

        HashMap<Class<? extends Element>, Float> resistances = super.resistances();
        if( buff( UnholyArmor.class ) != null ){
            for( Class<? extends Element> type : UnholyArmor.RESISTS ) {
                resistances.put( type, Element.Resist.IMMUNE );
            }
        }

        return resistances;
    }

	public static class RottingFist extends MobHealthy {

        public RottingFist() {

            super( 4, 20, true );
			name = "rotting fist";
			spriteClass = RottingFistSprite.class;

			EXP = 0;
			state = WANDERING;

            resistances.put( Element.Unholy.class, Element.Resist.PARTIAL );
            resistances.put( Element.Dispel.class, Element.Resist.PARTIAL );

            resistances.put( Element.Mind.class, Element.Resist.IMMUNE );
            resistances.put( Element.Body.class, Element.Resist.PARTIAL );
		}
/*
        @Override
        protected boolean getCloser( int target ) {
            //fists cannot get too far from yog
            Mob yog = getYog();
            if (yog != null && Level.distance(pos, yog.pos) > 8) {
                return super.getCloser(yog.pos);
            } else {
                return super.getCloser(target);
            }
        }
*/
        @Override
        protected float healthValueModifier() { return 0.25f; }

        @Override
        public float attackSpeed() {
            return 0.5f;
        }

		@Override
		public int attackProc( Char enemy, int damage, boolean blocked ) {

            if (!blocked)
                BuffActive.addFromDamage( enemy, Corrosion.class, damage );

			return damage;
		}

        @Override
        public void damage( int dmg, Object src, Element type ) {

            if ( type == Element.ACID ) {

                if (HP < HT) {
                    int reg = Math.min( dmg / 2, HT - HP );

                    if (reg > 0) {
                        HP += reg;
                        sprite.showStatus(CharSprite.POSITIVE, Integer.toString(reg));
                        sprite.emitter().burst(Speck.factory(Speck.HEALING), (int) Math.sqrt(reg));
                    }
                }

            } else {

                super.damage(dmg, src, type);

            }
        }

        @Override
        public boolean add( Buff buff ) {

            return !(buff instanceof Corrosion ) && super.add( buff );

        }

        @Override
        public int defenseProc( Char enemy, int damage,  boolean blocked ) {

            GameScene.add( Blob.seed( pos, 25, CorrosiveGas.class ) );

            return super.defenseProc(enemy,damage,blocked);
        }
		
		@Override
		public String description() {
			return TXT_DESC;
				
		}
	}
	
	public static class BurningFist extends MobCaster {

        //private boolean charged = false;

        public BurningFist() {

            super( 4, 20, true );
			name = "burning fist";
			spriteClass = BurningFistSprite.class;
			
			EXP = 0;
			state = WANDERING;

            resistances.put( Element.Unholy.class, Element.Resist.PARTIAL );
            resistances.put( Element.Dispel.class, Element.Resist.PARTIAL );

            resistances.put( Element.Flame.class, Element.Resist.IMMUNE );
            resistances.put( Element.Mind.class, Element.Resist.IMMUNE );
            resistances.put( Element.Body.class, Element.Resist.PARTIAL );
		}
/*
        @Override
        protected boolean getCloser( int target ) {
            //fists cannot get too far from yog
            Mob yog = getYog();
            if (yog != null && Level.distance(pos, yog.pos) > 6) {
                return super.getCloser(yog.pos);
            } else {
                return super.getCloser(target);
            }
        }
*/
        @Override
        protected float healthValueModifier() { return 0.25f; }

        @Override
        public boolean act() {

            GameScene.add( Blob.seed( pos, 2, Fire.class ) );

            //if( !enemySeen )
            //    charged = false;

            return super.act();
        }
        @Override
        public float attackSpeed() {
            return 0.5f;
        }

/*
        @Override
        protected boolean doAttack( Char enemy ) {

            if( !Level.adjacent( pos, enemy.pos ) && !charged ) {

                charged = true;

                if( Dungeon.visible[ pos ] ) {
                    sprite.centerEmitter().burst(EnergyParticle.FACTORY_WHITE, 15);
                }

                spend( attackDelay() );

                return true;

            } else {

                charged = false;

                return super.doAttack( enemy );
            }
        }
*/

        @Override
        protected void onRangedAttack( int cell ) {

            MagicMissile.fire(sprite.parent, pos, cell,
                    new Callback() {
                        @Override
                        public void call() {
                            onCastComplete();
                        }
                    });

            Sample.INSTANCE.play(Assets.SND_ZAP);

            super.onRangedAttack( cell );
        }

        @Override
        public boolean cast( Char enemy ) {
            return castBolt(enemy,damageRoll(),false,Element.FLAME);
        }
		
		@Override
		public String description() {
			return TXT_DESC;
				
		}
	}

    @Override
    public boolean cast( Char enemy ) {
        return castBolt(enemy, damageRoll(),false,Element.ENERGY);
    }

    private void blink(){
        int newPos;
        do {
            newPos = getThrones()[Random.Int(getThrones().length)];
        } while ( newPos == pos );

        Buff.detach(this, PinCushion.class);
        Debuff.removeAll( this );

        if (Dungeon.visible[pos]) {
            CellEmitter.get(pos).start( ShadowParticle.UP, 0.01f, Random.IntRange(5, 10) );
        }

        if (Dungeon.visible[newPos]) {
            CellEmitter.get(newPos).start(ShadowParticle.MISSILE, 0.01f, Random.IntRange(5, 10));
        }

        ((YogSprite)sprite).blink(pos,newPos);
        move( newPos, false );

        Sample.INSTANCE.play(Assets.SND_MELD,1,1,0.5f);
    }

    private void shadowBlink(){
        blink();

        //Move lashers
        for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
            if (mob instanceof VoidTendril) {
                VoidTendril lash = (VoidTendril) mob;
                lash.blink(pos);
            }
        }
        //spawn two more lashers
        spawnVoidTendrils(2, 4);
    }

    private void spawnDisruption(int pos, int power){
        GameScene.add(Blob.seed(pos, DisruptionField.SPREAD_STATE, DisruptionField.class));
        DisruptionField blob = (DisruptionField) Dungeon.level.blobs.get(DisruptionField.class);
        if (blob.cur[pos] > 0)
            blob.power[pos] = power;
    }

    private static final String ASPECTS = "aspects";
    private static final String CURRENT_ASPECT = "currentAspect";
    private static final String BREAKS	= "breaks";
    private static final String ATTACK_CD	= "attackCD";
    private static final String SPECIAL_CD = "specialCD";
    private static final String PHASES = "phases";


    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle(bundle);
        bundle.put( BREAKS, breaks );
        bundle.put(ATTACK_CD, attackCD);
        bundle.put(SPECIAL_CD, specialCD);
        bundle.put(PHASES, phases);
        bundle.put(CURRENT_ASPECT, currentAspect.getTag() );
//        ArrayList<String> aspectsTags =new ArrayList<>();
//        for (YogAspect a :aspects)
//            aspectsTags.add(a.getTag());
//        String[] arr = new String[aspectsTags.size()];
//        bundle.put(ASPECTS, aspectsTags.toArray(arr));


    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle(bundle);
        breaks = bundle.getInt( BREAKS );
        attackCD = bundle.getInt(ATTACK_CD);
        specialCD = bundle.getInt(SPECIAL_CD);
        phases = bundle.getInt(PHASES);
        currentAspect = getAspectFromTag(bundle.getString(CURRENT_ASPECT));
//        String[] xAspects=bundle.getStringArray(ASPECTS);
//        aspects = new ArrayList<>();
//        for (String aspectTag :xAspects)
//            aspects.add(getAspectFromTag(aspectTag));


    }

    private YogAspect getAspectFromTag(String tag){
        switch (tag){
            case YogChaosAspect.TAG:
                return new YogChaosAspect();
            case YogShadowAspect.TAG:
                return new YogShadowAspect();
            default:
                return new YogDestructionAspect();
        }
    }


    public interface YogAspect {
        boolean act(boolean enemyInFOV, boolean justAlerted);
        void init();
        String getTag();
        void onDefenseProc(int dmg, Char enemy);
    }

    private class Guarding extends Mob.Passive {

        @Override
        public boolean act(boolean enemyInFOV, boolean justAlerted) {
            spend(TICK);
            return currentAspect.act(enemyInFOV,justAlerted);
        }

    }


    private class YogChaosAspect implements YogAspect{

        public static final String TAG	= "CHAOS";

        public void init(){
            specialCD = Random.IntRange(20, 26);
            attackCD = Random.IntRange(10, 14);
            spawnTentacles(4, 4);
            yell( "I am chaos..." );
        }

        public void onDefenseProc(int dmg, Char enemy){}

        @Override
        public boolean act(boolean enemyInFOV, boolean justAlerted) {

            if ( HP < HT  ) {
                if (Dungeon.visible[pos])
                    sprite.emitter().burst(Speck.factory(Speck.HEALING), 1);
                HP++;
            }

            if(  3 - breaks > 4 * HP / HT && Dungeon.hero.isAlive()) {
                breaks++;
                int newPos;
                do {
                    newPos = Dungeon.level.randomRespawnCell();
                } while ( newPos == -1 || Level.distance(newPos, pos) < 6);
                Char ch = Dungeon.hero;
                CharmOfBlink.appear(ch, newPos);
                Dungeon.level.press(newPos, ch);
                Dungeon.observe();
                BuffActive.add(ch, Dazed.class, 6);
                spawnTentacles(2,4);
                specialCD = Random.IntRange(18, 22);
                attackCD = Random.IntRange(8, 12);
                return true;
            }

            if (enemy.isAlive() && specialCD <= 0 && Level.distance(enemy.pos, pos) > 8) {
                specialCD = Random.IntRange(20, 26);
                int tentacles=2;
                for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
                    if (mob instanceof Tentacle ) {
                        ((Tentacle) mob).blink(enemy.pos, 2);
                        tentacles--;
                        if (tentacles == 0)
                            break;
                    }
                }
                return true;
            }

            if (enemyInFOV && enemy.isAlive() && specialCD <= 0 && enemy.isAlive()) {
                specialCD = Random.IntRange(24, 30);
                GameScene.add(Blob.seed(enemy.pos, 500, ConfusionGas.class));
                return true;
            }

            if (enemy != null && enemy.isAlive() && attackCD <= 0) {
                if (Dungeon.visible[ pos ]){
                    CellEmitter.get(pos).burst(ElmoParticle.FACTORY, 6);
                }
                Dungeon.hero.interrupt();
                Sample.INSTANCE.play(Assets.SND_MELD);
                int enemyPos =enemy.pos;
                spawnDisruption(enemyPos + Level.NEIGHBOURS8[Random.Int(8)], damageRoll()*3/2);

                attackCD = Random.IntRange(10, 14);
                return true;
            }
            return true;
        }

        public String getTag(){
            return TAG;
        }

    }

    private class YogShadowAspect implements YogAspect{

        public static final String TAG	= "SHADOWS";

        public void init(){
            specialCD = Random.IntRange(20, 26);
            attackCD = Random.IntRange(8, 12);
            spawnVoidTendrils(4, 4);
            yell( "I am darkness..." );
        }

        public void onDefenseProc(int dmg, Char enemy) {
            VoidTendril furtherLash = null;
            int dist = -1;
            for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
                if (mob instanceof VoidTendril && Level.distance(pos, mob.pos) > dist) {
                    furtherLash = (VoidTendril) mob;
                    dist = Level.distance(pos, mob.pos);
                }
            }
            if (furtherLash != null) {
                furtherLash.blink(enemy.pos, 1);
                furtherLash.spend(-TICK);
            }
        }


        @Override
        public boolean act(boolean enemyInFOV, boolean justAlerted) {
            if(  3 - breaks > 4 * HP / HT ) {
                breaks++;
                shadowBlink();
                specialCD = Random.IntRange(20, 26);
                attackCD = Random.IntRange(8, 12);
                for (int n : Level.NEIGHBOURS9) {
                    int cell = pos + n;
                    if (!Dungeon.level.solid[cell])
                        GameScene.add(Blob.seed(cell, 10, Darkness.class));
                }
                return true;
            }

            for (int n : Level.NEIGHBOURS9) {
                int cell = pos + n;
                if (!Dungeon.level.solid[cell])
                    GameScene.add(Blob.seed(cell, 10, Darkness.class));
            }

            if (enemyInFOV && enemy != null) {
                if (attackCD <= 0) {
                    sprite.cast(enemy.pos, new Callback() {
                        @Override
                        public void call() {
                            MagicMissile.shadow(sprite.parent, pos, enemy.pos, new Callback() {
                                @Override
                                public void call() {
                                    onCastComplete();
                                }
                            });
                            Sample.INSTANCE.play(Assets.SND_ZAP);
                            sprite.idle();
                        }
                    });
                    spend(attackSpeed());
                    attackCD = Random.IntRange(12, 16);
                    return false;
                }
            }

            if (enemy.isAlive() && specialCD <= 0 && Level.distance(enemy.pos, pos) > 8) {
                specialCD = Random.IntRange(20, 26);
                for (int n : Level.NEIGHBOURS9) {
                    int cell = enemy.pos + n;
                    if (!Dungeon.level.solid[cell])
                        GameScene.add(Blob.seed(cell, 10, Darkness.class));
                }
                int lashers=2;
                for (Mob mob : (Iterable<Mob>) Dungeon.level.mobs.clone()) {
                    if (mob instanceof VoidTendril) {
                        ((VoidTendril) mob).blink(enemy.pos, 1);
                        lashers--;
                        if (lashers == 0)
                            break;
                    }
                }
                return true;
            }
            return true;
        }

        public String getTag(){
            return TAG;
        }
    }

    private class YogDestructionAspect implements YogAspect{

        public static final String TAG	= "DESTRUCTION";

        public void init(){
            yell( "I am destruction..." );
            specialCD = Random.IntRange(20, 26);
            spawnFists();
        }

        public void onDefenseProc(int dmg, Char enemy){
            for (Mob mob : Dungeon.level.mobs) {
                if (mob instanceof BurningFist || mob instanceof RottingFist) {
                    mob.beckon( pos );
                }
            }

            if (Level.adjacent(enemy.pos, pos) && Random.Int(6) == 0) {

                enemy.knockBack(pos, 40, 2);
                Camera.main.shake(2, 0.5f);
                Sample.INSTANCE.play(Assets.SND_BLAST, 1.0f, 1.0f, 0.8f);
                BlastWave.createAtPos(pos);
            }
        }

        public boolean act(boolean enemyInFOV, boolean justAlerted) {
            if(  3 - breaks > 4 * HP / HT && Dungeon.hero.isAlive()) {
                breaks++;

                //Buff.affect(YogNew.this, UnholyArmor.class, 8 + breaks*2 );
                BuffActive.add(YogDzewa.this, Shielding.class, 8 + breaks * 2 );
                for (Mob mob : (Iterable<Mob>)Dungeon.level.mobs.clone()) {
                    if (mob instanceof BurningFist || mob instanceof RottingFist) {
                        BuffActive.add(mob, Shielding.class, 8 + breaks * 2);
                    }
                }
                return true;
            }
/*
            if (enemy.isAlive() && specialCD <= 0 && Level.distance(enemy.pos, pos) > 8) {
                specialCD = Random.IntRange(20, 26);
                int newPos;
                do {
                    newPos = Dungeon.level.randomRespawnCell();
                } while ( newPos == -1 || Level.distance(newPos, pos) > 6);
                Char ch = Dungeon.hero;
                CharmOfBlink.appear(ch, newPos);
                Dungeon.level.press(newPos, ch);
                Dungeon.observe();
                return true;
            }
*/
            return true;
        }

        public String getTag(){
            return TAG;
        }

    }

    private class YogPassiveAspect implements YogAspect{

        public static final String TAG	= "PASSIVE";

        public void onDefenseProc(int dmg, Char enemy){}

        public boolean act(boolean enemyInFOV, boolean justAlerted) {
            UnholyArmor armor = buff(UnholyArmor.class);
            if (armor!= null)
                armor.delay(TICK);
            else{
                Buff.affect(YogDzewa.this, UnholyArmor.class, 8 );
            }

            if (specialCD == -10) {
                initPhase();
                return true;
            }

            if (specialCD != -10 && enemyInFOV && Dungeon.visible[pos] && Level.distance(pos, enemy.pos) < 5) {
                yell( "Hope is an illusion..." );
                specialCD = -10;
            }
            spend(TICK);
            return true;
        }

        public String getTag(){
            return TAG;
        }

        public void init(){
        }
    }

}