/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Darkness;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Ensnared;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Withered;
import com.ravenwolf.nonameyetpixeldungeon.items.quest.VoidStone;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.BArray;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ShadowParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.VoidTendrilSprite;
import com.watabou.utils.Callback;
import com.watabou.utils.PathFinder;
import com.watabou.utils.Random;

import java.util.ArrayList;

public class VoidTendril extends MobEvasive {

	{
		name = "void tendril";
		spriteClass = VoidTendrilSprite.class;
	}

	protected boolean damagedThisTurn=false;

	public VoidTendril() {

		super( Dungeon.chapter(), Dungeon.depth/3 + Dungeon.chapter() * 2, false );

		resistances.put(Element.Flame.class, Element.Resist.VULNERABLE);

		resistances.put(Element.Unholy.class, Element.Resist.PARTIAL);
		resistances.put(Element.Body.class, Element.Resist.PARTIAL);
		resistances.put(Element.Mind.class, Element.Resist.IMMUNE);

		PASSIVE = new Guarding();
		state = PASSIVE;
	}


	@Override
	public boolean ignoreDarkness() {
		return true;
	}

	@Override
	public boolean immovable() {
		return true;
	}

	@Override
	public boolean isMagical() {
		return true;
	}

	@Override
	protected boolean getCloser(int target) {
		return true;
	}

	@Override
	protected boolean getFurther( int target ) {
		if( enemySeen ) {
			blink(pos);
		}
		return true;
	}

	@Override
	public int viewDistance() {
		return 1;
	}

	@Override
	public Element damageType() {
		return Element.UNHOLY_PERIODIC;
	}

	@Override
	public boolean add(Buff buff) {
		if (buff instanceof Ensnared)
			return false;
		else
			return super.add(buff);
	}

	@Override
	public boolean act() {
		for (int n : Level.NEIGHBOURS9) {
			int cell = pos + n;
			if (!Dungeon.level.solid[cell])
				GameScene.add(Blob.seed(cell, 10, Darkness.class));
		}
		state = PASSIVE;
		if (!stunned && damagedThisTurn && Random.Int(2)==0){
			damagedThisTurn=false;
			return blink(pos);
		}
		damagedThisTurn=false;

		//if have void stone blink towards hero and attack it
		if (Dungeon.hero != null && !Level.adjacent(pos, Dungeon.hero.pos) && Random.Int(5)==0 && (Dungeon.hero.belongings.getItem( VoidStone.class ) != null)){
			spend(-moveSpeed());
			return blink(Dungeon.hero.pos, 1);
		}

		return super.act();
	}

	@Override
	public void damage( int dmg, Object src, Element type ) {
		super.damage( dmg, src, type );
		damagedThisTurn = true;
	}

	@Override
	public int attackProc( Char enemy, int damage, boolean blocked ) {
		if( !blocked && Random.Int( 9 ) < tier ) {
			BuffActive.addFromDamage( enemy, Withered.class, damage * 2 );
		}
		return damage;
	}

	//Blinks nearby target position
	public boolean blink(int target) {
		return blink(target,3);
	}

	//Blinks nearby target position
	public boolean blink(int target, int dist) {

		ArrayList<Integer> nearby = new ArrayList<>();
		PathFinder.buildDistanceMap( target, BArray.not( Dungeon.level.solid, null ), dist );
		for (int i = 0; i < PathFinder.distance.length; i++) {
			if (PathFinder.distance[i] < Integer.MAX_VALUE && Dungeon.level.passable[i]) {
				if ( Actor.findChar(i) == null) {
					nearby.add(i);
				}
			}
		}

		//if no position available, wait one turn
		if (nearby.isEmpty()) {
			spend( TICK );
			return true;
		}

		Integer newPos =Random.element(nearby);
		if (Dungeon.visible[pos]) {
			CellEmitter.get(pos).start( ShadowParticle.UP, 0.01f, Random.IntRange(5, 10) );
		}
		((VoidTendrilSprite)sprite).blink(pos, newPos);
		//move( newPos, false );
		spend( 1 / moveSpeed() );
		return false;
	}


	@SuppressWarnings("unchecked")
	public static VoidTendril spawnAt(int pos) {

		// cannot spawn on walls, chasms or already occupied tiles
		if (!Level.solid[pos] && !Level.chasm[pos] && Actor.findChar(pos) == null) {

			VoidTendril lash = new VoidTendril();

			lash.pos = pos;
			lash.enemySeen = true;
			lash.state = lash.PASSIVE;

			for (int n : Level.NEIGHBOURS9) {
				int cell = pos + n;
				if (!Dungeon.level.solid[cell])
					GameScene.add(Blob.seed(cell, 10, Darkness.class));
			}

			GameScene.add(lash, 0f);
			Dungeon.level.press(lash.pos, lash);

			lash.sprite.spawn();

			return lash;

		} else {

			return null;

		}
	}

	public void knockBack(int sourcePos, int damage, int amount, final Callback callback) {
		if (callback != null)
			callback.call();
		return;
	}


	private class Guarding extends Passive {

		@Override
		public boolean act( boolean enemyInFOV, boolean justAlerted ){

			if (enemyInFOV && canAttack( enemy ) ) {
				return doAttack( enemy );
			} else {
				//reset enemy if cannot attack
				resetEnemy();
				spend( TICK );
				return true;
			}
		}

		@Override
		public String status(){
			return "guarding";
		}
	}

	@Override
	public String description() {
		return
				"Formed out of pure dark energy from the void, this tendril is surrounded " +
				"by an aura of thick darkness that blocks vision. It can also phase in and out of the void to change its position.";
	}


}
