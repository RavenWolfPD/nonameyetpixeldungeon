/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.blobs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Chilled;
import com.ravenwolf.nonameyetpixeldungeon.actors.hazards.Hazard;
import com.ravenwolf.nonameyetpixeldungeon.actors.hazards.SubmergedPiranha;
import com.ravenwolf.nonameyetpixeldungeon.items.Heap;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.BlobEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.SnowParticle;
import com.watabou.utils.Random;

public class FrigidVapours extends Blob {


    int startCell;
    public FrigidVapours() {
        super();

        name = "frigid vapours";
    }

    @Override
    protected void evolve() {
        super.evolve();

        for (int i=0; i < LENGTH; i++) {
            if (cur[i] > 0) {

                Char ch = Actor.findChar( i );

                if( ch != null ){
                    BuffActive.addFromDamage( ch, Chilled.class, 2+Dungeon.depth/2 );
                }

                Heap heap = Dungeon.level.heaps.get( i );
                if (heap != null) {
                    heap.freeze( TICK );
                }

                off[i] = cur[i]/2;
                volume += off[i];
            }  else if (Level.distance(i,startCell)>1){//only spread near the source point
                //FIXME
                volume-=off[i];
                off[i] = 0;
            }

        }

        Blob blob = Dungeon.level.blobs.get( Fire.class );

        if (blob != null) {

            for (int pos=0; pos < LENGTH; pos++) {

                if ( cur[pos] > 0 && blob.cur[ pos ] < 2 ) {

                    blob.clear( pos );

                }
            }
        }
    }

    public void seed( int cell, int amount ) {
        super.seed(cell,amount);
        startCell=cell;
    }

    @Override
    public void use( BlobEmitter emitter ) {
        super.use( emitter );

        emitter.pour( SnowParticle.FACTORY, 0.3f );
    }

    @Override
    public String tileDesc() {
        return "A cloud of freezing vapours is swirling here.";
    }

//
//
//	// Returns true, if this cell is visible
//	public static boolean affect( int cell, int duration, Fire fire ) {
//
//		Char ch = Actor.findChar( cell );
//		if (ch != null) {
//
//            BuffActive.add( ch, Chilled.class, (float)duration );
//
////			Buff.prolong( ch, Chilled.class, duration * ( Level.water[cell] && !ch.flying ? 2 : 1 ) );
//		}
//
//		if (Dungeon.visible[cell]) {
//			CellEmitter.get( cell ).start( SnowParticle.FACTORY, 0.2f, 6 + duration );
//			return true;
//		} else {
//			return false;
//		}
//	}
}
