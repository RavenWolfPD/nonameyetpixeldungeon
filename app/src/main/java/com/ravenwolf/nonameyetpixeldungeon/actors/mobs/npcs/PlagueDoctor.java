/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Journal;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Miasma;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ShadowParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ShopkeeperPlagueDocSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.windows.WndPlagueDoctor;
import com.watabou.utils.Random;


public class PlagueDoctor extends NPCSupplier {

    private static final String TXT_GREETINGS = "Finally! an appropriate test subject";

    private static final String TXT_OUT_OF_STOCK = "Sorry, I must save my supplies. For later...";


    private static String[][] LINES = {

            {
                    "Dammit! My vials are broken! That will cost extra!",
                    "Cross me and you'll wake up sick.",
            },
            {
                    "You want my secrets! You would take them off my corpse!",
                    "You fear my knowledge. You cannot understand it!",
            },
            {
                    "All flesh fails, in the fullness of time.",
                    "I have spilled my latest batch of plague. We're doomed.",
                    "Look how they wither and die!",
            },
    };

	{
		name = "plague doctor";
		spriteClass = ShopkeeperPlagueDocSprite.class;
	}

    protected Journal.Feature feature(){
	    return Journal.Feature.PLAGUE_DOCTOR;
    }

    protected String greetingsText(){
	    return TXT_GREETINGS;
    }

    protected String[][] lines(){
	    return LINES;
    }

    public static void spawn(Level level ){
        PlagueDoctor npc = new PlagueDoctor();
        do {
            npc.pos = level.randomRespawnCell();
        } while (npc.pos == -1 || level.heaps.get( npc.pos ) != null || !level.NPCSafePos(npc.pos));

        npc.stock = 2 + Random.Int(2);
        level.mobs.add(npc);
        Actor.occupyCell( npc );
    }

    public void runAway() {
	    super.runAway();
        GameScene.add( Blob.seed( pos, 600, Miasma.class ) );
        CellEmitter.get(pos).start( ShadowParticle.CURSE, 0.02f, 40 );
    }

    @Override
    public String description() {
        return
            "Where there's plague, there's the beak doctors and also death, as the saying goes. " +
            "Though, you have no idea what this plague doctor is doing here. " +
            "It does not looks totally sane... but at least he seems to be uncorrupted by the plague.";
    }

	@Override
	public void interact() {
        sprite.turnTo( pos, Dungeon.hero.pos );
        if (outOfStock() )
            tell( TXT_OUT_OF_STOCK );
        else
            GameScene.show( new WndPlagueDoctor( this ));
	}

}
