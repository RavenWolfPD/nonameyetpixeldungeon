/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.actors.mobs;

import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Miasma;
import com.ravenwolf.nonameyetpixeldungeon.items.Generator;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.MissileSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.PlagueDoctorSprite;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.Random;


public class PlagueDoctor extends MobCaster {

    private int plagueCD = 0;

    public PlagueDoctor() {

        super( 7);

		name = "plague doctor";
		spriteClass = PlagueDoctorSprite.class;

        loot = Generator.random(Generator.Category.POTION);
        lootChance =0.25f;
	}

    @Override
    public String getTribe() {
        return TRIBE_UNDEAD;
    }

    protected boolean canCastBolt( Char enemy ) {
        return enemy!=null && plagueCD <=0 && !Level.adjacent( pos, enemy.pos )
                && Ballistica.cast( pos, enemy.pos, false, false ) == enemy.pos ;
    }

    @Override
    public boolean act() {

        if (plagueCD >0)
            plagueCD--;
        if (state==FLEEING && canCastBolt( enemy ))
            state = HUNTING;
        else if (state==HUNTING && (Random.Int(5)==0 && plagueCD <=0 || plagueCD >8))
            state = FLEEING;

        return super.act();
    }


    @Override
    protected void onRangedAttack( int cell ) {

        plagueCD =Random.IntRange(12, 17);
        final int targetCell = Ballistica.trace[Ballistica.distance-2];
        ((MissileSprite) sprite.parent.recycle(MissileSprite.class)).
                reset(pos, targetCell, ItemSpriteSheet.POTION_CHARCOAL, new Callback() {
                    @Override
                    public void call() {
                        GameScene.add( Blob.seed( targetCell, damageRoll()*12, Miasma.class ) );
                        Sample.INSTANCE.play( Assets.SND_SHATTER );
                        next();
                    }
                });

        state = FLEEING;
        super.onRangedAttack( targetCell );
    }


    @Override
    public String description() {
        return
            "When the dark miasma reached the prison, plague doctors where send to assist the sick and contain the disease. " +
            "But not even their beak masks could protected them from such plague... " +
            "Twisted by the dark magic, they are now utterly dedicated to the spreading of corruption and decay.";
    }

    private static final String PLAGUE_CD = "plagueCD";

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle(bundle);
        bundle.put(PLAGUE_CD, plagueCD);
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle(bundle);
        plagueCD = bundle.getInt(PLAGUE_CD);
    }
}
