/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.visuals.sprites;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ShadowParticle;
import com.watabou.noosa.TextureFilm;
import com.watabou.utils.Callback;

public class VoidTendrilSprite extends MobSprite {

	private Animation blink;
	private Animation blinkStart;

	public VoidTendrilSprite() {
		super();
		
		texture( Assets.SHADOWLASH );
		
		TextureFilm frames = new TextureFilm( texture, 16, 16 );

		idle = new Animation( 5, true );
		idle.frames( frames, 4, 5, 6, 7 );
		
		run = new Animation( 5, true );
		run.frames( frames, 4, 5, 6, 7 );
		
		attack = new Animation( 15, false );
		attack.frames( frames, 8, 9, 10, 11, 12, 13 );

        spawn = new Animation( 10, false );
        spawn.frames( frames, 0, 1, 2, 3, 4 );
		
		die = new Animation( 10, false );
		die.frames( frames, 3, 2, 1, 0 );

		blink = spawn.clone();
		blinkStart = die.clone();

		play( idle );
	}

	@Override
	public void onComplete( Animation anim ) {
		if (anim == blink) {
			isMoving = false;
			idle();
		}
		super.onComplete(anim);
	}

	@Override
	public void die() {
		super.die();
		if (Dungeon.visible[ch.pos]) {
			emitter().burst( ShadowParticle.CURSE, 10 );
		}
	}

	public void blink( final int from, final int to ) {
		if (visible) {
			play(blinkStart);
			animCallback = new Callback() {
				@Override
				public void call() {
					blinkEnd(from, to);
				}
			};
		}else {
			blinkEnd(from, to);
		}
	}

	private void blinkEnd( int from, int to ) {
		place( to );
		play( blink );
		isMoving = true;
		turnTo( from , to );
		ch.onMotionComplete();
		ch.move( to, false );
	}


	@Override
	public int blood() {
		return 0x88000000;
	}
}
