/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.visuals.windows;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs.Ghost;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs.MysteriousGuy;
import com.ravenwolf.nonameyetpixeldungeon.items.EquipableItem;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.Utils;
import com.ravenwolf.nonameyetpixeldungeon.scenes.PixelScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.RedButton;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.Window;
import com.watabou.noosa.BitmapTextMultiline;

public class WndMysteriousGuy extends Window {


	private static final String TXT_INTRO	=
			"I have a very special item for you, " +
			"pay me first and it will be yours. " +
			"What do you say?";
	private static final String TXT_BUY		= "Buy for %dg";
	private static final String TXT_CANCEL		= "Never mind";

	private static final int WIDTH		= 120;
	private static final int BTN_HEIGHT	= 20;
	private static final float GAP		= 2;

	public WndMysteriousGuy(final MysteriousGuy guy, final EquipableItem item) {
		
		super();
		IconTitle titlebar = new IconTitle();
		titlebar.icon(guy.sprite());
		titlebar.label(Utils.capitalize(guy.name));
		titlebar.setRect( 0, 0, WIDTH, 0 );
		add( titlebar );
		
		BitmapTextMultiline message = PixelScene.createMultiline(TXT_INTRO, 6 );
		message.maxWidth = WIDTH;
		message.measure();
		message.y = titlebar.bottom() + GAP;
		add( message );
		final int price =item.price() * Dungeon.chapter() * 2;
		RedButton btnBuy = new RedButton(  Utils.format( TXT_BUY, price ) ) {
			@Override
			protected void onClick() {
				buyItem( guy, item, price );
			}
		};
		btnBuy.enable( price <= Dungeon.gold );
		btnBuy.setRect( 0, message.y + message.height() + GAP, WIDTH, BTN_HEIGHT );
		add( btnBuy );
		
		RedButton btnNo = new RedButton( TXT_CANCEL ) {
			@Override
			protected void onClick() {
				hide();
			}
		};
		btnNo.setRect( 0, btnBuy.bottom() + GAP, WIDTH, BTN_HEIGHT );
		add( btnNo );
		
		resize( WIDTH, (int)btnNo.bottom() );
	}
	
	private void buyItem(MysteriousGuy guy, EquipableItem reward, int price ) {
		hide();
		Dungeon.gold-=price;
		guy.yell( "See ya" );
		guy.runAway();
		Dungeon.level.drop( reward, guy.pos ).sprite.drop();

	}
}
