/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.visuals.windows;

import com.ravenwolf.nonameyetpixeldungeon.DungeonTerrainFeaturesTilemap;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.Trap;


public class WndInfoTrap extends WndTitledMessage {

	public WndInfoTrap(Trap trap) {

		super(DungeonTerrainFeaturesTilemap.tile( trap.pos),
				trap.name(),
				(!trap.active ? "The trap has been triggered before and it's not dangerous anymore.\n\n" : trap.description()));

	}
}
