/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.visuals.windows;

import com.ravenwolf.nonameyetpixeldungeon.NoNameYetPixelDungeon;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.scenes.PixelScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.ScrollPane;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.Window;
import com.watabou.gltextures.SmartTexture;
import com.watabou.gltextures.TextureCache;
import com.watabou.noosa.BitmapText;
import com.watabou.noosa.BitmapTextMultiline;
import com.watabou.noosa.Image;
import com.watabou.noosa.TextureFilm;
import com.watabou.noosa.ui.Component;

public class WndTutorial extends WndTabbed {

	private static final int WIDTH_P	= 112;
	private static final int HEIGHT_P	= 160;

	private static final int WIDTH_L	= 128;
	private static final int HEIGHT_L	= 128;

    private static SmartTexture icons;
    private static TextureFilm film;

	private static final String TXT_TITLE	= "Tutorial";

	private static final String[] TXT_LABELS = {
        "I", "II", "III", "IV", "V",
    };

    private static final String[] TXT_TITLES = {
        "Tutorial - Interface",
		"Tutorial - Mechanics",
		"Tutorial - Equipment",
		"Tutorial - Misc & Consumables",
		"Tutorial - Denizens",
    };

    private static final String[][] TXT_POINTS = {
            {
                    "Almost all the actions in the game are performed via tapping a desired tile. You tap to move, tap to attack, tap to pick up items and tap to interact with NPCs and dungeon features.",
                    "If you tap the character avatar on the top left of the screen, you'll be able to see the stats and buffs your character currently has. If one of the stats is affected by an unidentified item, its power will be displayed as \"??\".",
                    "On the bottom right of the screen you can see this button. Tap it to open your inventory (you have probably guessed it by now). Long press it to show your keys available at the moment.",
                    "You can skip a turn by tapping this button on the bottom left of the screen. Press and hold this button to rest - this allows to skip time much faster and significantly increases your health regeneration.",
                    "Depending on your settings, this button can be tapped or pressed to make your character search nearby tiles, revealing traps and secret doors. Alternatively, it allows you to examine anything you meet in the dungeon to read its description.",
                    "To the left of the inventory button you can see three quickslots. You can use some items from there without need to open your inventory. You can add items for the quickslots by pressing and holding them.",
                    "There are some action button on the right of the screen. These buttons are block (only available with shields and melee weapons equipped, long tap to use your shield to slam enemies), ranged attack (only available with throwing weapons or missile weapon with their corresponding ammo equipped), and wand attack (only available if you have a wand equipped).",
            /*"There is an offhand quickslot right above the inventory button. Its effect depends on the combination of weapons you have equipped at the moment. For example, it will shoot if wands or ranged weapons are equipped.",*/
                    "There are also several buttons which are probably hidden at the moment. These are danger indicator, attack button, pickup button and resume button. Danger indicator shows the number of currently visible enemies.",
                    "Selecting the enemy by clicking the danger indicator and tapping the attack button allows you to attack an enemy without tapping the tile. Also, pressing and holding the attack button will show target's description.",
                    "Pickup button is shown only when there are items on the tile on which your character is standing right now. Tapping the pickup button allows you to pick up these items without the need to click on the character.",
                    "There is also a button which allows you to drink from one of your waterskins without having to open your inventory. Or you can long press this button to pour water from your waterskin instead.",
                    "Also there are another button which allows you to interact with your lantern. It is kinda smart one - tap it to light, snuff or refill your lantern. Long pressing it allows you to use your spare oil flasks to ignite nearby tiles.",
            },
            {
                    "Most actions in the game spend one turn when performed, which means that one attack spend just as much time as one tile travelled, and that almost everyone moves with the same speed. Keep in mind that actions take their effect the moment they are performed and make the character wait after that.",
                    "Enemies have a certain chance to notice you. It depends on their Perception power and your Stealth power. It also depends on the distance between you and the mob, but the mobs who are already on the hunt will have 100% chance to notice you if they can see you at all.",
                    "However, as enemy loses sight of you, they can lose track of you and become open to a sneak attack. The chance of this to happen mostly depends on your Stealth. You can (and should) utilize corners, doors or high grass for this tactic. Don't forget that flying creatures can see over high grass.",
                    "When you are attacking or being attacked, the first thing to be determined is whether this attack was a hit or miss. In general, the chance to hit depends on the attacker's Accuracy power and the defender's Dexterity power. Different characters and even mobs have different growth rates for these values.",
                    "Dexterity power of the defender is decreased by 1/16 of its power for every occupied or impassable tile near it. This means that you should lure your enemies to narrow corridors if you want to miss less often, and you'll want to stick to the open areas if you want to dodge more reliably.",
                    "All ranged attacks consider attacker's Accuracy to be decreased for every tile of distance. Bows and crossbows distance penalty is lower though. The only exception for this rule are wands and charms, that never miss.",
                    "For the player character (and some late-game mobs), every melee consecutive hit after the second one slightly increases damage done by the attack. This is shown by the \"combo\" message over your character and can significantly improve your effectiveness against packs of mobs.",
                    "Your Perception attribute affects your chances to score critical hits, very useful to deal additional damage and effects to most enemies. It also affects your chances to find a trap or a secret door by walking near it, as well as your chance to hear an enemy through the wall. Mind that secrets become more difficult to find as you descend further into the dungeon.",
                    "High grass is extremely useful for setting up ambushes, as it both blocks field of view and makes you stealthier. However, water is noisy and will decrease your stealth when you are standing in it.",
                    "Sleeping is the most readily available source of recovery. While you are sleeping, your regeneration rate is tripled.",
                    "Searching guarantees that all secrets on nearby tiles (such as traps or hidden doors) will be revealed to you. Lighting your lantern will reveal any hidden door and increases your chances to detect traps. Keep in mind that lit lantern also increases your chances to be detected by enemies, however.",
                    "Your Willpower very significantly affects wands recharge rate, and also determines chance to prevent triggering cursed effects of equipped gear. Magic skill determines your power with wands and charms and also affects effectiveness of certain offensive scrolls.",
            },
            /*{
                    "Melee weapons are separated into different categories. The most basic of them are Light one-handed weapons which do not have any penalties and can be used as off-hand weapons without strength penalties. Offhand weapons can be used in combination with a ranged weapon on your main hand, allowing you to perform melee attacks with them instead of using a ranged weapon which have several penalties at melee range  ",
                    "Heavy one-handed weapons are very similar to light one-handed weapons, but they have a Strength penalty if used as offhand weapons.",
                    "Light two-handed weapons are basically different kinds of polearms, most of them have extra reach.  They cannot be used as offhand weapons. However, they can be used with shields just fine.",
                    "Heavy two-handed weapons are not intended to be used with shields and will require additional strength if you want to use them in this way. But those are the most powerful weapons.",
                    "Dual weapons are intended to be used with both hands, and attack faster than any other weapon, they can still be used with shields, but only one of them, so the attack speed bonus is negated if used this way.",
                    "Throwing weapons can be equipped in your ammo slot. They can't be upgraded, but they allow you to attack from distance while keeping your melee weapon equipped. They have a chance to break on use (same applies for ammunition as well).",
                    "Ranged weapons require both hands to use - meaning they cannot be equipped with shields, but you still can use your offhand slot to equip a wand or a melee weapon to fight in melee range. Without ammunition, you will attack as if you had no weapon at all. Every kind of ranged weapon requires specific kind of ammunition in you ammo slot.",
                    "Crossbows are the strongest ranged weapons, but require to reload after each attack, consuming half of a turn. But you can reload while moving for free.",
                    //"Flintlock weapons require bullets to shoot and gunpowder in your inventory to reload. Also, loud noises tend to draw unnecessary attention. However, firearms are equally accurate on any distance and they penetrate target's armor.",
                    "Always have at least some kind of armor equipped. Proper armor will greatly increase your chances of survival, decreasing damage from most sources. It will not protect from non-physical damage though, such as fire, lightning or disintegration.",
                    "Cloth armors offer very little protection but they can enhance one of your secondary attributes - Stealth, Detection or Willpower. This bonus can be increased by upgrading the armor, and can lead to some powerful (but risky) character builds.",
                    "Shields occupy your offhand slot, but they allow you to enter a defencive stance increasing your armor class for some turns and providing a chance to complete negate enemy blows. When successfully blocking or parrying, you have a chance to leave your enemy open to a counter attack, which will be a guaranteed hit. Shields can also be used to slam your target, dealing some damage with chances to daze and knockback your target (granted on exposed enemies)",
                    //"Wands can be very powerful, but you need to equip them and they have a limited number of charges. Utility wands spend all of their charges on use, and their effect depends on amount of charges used.",
                    "Magical trinkets such wands and charms can be very powerful, and their power is tied to the user magical aptitude. But you need to equip them and they have a limited number of charges. Wands are very reliable source of ranged damage. Charms spend all of their charges on use, and their effect mainly depends on amount of charges used.",
            },*/
            {
                    "There are several types of equipments that you can wear. Melee Weapons are the most basic of them and require to be equipped in your main weapon slot",
                    "Light weapons deals the lower amount of damage, but do not have any penalties. And are generally good for backstab your foes ",
                    "Heavy weapons have grater penalties to accuracy and stealth. But those are the most powerful weapons.",
                    "Every weapon have some additional bonus, some of them have extra reach, some are good scoring critical hits, and others are fast. Check weapon descriptions to know its properties and try to pick weapons that adapt to your strategy and synergy with other gear",
                    "Shields occupy your offhand slot, but they allow you to enter a defencive stance increasing your armor class for some turns and providing a chance to complete negate enemy blows, specially ranged ones. When successfully blocking or parrying a melee attack, you have a chance to leave your enemy open to a counter attack, which will be a guaranteed hit. Shields can also be used to slam your target, dealing some damage with chances to daze your target",
                    //"Wands can be very powerful, but you need to equip them and they have a limited number of charges. Utility wands spend all of their charges on use, and their effect depends on amount of charges used.",
                    "Ranged weapons require to be equipped in your offhand slot. Every kind of ranged weapon requires specific kind of ammunition in you ammo slot.",
                    "Crossbows are the strongest ranged weapons, but require to reload after each attack, consuming half of a turn. But you can reload while moving for free.",
                    "Throwing weapons can be equipped in your ammo slot. They can't be upgraded, but they allow you to attack from distance without needing a ranged weapon in your offhand. They have a chance to break on use (same applies for ammunition as well).",
                    "Magical trinkets such wands and charms can be very powerful, and their power is tied to the user magical aptitude. But you need to equip them in your offhand slot and they have a limited number of charges. Wands are very reliable source of ranged damage. Charms spend all of their charges on use, and their effect mainly depends on amount of charges used.",
                    //"Flintlock weapons require bullets to shoot and gunpowder in your inventory to reload. Also, loud noises tend to draw unnecessary attention. However, firearms are equally accurate on any distance and they penetrate target's armor.",
                    "Always have at least some kind of armor equipped. Proper armor will greatly increase your chances of survival, decreasing damage from most sources. It will not protect from non-physical damage though, such as fire, lightning or disintegration.",
                    "Cloth armors offer very little protection but they can enhance one of your secondary attributes - Stealth, Perception or Willpower. This bonus can be increased by upgrading the armor, and can lead to some powerful (but risky) character builds.",

            },
            {
                    "Most equipment can be upgraded. Upgraded items are much more powerful than common ones, they deal more damage, offer better protection, require less strength, have higher amount of charges and recharge faster. However, items cannot be upgraded more than five times. Keep that in mind.",
                    "Weapons and armors can be enchanted. Enchantments provide some unique effects such as bonus fire damage or resistance to acid, but chance of them working significantly depends on the upgrade level of your weapon. Also, cursed items may reverse the effects of their enchantments, turning them against you.",
                    "Some items may happen to be cursed, which means that you will be unable to unequip them until curse is removed (with help of certain scrolls). Cursed items offer the same damage/protection as non-cursed ones. Items which are too good for the current chapter have a much higher chance of being cursed.",
                    "Rings are rare trinkets which can greatly help you when equipped. They are not really powerful by themselves, but effects of similar rings stack with each other. Cursed rings will hinder your abilities instead.",
        /*    "Most equipment has condition level. It slowly decreases as the item is used, but can be restored with the corresponding repair tools or scrolls of Upgrade. Every condition level affects item performance just as much as upgrade level does, but it doesn't affects the strength requirement of the weapon or the number of charges of the wand.",*/
                    "Your character needs food to survive. There is always at least one ration at every floor; you can also find some food in shops or dropped from certain mobs. Your regeneration rate is increased when your satiety is over 75% and halved when it is 25% or lower. When your satiety hits 0%, it will cause periodical damage from starvation.",
                    "Your most readily available source of healing are your waterskins. When used, waterskin recovers part of your missing health, and can be refilled in occasional water fountains. There is always two fountain per chapter.",
                    "In the darkness of the dungeon, ambient lighting is barely noticeable, limiting your field of vision. To counter this, you can use lantern you start with. It will reduce your Stealth though, so use it wisely.",
                    "Bombs are powerful items that can be used to several damage mobs and boss, but also to destroy some scenery such barricades and libraries. These bombs can be combined into even more powerful bomb bundles.",
                    "Scrolls can be very powerful when used correctly. Some of them can lead to your quick demise if used incorrectly. There is no way to know which scroll is which, unless you try to read it or find one in the shop.",
                    "On your quest, you can find randomly colored potions. Some of them are beneficial and some of them are harmful. Beneficial potions buff you when used, and harmful potions are better to be thrown into your enemies.",
                    "Sometimes in the high grass you can find alchemical herbs. This herbs can be consumed to absorb some nutrients and part of their magical powers. Mix them into an alchemy pot to brew a potion. Type of the potion will depend on the herbs you used (order in which herbs are thrown into the pot is not important).",
                    "If you find inventory to be too limiting, consider buying bags in the shops. A bag unlocks separate inventories for herbs, potions, scrolls or wands. Additionally, it will protect these items from harmful effects (like fire).",
            },
            {
                    "As you explore this dungeon, you'll meet many adversaries on your path. Defeating your enemies is the main source of experience to level up your character, but you'll need the level of threat of your opponents to be appropriate to see any improvements.",
                    "Dungeon is filled with monsters to the brim, and even as you kill them, it will always spawn more to get you. Some mobs can even drop something useful on death. Be careful not to rely on that too much though. Each time the dungeon spawns another creature, respawn time on the current floor is increased a bit.",
                    "Every denizen of this dungeon possesses some special abilities, but in general all of them can be separated into several categories. Most common enemies like rats and flies usually have higher dexterity and stealth, but their attacks are weaker and they can't take a good hit without, you know, dying horribly.",
                    "A bit less common enemies like muggers, skeletons or brutes usually do not have any significant drawbacks or advantages. Some of them can even try to attack you from short distance, but these attacks are usually weaker and limited.",
                    "Some enemies possess proper ranged attacks though, and they will use them whenever possible. Even worse is that most of these enemies also deal non-physical damage which ignores your armor class. On the other hand, some of them also need to spend a turn to charge their attack before that.",
                    "There are also some mobs which are a much greater threat than others, being strong, durable, and pretty accurate as well. Their only weakness is their very low dexterity. They are also more susceptible to be ambushed and are much easier to be heard.",
                    "While in general, most enemies belong to certain chapter and will never spawn out of it, some enemies can be encountered in any part of the dungeon. They grow in strength to always represent adequate threat for the current floor. Most of them also have some kind of weakness which makes dealing with them much easier.",
                    "However, bosses take the cake for being the greatest threat in this dungeon. All of them are very powerful, durable and possess unique abilities. Worst of all, you can't evade the fight with them and you have to defeat them to descend deeper into the dungeon. They require preparation and attention to be defeated, but some of them also possess certain tricks to make fight with them easier.",
                    "But keep in mind that not everything in this dungeon wants you dead. Some denizens of this dungeon are quite friendly and can even give you a short quest to complete. Obviously, doing what they ask will net you a proper reward, but they can be simply ignored if you want, it will have almost no effect on your future progress.",
                    "Some of these NPCs do not want anything from you... except for your gold. There will be small shop on every fifth floor where you can sell your surplus items and buy something in return. Assortment and quality of items in these shops depends on the current chapter, but some of the items are guaranteed to be sold.",
                    "Finally, keep in mind that some of the enemies in this dungeon are of magical, unnatural origin, and thus can be immune to some effects which require living flesh and thinking mind to be affected. But this also makes them susceptible to some other effects which do not affect natural, living creatures."

        /*"Well, that's it for now. If you read this tutorial from the beginning to the end, then you now know everything what you need to start playing this game. Some of details are gonna be explained in loading tips (pay attention to them) and you can learn more about inner workings of the game by reading the YAPD article on the Pixel Dungeon wikia. Good luck, and watch out for mimics!"*/,
},
        };

	private BitmapText txtTitle;
	private ScrollPane list;

    private enum Tabs {

        INTERFACE,
        MECHANICS,
        CONSUMABLES,
        EQUIPMENT,
        DENIZENS,

    }

//	private ArrayList<Component> items = new ArrayList<>();

	private static Tabs currentTab;

	public WndTutorial() {
		
		super();

        icons = TextureCache.get( Assets.HELP );
        film = new TextureFilm( icons, 24, 24 );
		
		if (NoNameYetPixelDungeon.landscape()) {
			resize( WIDTH_L, HEIGHT_L );
		} else {
			resize( WIDTH_P, HEIGHT_P );
		}
		
		txtTitle = PixelScene.createText( TXT_TITLE, 9 );
		txtTitle.hardlight( Window.TITLE_COLOR );
		txtTitle.measure();
		add( txtTitle );
		
		list = new ScrollPane( new Component() );
		add( list );
		list.setRect( 0, txtTitle.height(), width, height - txtTitle.height() );

		Tab[] tabs = {
            new LabeledTab( TXT_LABELS[0] ) {
                @Override
                protected void select( boolean value ) {
                    super.select( value );

                    if( value ) {
                        currentTab = Tabs.INTERFACE;
                        updateList( TXT_TITLES[0] );
                    }
                }
            },
            new LabeledTab( TXT_LABELS[1] ) {
                @Override
				protected void select( boolean value ) {
					super.select( value );

                    if( value ) {
                        currentTab = Tabs.MECHANICS;
                        updateList( TXT_TITLES[1] );
                    }
				}
            },
            new LabeledTab( TXT_LABELS[2] ) {
                @Override
                protected void select( boolean value ) {
                    super.select( value );

                    if( value ) {
                        currentTab = Tabs.EQUIPMENT;
                        updateList( TXT_TITLES[2] );
                    }
                }
            },
            new LabeledTab( TXT_LABELS[3] ) {
                @Override
                protected void select( boolean value ) {
                    super.select( value );

                    if( value ) {
                        currentTab = Tabs.CONSUMABLES;
                        updateList( TXT_TITLES[3] );
                    }
                }
            },
            new LabeledTab( TXT_LABELS[4] ) {
                @Override
                protected void select( boolean value ) {
                    super.select( value );

                    if( value ) {
                        currentTab = Tabs.DENIZENS;
                        updateList( TXT_TITLES[4] );
                    }
                }
            },
		};

        int tabWidth = ( width + 12 ) / tabs.length ;

		for (Tab tab : tabs) {
			tab.setSize( tabWidth, tabHeight() );
			add( tab );
		}
		
		select( 0 );
	}
	
	private void updateList( String title ) {

		txtTitle.text( title );
		txtTitle.measure();
		txtTitle.x = PixelScene.align( PixelScene.uiCamera, (width - txtTitle.width()) / 2 );
		
//		items.clear();
		
		Component content = list.content();
		content.clear();
		list.scrollTo( 0, 0 );
		
		int index = 0;
		float pos = 0;

        switch( currentTab ) {

            case INTERFACE:
                for (String text : TXT_POINTS[0]) {
                    TutorialItem item = new TutorialItem(text, index++, width);
                    item.setRect(0, pos, width, item.height());
                    content.add(item);
//                    items.add(item);

                    pos += item.height();
                }
                break;

            case MECHANICS:

                index += 12;

                for (String text : TXT_POINTS[1]) {
                    TutorialItem item = new TutorialItem(text, index++, width);
                    item.setRect(0, pos, width, item.height());
                    content.add(item);
//                    items.add(item);

                    pos += item.height();
                }
                break;

            case EQUIPMENT:

                index += 24;

                for (String text : TXT_POINTS[2]) {
                    TutorialItem item = new TutorialItem(text, index++, width);
                    item.setRect(0, pos, width, item.height());
                    content.add(item);
//                    items.add(item);

                    pos += item.height();
                }
                break;

            case CONSUMABLES:

                index += 36;

                for (String text : TXT_POINTS[3]) {
                    TutorialItem item = new TutorialItem(text, index++, width);
                    item.setRect(0, pos, width, item.height());
                    content.add(item);
//                    items.add(item);

                    pos += item.height();
                }
                break;

            case DENIZENS:

                index += 48;

                for (String text : TXT_POINTS[4]) {
                    TutorialItem item = new TutorialItem(text, index++, width);
                    item.setRect(0, pos, width, item.height());
                    content.add(item);
//                    items.add(item);

                    pos += item.height();
                }
                break;

        }
		
		content.setSize( width, pos );
	}
	
	private static class TutorialItem extends Component {

        private final int GAP = 4;
        private Image icon;
		private BitmapTextMultiline label;
		
		public TutorialItem( String text, int index, int width ) {
			super();

            icon.frame( film.get( index ) );

            label.text( text );
            label.maxWidth = width - (int)icon.width() - GAP;
            label.measure();

            height = Math.max( icon.height(), label.height() ) + GAP;
		}
		
		@Override
		protected void createChildren() {

            icon = new Image( icons );
            add( icon );
			
			label = PixelScene.createMultiline( 6 );
			add( label );
		}
		
		@Override
		protected void layout() {
			icon.y = PixelScene.align( y );
			
			label.x = icon.x + icon.width;
			label.y = PixelScene.align( y );
		}
	}
}
