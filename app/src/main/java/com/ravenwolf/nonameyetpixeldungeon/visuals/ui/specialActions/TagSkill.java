/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.visuals.ui.specialActions;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.special.skills.BuffSkill;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.HeroSkill;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.windows.WndTitledMessage;
import com.watabou.noosa.Image;

public class TagSkill extends TagSpecialAction{

	BuffSkill skill;
	int index;

	{
		visible=false;
	}

	@Override
	protected void onClick() {
		if (enabled() && Dungeon.hero.ready) {
			skill.doAction();
		}
	}

	@Override
	protected boolean onLongClick() {
		if (Dungeon.hero.ready) {

			String cooldownText="";
			if (skill.getCD()>0)
				cooldownText="\n\nThis ability will be available in _"+(int)skill.getCD()+" turns_.";
			else
				cooldownText="\n\nAfter using this ability will be on cooldown for _"+(int)skill.getMaxCD()+" turns_.";
			Image icon = new Image( icons );
			if(index==1 && Dungeon.hero.skill1!=null && Dungeon.hero.skill1!=HeroSkill.NONE) {
				icon.frame( film.get( Dungeon.hero.skill1.icon() ) );
				GameScene.show(new WndTitledMessage(icon, Dungeon.hero.skill1.title(), Dungeon.hero.skill1.desc()+cooldownText));
			}else if(index==2 && Dungeon.hero.skill2!=null && Dungeon.hero.skill2!=HeroSkill.NONE) {
				icon.frame( film.get( Dungeon.hero.skill2.icon() ) );
				GameScene.show(new WndTitledMessage(icon, Dungeon.hero.skill2.title(), Dungeon.hero.skill2.desc()+cooldownText));
			}
			return true;
		}
		return false;
	}

	public TagSkill(int index){
		super();
		this.index=index;
	}



	@Override
	public void update() {

		super.update();
		if (visible && skill!=null){
			if (skill.getCD()!=0){
				cooldownRatio=skill.getCD()/skill.getMaxCD();
			}else
				cooldownRatio=0;

		}else{
			if(index==1 && Dungeon.hero.skill1!=null && Dungeon.hero.skill1!=HeroSkill.NONE){
				activate(Dungeon.hero.skill1);
			}else if(index==2 && Dungeon.hero.skill2!=null && Dungeon.hero.skill2!=HeroSkill.NONE){
				activate(Dungeon.hero.skill2);
			}
		}

	}

	private void activate(HeroSkill skill) {
		this.skill=Dungeon.hero.buff(skill.skillClass());
		setIcon(skill.icon());
		visible=true;
		bg.visible = true;
	}


}
