/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.visuals.windows;

import com.ravenwolf.nonameyetpixeldungeon.NoNameYetPixelDungeon;
import com.ravenwolf.nonameyetpixeldungeon.scenes.PixelScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.ScrollPane;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.Window;
import com.watabou.noosa.BitmapText;
import com.watabou.noosa.BitmapTextMultiline;
import com.watabou.noosa.ui.Component;

public class WndChangelog extends Window {

	private static final int WIDTH_P	= 128;
	private static final int HEIGHT_P	= 160;

	private static final int WIDTH_L	= 160;
	private static final int HEIGHT_L	= 128;

	private static final String TXT_TITLE	= "NNYPD 0.5 released!";

    private static final String TXT_DESCR =

        "Hi dungeoneers! After a long break, a new NNYPD version has been released!" +
        "\n" +
        "\n" +
        "_HIGHLIGHTS_\n\n" +

        "_2.5D Graphics!_\n " +
        " Visuals has been updated providing a perspective view. While similar to shattered look, there are some differences that I feel that look better with the rest of the graphics.\n\n " +
        "_Traps reworked_ \n" +
        " Several new traps added. Traps also vary based on chapters.\n\n "+
        "_Quest!_ \n" +
        " Added two different quests for Sad Ghost. \n" +
        " Added three different quests for the Wand Maker.\n\n"+
        "_Bosses tweaks!_ \n" +
        " Tengu and DM-300 bosses were tweaked introducing new special moves.\n "+
        " Yog-Dzewa now has an additional combat phase.\n "+

        "------" +
        "\n" +
        "\n" +


        "_0.5 LIST OF CHANGES_\n" +
        "\n" +
        "\n" +

        "_2.5D Tiles_\n"+
        "\n" +
        "- Improved graphics to 2.5D Style \n"+
        "- Added variations to floor and wall tiles to make the dungeon less monotone.\n" +
        "- This new feature can be toggled on/off in the main settings screen\n" +
        "\n" +
        "\n" +

        "_Traps Reworked_\n"+
        "\n" +
        "- Added 17 new traps and reworked existing ones \n"+
        "Traps can be generally categorized by its shape \n"+
        "- Lines: affect only the trapped tile \n"+
        "- Vertical/horizontal arrows: affects a line in vertical/horizontal orientation \n"+
        "- Square: Affects a 3x3 area \n"+
        "- Diagonal lines: release different gasses \n"+
        "- Stars: Summoning traps \n"+
        "\n" +
        "\n" +

        "_Levels_\n"+
        "\n" +
        "- New challenging quests for Sad Ghost and Wand Maker \n" +
        "- New regular rooms layouts different for each chapter \n" +
        "- A new Feeling was added: Permafrost. Ice Blocks don't decay. Some items will be encased in ice blocks. There will additional chilling and ice barrier traps\n" +
        "- Some special rooms will have traps that cannot be detected and will only trigger when stepped in\n" +
        "\n" +

        "_Heroes_\n"+
        "\n" +
        "- All Heroes now start with 10 Strength (Warrior gets -1 and huntress +1)\n" +
        "- Counter attacks are no longer automatic\n" +
        "- Blocking no longer grant block instances, instead will provide chance to block any number of attacks while is active, making it better against multiple enemies (or fast attacking ones)\n" +
        "- Ranged accuracy penalty tweaked (not a significant change, but should make ranged combat at max distance resulting in less misses)\n" +
        "- Ranged weapons (bows and crossbows) have less distance penalty\n" +
        "- Knight subclass bonus tweaked, no longer maintain guard when moving, instead it gain bonus accuracy and armor while guarding\n" +
        "\n" +
        "\n" +

        "_Enemies_\n"+
        "\n" +
        "- Slightly reduce enemies HP\n" +
        "- Skeletons no longer cause withering\n" +
        "- Swarm of flies no longer attracted by eating and don't reduce starvation on hit, instead they have a chance to wither enemies\n" +
        "- Gnoll Berserker no longer have a ranged attack, drop changed from ranged weapon to potion of healing\n" +
        "- Bandits ranged attack changed from 3 attacks to a cooldown ranged attack\n" +
        "- Wraiths ranged attack now have a cooldown\n" +
        "- Plague doctors are immune to Miasma\n" +
        "- Robots can now be affected by domination\n" +
        "- Reduced XP granted by Wraiths, Piranhas and Statues\n" +
        "\n" +
        "\n" +

        "_Bosses_\n"+
        "\n" +
        "- DK no longer enrages after ritual\n" +
        "- DK is immortal on the first turns of the battle\n" +
        "- Necromancer bone surge spawn closer to the hero and deals half damage to abomination\n" +
        "- Abomination hook causes bleeding instead of cripple and can be used from closer distance\n" +
        "\n" +
        "\n" +
        " Tengu\n" +
        "- Reworked level. Arena slightly larger\n" +
        "- Added two new special moves. No longer enrages\n" +
        "\n" +
        "\n" +
        " DM-300\n" +
        "- No longer gain shield from inactive traps, instead there is a special new trap (power cell), that grant shield to DM and shock the player if stepped in\n" +
        "- Added two new special ranged attacks\n" +
        "- Moves at regular speed. No longer avoid traps.\n" +
        "- Enrage phase replaced with overloaded phase: Returns melee damage, and cause directional damage aiming nearby power cells when moving.\n" +
        "\n" +
        "\n" +
        " Yog-Dzewa\n" +
        "- A new initial -tentacle- phase added to Yog battle\n" +
        "- For now there are two different aspects for the first phase. A random one will be chosen each run\n" +
        "- After beating the first phase, Yog will summon their fists \n" +
        "- Fists stats were reduced and attack speed was halved\n" +
        "- Fists are linked to Yog, damaging them will transfer half of the damage done to Yog\n" +
        "\n" +
        "\n" +

        "_Equipment_\n"+
        "\n" +
        "- Potions and bombs can be thrown over enemies (will activate on the targeted cell instead of stoping on the fist enemy in its trajectory)\n" +
        "- Reduced tier 4 weapon scaling by upgrades \n" +
        "- Slightly reduced wands damage\n" +
        "- Slightly reduced magic missile recharge rate\n" +
        "\n" +
        "\n" +

        "_Misc_\n"+
        "\n" +
        "- Reworked Sad Ghost and Wand Maker quests\n" +
        "- Reduced prices of special NPCs\n" +
        "- Bleeding debuff visual effect now match the affected target blood color (a small change that makes bleed more realistic for enemies without red blood)\n" +
        "\n" +
        "\n" +

        "_Fixes_\n"+
        "\n" +
        "- Some rare crash when equipping ranged weapons from quickslot and not having a melee weapon\n" +
        "- Dwarf king freezing if the throne was occupied after activating bone pits\n" +
        "- Some movement issues when dazed\n" +
        "- Fixed some ranged enemies attacking the hero when out of their field of view in some cases\n" +
        "- Several minor fixes\n" +

        "\n" +
        "\n" +
        "------" +
        "\n" +
        "\n";


	private BitmapText txtTitle;
	private ScrollPane list;

	public WndChangelog() {
		
		super();
		
		if (NoNameYetPixelDungeon.landscape()) {
			resize( WIDTH_L, HEIGHT_L );
		} else {
            resize( WIDTH_P, HEIGHT_P );
		}
		
		txtTitle = PixelScene.createText( TXT_TITLE, 9 );
		txtTitle.hardlight( Window.TITLE_COLOR );
		txtTitle.measure();
        txtTitle.x = PixelScene.align( PixelScene.uiCamera, (width - txtTitle.width() ) / 2 );
        add( txtTitle );

        list = new ScrollPane( new ChangelogItem( TXT_DESCR, width, txtTitle.height() ) );
        add( list );

        list.setRect( 0, txtTitle.height(), width, height - txtTitle.height() );
        list.scrollTo( 0, 0 );

	}

    private static class ChangelogItem extends Component {

        private final int GAP = 4;

        private BitmapTextMultiline normal;
        private BitmapTextMultiline highlighted;

        public ChangelogItem( String text, int width, float offset ) {
            super();

//            label.text( text );
//            label.maxWidth = width;
//            label.measure();

            Highlighter hl = new Highlighter( text );

//            normal = PixelScene.createMultiline( hl.text, 6 );
            normal.text( hl.text );
            normal.maxWidth = width;
            normal.measure();
//            normal.x = 0;
//            normal.y = offset;
//            add( normal );

            if (hl.isHighlighted()) {
                normal.mask = hl.inverted();

//                highlighted = PixelScene.createMultiline( hl.text, 6 );
                highlighted.text( hl.text );
                highlighted.maxWidth = normal.maxWidth;
                highlighted.measure();
//                highlighted.x = normal.x;
//                highlighted.y = normal.y;
//                add( highlighted );

                highlighted.mask = hl.mask;
                highlighted.hardlight( TITLE_COLOR );
            }

            height = normal.height() + GAP;
        }

        @Override
        protected void createChildren() {
            normal = PixelScene.createMultiline( 6 );
            add( normal );
            highlighted = PixelScene.createMultiline( 6 );
            add( highlighted );
        }

        @Override
        protected void layout() {
            normal.y = PixelScene.align( y + GAP );
            highlighted.y = PixelScene.align( y + GAP );
        }
    }
}
