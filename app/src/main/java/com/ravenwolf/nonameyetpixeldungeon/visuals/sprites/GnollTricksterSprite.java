/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.visuals.sprites;

import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.watabou.noosa.TextureFilm;

public class GnollTricksterSprite extends MobSprite {

	private Animation shadowWalking;

    public GnollTricksterSprite() {
		super();
		
		texture( Assets.GNOLL );
		
		TextureFilm frames = new TextureFilm( texture, 12, 15 );

		int offset = 42;


		idle = new Animation( 2, true );
		idle.frames( frames, 0+offset, 0+offset, 0+offset, 1+offset, 0+offset, 0+offset, 1+offset, 1+offset);

		run = new Animation( 12, true );
		run.frames( frames, 4+offset, 5+offset, 6+offset, 7+offset );

		attack = new Animation( 12, false );
		attack.frames( frames, 2+offset, 3+offset, 0+offset );

		die = new Animation( 12, false );
		die.frames( frames, 8+offset, 9+offset, 10+offset );

		sleep = new Animation(1,true);
		sleep.frames( frames, 12+offset,13+offset);

		shadowWalking = new Animation( 1, true );
		shadowWalking.frames( frames, 13+offset );
		
		play( idle );
	}

	public void blink( int from, int to ) {
		place( to );
		play( shadowWalking );
		//isMoving = true;
		turnTo( from , to );

		ch.onMotionComplete();
	}
}
