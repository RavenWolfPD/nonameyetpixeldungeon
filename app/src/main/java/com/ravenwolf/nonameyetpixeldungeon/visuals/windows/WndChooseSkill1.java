/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.visuals.windows;

import com.ravenwolf.nonameyetpixeldungeon.actors.hero.HeroSkill;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.HeroSubClass;
import com.ravenwolf.nonameyetpixeldungeon.items.misc.TomeOfMastery;
import com.ravenwolf.nonameyetpixeldungeon.items.misc.TomeOfMasterySkill1;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.Utils;
import com.ravenwolf.nonameyetpixeldungeon.scenes.PixelScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.RedButton;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.Window;
import com.watabou.gltextures.SmartTexture;
import com.watabou.gltextures.TextureCache;
import com.watabou.noosa.BitmapText;
import com.watabou.noosa.BitmapTextMultiline;
import com.watabou.noosa.Image;
import com.watabou.noosa.TextureFilm;

public class WndChooseSkill1 extends Window {

	private static final String TXT_MESSAGE	= "Are you ready to get such power?";
	private static final String TXT_DESC	= "This tome contains an exceptional power that will be transfer to the reader, allowing them to perform special techniques";
	private static final String TXT_YES	= "Yes";
	private static final String TXT_CANCEL	= "I'll decide later";

	private static final int WIDTH		= 120;
	private static final int BTN_HEIGHT	= 18;
	private static final float GAP		= 2;

	private static SmartTexture icons;
	private static TextureFilm film;

	public WndChooseSkill1(final TomeOfMasterySkill1 tome, final HeroSkill skill) {

		super();

		icons = TextureCache.get(Assets.SKILLS);
		film = new TextureFilm(icons, 16, 16);

		IconTitle titlebar = new IconTitle();
		titlebar.icon( new ItemSprite( tome.image(), null ) );
		titlebar.label( tome.name() );
		titlebar.setRect( 0, 0, WIDTH, 0 );
		add( titlebar );

		BitmapTextMultiline txtDesc = PixelScene.createMultiline( Utils.capitalize( TXT_DESC), 6 );
		txtDesc.maxWidth = WIDTH;
		txtDesc.x = titlebar.left();
		txtDesc.y = titlebar.bottom() + GAP;
		txtDesc.measure();
		add( txtDesc );

		BitmapText txtTitle = PixelScene.createText(  Utils.capitalize( skill.title()), 8);
		txtTitle.hardlight( Window.TITLE_COLOR );
		txtTitle.measure();
		txtTitle.x = WIDTH/2 -txtTitle.width()/2;
		txtTitle.y = txtDesc.y + txtDesc.height() + GAP;
		add( txtTitle );

		Image icon = new Image( icons );
		icon.frame( film.get( skill.icon() ) );
		icon.y =  txtTitle.y + txtTitle.height() +GAP;
		icon.x =  WIDTH/2 -8;
		add( icon );

		Highlighter hl = new Highlighter( skill.desc() + "\n\n" + TXT_MESSAGE );
		
		BitmapTextMultiline normal = PixelScene.createMultiline( hl.text, 6 );
		normal.maxWidth = WIDTH;
		normal.measure();
		normal.x = titlebar.left();
		normal.y = icon.y + icon.height() + GAP;
		add( normal );
		
		if (hl.isHighlighted()) {
			normal.mask = hl.inverted();
			
			BitmapTextMultiline highlighted = PixelScene.createMultiline( hl.text, 6 );
			highlighted.maxWidth = normal.maxWidth;
			highlighted.measure();
			highlighted.x = normal.x;
			highlighted.y = normal.y;
			add( highlighted );
	
			highlighted.mask = hl.mask;
			highlighted.hardlight( TITLE_COLOR );
		}
		
		RedButton btnWay1 = new RedButton( Utils.capitalize( TXT_YES ) ) {
			@Override
			protected void onClick() {
				hide();
				tome.choose( skill );
			}
		};
		btnWay1.setRect( 0, normal.y + normal.height() + GAP, WIDTH, BTN_HEIGHT );
		add( btnWay1 );
		

		
		RedButton btnCancel = new RedButton( TXT_CANCEL ) {
			@Override
			protected void onClick() {
				hide();
			}
		};
		btnCancel.setRect( 0, btnWay1.bottom() + GAP, WIDTH, BTN_HEIGHT );
		add( btnCancel );
		
		resize( WIDTH, (int)btnCancel.bottom() );
	}
}
