/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon;

import android.graphics.RectF;

import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.traps.Trap;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.watabou.noosa.Image;
import com.watabou.utils.SparseArray;

public class DungeonTerrainFeaturesTilemap extends BaseTilemap {

	public static final int SIZE = 16;

	private static DungeonTerrainFeaturesTilemap instance;

	private SparseArray<Trap> traps;

	public DungeonTerrainFeaturesTilemap( SparseArray<Trap> traps) {
		super(Assets.TERRAIN_FEATURES);
		this.traps= traps;
		map( Dungeon.level.map, Level.WIDTH );

		instance = this;
	}


	public static Image tile(int pos ) {
		RectF uv = instance.tileset.get( instance.getTile( pos ) );
		if (uv == null) return null;

		Image img = new Image( instance.texture );
		img.frame(uv);
		return img;
	}

	public void map( int[] data, int cols ) {
		int[] raisedData = new int[Dungeon.level.LENGTH];

		for (int i=0; i < Dungeon.level.LENGTH; i++) {
			raisedData[i] =  getTile(i);
		}
		super.map(raisedData, cols);
	}

	private int getTile(int cell){

		Trap trap = traps.get(cell);
		if (trap != null && trap.visible) {
			return (trap.active ? trap.color : Trap.BLACK) + (trap.shape * 16);
		}
		return 15;//15 is empty
	}


	public void updateTerrainFeatureTile( int pos) {
			data[pos]=getTile(pos);;
			updated.union(pos % Level.WIDTH, pos / Level.WIDTH);
	}

}
