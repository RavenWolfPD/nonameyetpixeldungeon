/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.wands;

import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Charmed;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Controlled;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.MagicMissile;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Callback;

public class CharmOfDomination extends WandUtility{

	{
		name = "Charm of Domination";
		image = ItemSpriteSheet.CHARM_DOMINATION;
	}

	protected void cursedProc(Hero hero){
		curCharges = (curCharges+1)/2;
		BuffActive.addFromDamage(hero, Charmed.class, damageRoll()*2);
	}

	public int basePower(){
		return super.basePower()+4;
	}

	@Override
	protected void onZap( int cell ) {
		super.onZap(cell );
		Char ch = Actor.findChar( cell );
		if (ch != null) {
			Controlled buff = BuffActive.addFromDamage(ch, Controlled.class, damageRoll()*6 );
			if( buff != null ) {
				buff.object = curUser.id();
			}
		} else {
			GLog.i( "nothing happened" );
		}
	}
	
	protected void fx( int cell, Callback callback ) {
		MagicMissile.purpleLight( curUser.sprite.parent, curUser.pos, cell, callback );
		Sample.INSTANCE.play( Assets.SND_ZAP );
	}
	
	@Override
	public String desc() {
		return
			"The purple light from this charm will hypnotize your target, dominating its will and forcing it " +
			"to protect you against other enemies for a while. Although, causing damage to the controlled enemy will release it from your control, " +
					"still the target will remain weakened for the duration.";
	}

	protected String getEffectDescription(int min , int max, boolean known){
		return  "this charm will " +(known? "":"(probably) ")+"control enemies with a power of _" + min + "-" + max + "_ ";
	}
}
