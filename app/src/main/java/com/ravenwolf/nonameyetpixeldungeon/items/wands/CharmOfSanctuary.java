/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.wands;


import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Sanctuary;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Tormented;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.HolyLight;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Callback;


public class CharmOfSanctuary extends WandUtility {

	{
		name = "Charm of Sanctuary";
		hitChars = false;
		image = ItemSpriteSheet.CHARM_BLESS;
	}


	protected void cursedProc(Hero hero){
		curCharges = (curCharges+1)/2;
		BuffActive.addFromDamage(hero, Tormented.class, damageRoll());
	}

	@Override
	protected void onZap( int cell ) {
		super.onZap(cell );
		if( Level.solid[ cell ] )
			cell = Ballistica.trace[ Ballistica.distance - 1 ];
		int dmg=damageRoll();
		for (int i : Level.NEIGHBOURS9) {
			int pos=cell+i;
			if (!Level.solid[pos])
				GameScene.add( Blob.seed( pos, dmg /2, Sanctuary.class ) );
		}
	}

	@Override
	protected void fx( int cell, Callback callback ) {

		if( Level.solid[ cell ] )
			cell = Ballistica.trace[ Ballistica.distance - 1 ];
		if( Dungeon.visible[ cell ] ){
			Sample.INSTANCE.play( Assets.SND_LEVELUP, 0.5f, 0.5f, 0.5f );
			HolyLight.createAtPos( cell );
		}
		Sample.INSTANCE.play( Assets.SND_ZAP );
		curUser.sprite.cast(cell,callback);
	}
	
	@Override
	public String desc() {
		return 
			"This golden charm have the power to create a field of holy light " +
				"that will disrupt magical entities presents in the sacred ground. If stand for too long, disrupted enemies will be also banished. " +
				"The owner instead, will be protected by the light, becoming more resistant to magical and physical damage.";
	}

	protected String getEffectDescription(int min , int max, boolean known){
		return  "this charm will " +(known? "":"(probably) ")+"bless the target area for _" + min/2 + "-" + max/2 + " turns_ ";
	}

}
