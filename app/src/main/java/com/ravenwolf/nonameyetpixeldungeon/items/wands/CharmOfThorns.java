/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.wands;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Bleeding;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Ensnared;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.Mob;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs.NPC;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.levels.Terrain;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.MagicMissile;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.LeafParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ThornvineSprite;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.Random;

import java.util.ArrayList;
import java.util.HashSet;

public class CharmOfThorns extends WandUtility {

	{
		name = "Charm of Thorns";
        image = ItemSpriteSheet.CHARM_THORN;
	}

	private static int HP_MODIFIER = 2;


    @Override
    protected void cursedProc(Hero hero){
        curCharges = (curCharges+1)/2;
        int dmg=hero.absorb( damageRoll(), true )/2;
        hero.damage(dmg, this, Element.PHYSICAL);
        BuffActive.addFromDamage( hero, Bleeding.class, dmg );
    }

    public int basePower(){
        return super.basePower()-4;
    }

    @Override
    public int damageRoll() {
        if( curUser != null ) {
            float eff = effectiveness();
            return getMaxDamage(curUser.magicSkill, eff);
        } else {
            return 0;
        }
    }

	@Override
	protected void onZap( int cell ) {

        spawnThornvine(cell);
	}

    protected <T extends Thornvine> void spawnThornvine( int cell) {

        int stats = damageRoll();

        // first we check the targeted tile
        if( Thornvine.spawnAt( stats, curCharges, cell) == null ) {

            // then we check the previous tile
            int prevCell = Ballistica.trace[ Ballistica.distance - 1 ];

            if( Thornvine.spawnAt( stats, curCharges, prevCell) == null ) {

                // and THEN we check all tiles around the targeted
                ArrayList<Integer> candidates = new ArrayList<Integer>();

                for (int n : Level.NEIGHBOURS8) {
                    int pos = cell + n;
                    if( Level.adjacent( pos, prevCell ) && !Level.solid[ pos ] && !Level.chasm[ pos ] && Actor.findChar( pos ) == null ){
                        candidates.add( pos );
                    }
                }

                if ( candidates.size() > 0 ){
                    Thornvine.spawnAt( stats, curCharges, candidates.get( Random.Int( candidates.size() ) ));
                } else {
                    //if cant spawn thornvine but targeted char, deals damage and bleed
                    Char enemy=Actor.findChar( cell );
                    if (enemy!=null){
                        int dmg=damageRoll();
                        enemy.damage(dmg, this, Element.PHYSICAL);
                        BuffActive.addFromDamage( enemy, Bleeding.class, dmg );
                        CellEmitter.get(cell).burst(LeafParticle.LEVEL_SPECIFIC, Random.IntRange(4, 6));
                    }
                    else
                        GLog.i( "nothing happened" );
                }
            }
        }

        CellEmitter.center( cell ).burst( LeafParticle.GENERAL, 5 );
        Sample.INSTANCE.play( Assets.SND_PLANT );
    }

	protected void fx( int cell, Callback callback ) {
		MagicMissile.foliage( curUser.sprite.parent, curUser.pos, cell, callback );
		Sample.INSTANCE.play( Assets.SND_ZAP );
	}

	@Override
	public String desc() {
		return
			"This wand possesses the mystical power of summoning forces of the very earth to the " +
            "wielder's command, allowing to spawn thornvines from the floor. These " +
            "plants will fiercely lash out at any enemy passing through, and are tougher " +
            "when created on grass-covered areas.";
	}

    protected String getEffectDescription(int min , int max, boolean known){

        int charges = known ? 3 + bonus : 3;
        int HP = (4 + charges * 4  ) * HP_MODIFIER;
        return  "Thornvines summoned by this charm will " +(known? "":"(probably) ")+"deal _" + max *2 / 5+ "-" + max * 3 / 4+ " points of damage_ " +
                "and have _" + HP + " health_ ";
    }


    public static class Thornvine extends NPC {

        private int stats;
        private int charges;

        public Thornvine() {

            name = "thornvine";
            spriteClass = ThornvineSprite.class;

            resistances.put(Element.Flame.class, Element.Resist.VULNERABLE);
            resistances.put(Element.Shock.class, Element.Resist.PARTIAL);

            resistances.put(Element.Mind.class, Element.Resist.IMMUNE);


            PASSIVE = new Guarding();
            hostile = false;
            friendly = true;

        }

        public boolean immovable() {
            return true;
        }

        @Override
        public boolean isMagical() {
            return false;
        }

        @Override
        protected boolean getCloser(int target) {
            return true;
        }

        @Override
        protected boolean getFurther(int target) {
            return true;
        }

        @Override
        public int viewDistance() {
            return 1;
        }


        @Override
        protected boolean act() {
            HP -= HP_MODIFIER;
            if (HP <= 0) {
                die(this);
                return true;
            }

            return super.act();
        }

        @Override
        public boolean add(Buff buff) {
            if (buff instanceof Ensnared)
                return false;
            else
                return super.add(buff);
        }


        @Override
        public void interact() {
            Dungeon.hero.sprite.operate(pos);

            if (Dungeon.hero.belongings.weap2 instanceof CharmOfThorns) {
                CharmOfThorns charm = (CharmOfThorns) Dungeon.hero.belongings.weap2;
                // we restore at least one charge less than what was spent on the vine
                charm.recharge( (int) ((HP  * HP_MODIFIER / HT) * charm.rechargeRate())
                );
                GLog.i("You recall the thornvine into the charm.");
            } else {
                GLog.i("You unsummon the thornvine.");
            }

            Sample.INSTANCE.play(Assets.SND_PLANT);

            Dungeon.hero.spend(TICK);
            Dungeon.hero.busy();

            die(this);
        }

        @Override
        public int attackProc(Char enemy, int damage, boolean blocked) {

            // thornvines apply bleed on hit
            if (!blocked) {
                BuffActive.addFromDamage(enemy, Bleeding.class, damage / 2);
            }

            return damage;
        }

        //lose one HP after each attack
        public void onAttackComplete() {
            super.onAttackComplete();
            HP -= HP_MODIFIER;
            if (HP <= 0)
                die(this);
        }

        @Override
        protected Char chooseEnemy() {

            // thornvines attack your enemies by default
            if (enemy == null || !enemy.isAlive() || enemy.invulnerable()) {

                HashSet<Mob> enemies = new HashSet<Mob>();

                for (Mob mob : Dungeon.level.mobs) {
                    if (mob.hostile && !mob.isFriendly() && !mob.invulnerable() && Level.fieldOfView[mob.pos]) {
                        enemies.add(mob);
                    }
                }
                return enemies.size() > 0 ? Random.element(enemies) : null;

            } else {

                return enemy;

            }
        }

        protected void adjustStats(int stats, int charges) {
            HT =  (4 + charges * 4  ) * HP_MODIFIER;
            armorClass = stats * 2 / 5;

            maxDamage = stats * 3 / 4;
            minDamage = stats * 2 / 5;

            accuracy = stats;
            dexterity = stats / 2;

            this.stats = stats;
            this.charges = charges;
        }

        @SuppressWarnings("unchecked")
        public static Thornvine spawnAt(int stats, int charges,  int pos) {

            // cannot spawn on walls, chasms or already occupied tiles
            if (!Level.solid[pos] && !Level.chasm[pos] && Actor.findChar(pos) == null) {

                Thornvine vine = new Thornvine();

                if (Dungeon.level.map[pos] == Terrain.GRASS || Dungeon.level.map[pos] == Terrain.HIGH_GRASS) {
                    charges += 2;
                }

                vine.pos = pos;
                vine.enemySeen = true;
                vine.state = vine.PASSIVE;

                GameScene.add(vine, 0f);
                Dungeon.level.press(vine.pos, vine);

                vine.sprite.emitter().burst(LeafParticle.LEVEL_SPECIFIC, 5);
                vine.sprite.spawn();

                vine.adjustStats(stats, charges);
                vine.HP = vine.HT;

                return vine;

            } else {

                return null;

            }
        }

        public void knockBack(int sourcePos, int damage, int amount, final Callback callback) {
            //kill thornvine if knockBack
            die(null);
            if (callback != null)
                callback.call();
            return;
        }


        private class Guarding extends Passive {

            @Override
            public boolean act( boolean enemyInFOV, boolean justAlerted ){

                if (enemyInFOV && canAttack( enemy ) && enemy != Dungeon.hero ) {

                    return doAttack( enemy );

                } else {
                    //reset enemy if cannot attack
                    resetEnemy();
                    spend( TICK );
                    return true;

                }
            }

            @Override
            public String status(){
                return "guarding";
            }
        }

        @Override
        public String description() {
            return
                "Thornvines are kind of semisentient plants which are very territorial and will " +
                "attack anything which comes near. Their sharp thorns can inflict grievous wounds, " +
                "but they are very vulnerable to fire and will quickly wither as time passes.";
        }

        private static final String STATS	= "stats";
        private static final String CHARGES	= "charges";

        @Override
        public void storeInBundle( Bundle bundle ) {
            super.storeInBundle(bundle);
            bundle.put( STATS, stats );
            bundle.put(CHARGES, charges);
        }

        @Override
        public void restoreFromBundle( Bundle bundle ) {
            super.restoreFromBundle(bundle);
            adjustStats( bundle.getInt( STATS ),  bundle.getInt(CHARGES) );
        }
    }

}
