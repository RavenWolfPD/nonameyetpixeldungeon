/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.weapons.ranged;

import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.criticals.BladeCritical;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.throwing.Bullets;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.throwing.ThrowingWeaponAmmo;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.layers.RangedWeaponSprite;
import com.watabou.utils.Random;

public class Handcannon extends RangedWeaponFlintlock {

	{
		name = "handcannon";
		image = ItemSpriteSheet.HANDCANNON;
		drawId= RangedWeaponSprite.HANDCANNON;
		critical=new BladeCritical(this, true, 1f);
	}

	public Handcannon() {
		super( 5 );
	}

	@Override
	public boolean incompatibleWithShield() {
		return true;
	}

	@Override
	public Type weaponType() {
		return Type.R_FLINTLOCK;
	}
	
	@Override
	public String desc() {
		return "While initially flintlock weapons were a human invention, dwarves quickly adapted " +
                "and improved the concept. Handcannons are heavy and require lots of gunpowder to " +
                "use, but they are far more deadly than any other firearm."+
				"\n\n This weapon will inflict improved critical effects.";
	}


	@Override
	public int proc(Char attacker, Char defender, int damage ) {
		damage=super.proc(attacker, defender, damage);
		if (Random.Int(defender.HT) < damage/2) {
			if (defender != null) {
				defender.knockBack( attacker, damage);
			}
		}
		return damage;
	}

}
