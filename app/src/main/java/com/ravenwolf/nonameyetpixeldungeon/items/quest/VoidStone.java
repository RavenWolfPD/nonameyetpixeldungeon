/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.quest;

import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;

public class VoidStone extends Item {
	
	{
		name = "void stone";
		image = ItemSpriteSheet.VOID_STONE;
		
		unique = true;
	}
	
	@Override
	public String info() {
		return
			"A smooth stone filled with a swirling cloud of void that constantly shifts." +
					" You feel a sense of unease holding the stone.";
	}
}
