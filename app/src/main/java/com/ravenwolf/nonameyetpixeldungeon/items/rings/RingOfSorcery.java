/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.rings;

import java.util.Locale;

public class RingOfSorcery extends Ring {

	{
		name = "Ring of Sorcery";
        shortName = "So";
	}
	
	@Override
	protected RingBuff buff( ) {
		return new Sorcery();
	}

    @Override
    public String desc() {

        String mainEffect = "??";
        String sideEffect = "??";

        if( isIdentified() ){
            mainEffect = String.format( Locale.getDefault(), "%.0f", 100 * effect( bonus ) / 2 );
            sideEffect = String.format( Locale.getDefault(), "%.0f", 100 * effect( bonus ));
        }

        StringBuilder desc = new StringBuilder(
                "This ring was enchanted to increase magical sensitivity of it's wearer. Rings of " +
                        "this kind are often used by spellcasters of all kinds, since having greater " +
                        "control of magical currents really helps in their line of work."
        );

        desc.append( "\n\n" );
        desc.append( super.desc() );
        desc.append( " " );

        desc.append(
                "Wearing this ring will increase your magic skill by _" + mainEffect + "%_ and chance " +
                        "to proceed of enchants on your weapons and armors by _" + sideEffect + "%_."
        );

        return desc.toString();
    }


    public class Sorcery extends RingBuff {
        @Override
        public String desc() {
            return !isCursed() ?
                    "Your arcane proficiency is improved." :
                    "Your arcane proficiency is decreased." ;
        }
    }
}
