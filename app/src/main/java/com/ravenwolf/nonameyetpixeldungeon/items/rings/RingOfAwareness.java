/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.rings;

import java.util.Locale;

public class RingOfAwareness extends Ring {

	{
		name = "Ring of Awareness";
        shortName = "Aw";
	}
	
//	@Override
//	public boolean doEquip( Hero hero ) {
//		if (super.doEquip( hero )) {
//			Dungeon.hero.search( false );
//			return true;
//		} else {
//			return false;
//		}
//	}
	
	@Override
	protected RingBuff buff( ) {
		return new Awareness();
	}
	
	@Override
	public String desc() {

        String mainEffect = "??";
        //String sideEffect = "??";

        if( isIdentified() ){
            mainEffect = String.format( Locale.getDefault(), "%.0f", 100 * effect( bonus ) / 2 );
            //sideEffect = String.format( Locale.getDefault(), "%.0f", 100 * effect( bonus ) /2);
        }

        StringBuilder desc = new StringBuilder(
            "These rings improve wearer's perception, making him or her to be more aware " +
            "of different threats as well as allowing for a much deadlier counterattacks."
        );

        desc.append( "\n\n" );

        desc.append( super.desc() );

        desc.append( " " );

        desc.append(
            "Wearing this ring will increase your perception and bonus damage from counter attacks by _" + mainEffect + "%_."
        );

        return desc.toString();

	}
	
	public class Awareness extends RingBuff {
        @Override
        public String desc() {
            return !isCursed() ?
                    "You feel that your alertness is improved." :
                    "You feel that your alertness is dimmed." ;
        }
	}
}
