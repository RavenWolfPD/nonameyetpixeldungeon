/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.wands;

import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Miasma;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Decay;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.DoomSkull;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.MagicMissile;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.watabou.noosa.audio.Sample;
import com.watabou.utils.Callback;

public class CharmOfDecay extends WandUtility {

	{
		name = "Charm of Decay";
		image = ItemSpriteSheet.CHARM_BONE;
	}

	@Override
	protected void cursedProc(Hero hero){
		curCharges = (curCharges+1)/2;
		GameScene.add( Blob.seed( hero.pos, damageRoll()*6, Miasma.class ) );
	}

	@Override
	protected void onZap( int cell ) {
		super.onZap(cell );
		if( Level.solid[ cell ] ) {
			cell = Ballistica.trace[ Ballistica.distance - 1 ];
		}
		Char ch = Actor.findChar( cell );
		if (ch != null) {
			BuffActive.addFromDamage(ch, Decay.class, damageRoll()*4 );
			Sample.INSTANCE.play(Assets.SND_DEGRADE);
			DoomSkull.createAtChar(ch);

		} else
			GameScene.add( Blob.seed( cell, damageRoll()*10, Miasma.class ) );
	}


	protected void fx( int cell, Callback callback ) {
		if( Level.solid[ cell ] ) {
			cell = Ballistica.trace[ Ballistica.distance - 1 ];
		}
		MagicMissile.miasma( curUser.sprite.parent, curUser.pos, cell, callback );
		Sample.INSTANCE.play( Assets.SND_ZAP );
	}


	@Override
	public String desc() {
		return 	"Crafted from bones and dark crystals, this charm can infuse its victims with a powerful curse. " +
				"Affected targets will suffer unholy damage, and their combat capabilities, such strength and resistance, will decrease as the curse grows in power. " +
				"When focused on the ground, the unholy energy of this charm will be released as a cloud of toxic miasma.";
	}

	protected String getEffectDescription(int min , int max, boolean known){
		return  "this charm will " +(known? "":"(probably) ")+"curse enemies with a power of _" + min + "-" + max + "_ ";
	}
}
