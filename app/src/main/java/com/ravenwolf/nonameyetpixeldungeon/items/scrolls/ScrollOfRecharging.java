/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.scrolls;

import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Darkness;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.bonuses.Recharging;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.SpellSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.EnergyParticle;
import com.watabou.noosa.audio.Sample;

public class ScrollOfRecharging extends Scroll {

    private static final String TXT_MESSAGE	= "A surge of energy courses through your body, invigorating your wands!";

	{
		name = "Scroll of Recharging";
        shortName = "Re";

        spellSprite = SpellSprite.SCROLL_SUNLIGHT;
        spellColour = SpellSprite.COLOUR_RUNE;
        icon=7;
	}
	
	@Override
	protected void doRead() {

        int duration=(int)(14.0f * ( 110 + curUser.magicSkill() ) / 100);
        BuffActive.add(curUser, Recharging.class, duration);
        Recharging.recharge(curUser, duration);
        curUser.sprite.centerEmitter().burst(EnergyParticle.FACTORY, (int) Math.sqrt(duration / 2) + 2);

        GLog.i( TXT_MESSAGE );

        super.doRead();
	}


    @Override
	public String desc() {
		return
			"The raw magical power bound up in this parchment will, charge up all the users wands over time when released";
	}

    @Override
    public int price() {
        return isTypeKnown() ? 30 * quantity : super.price();
    }
}
