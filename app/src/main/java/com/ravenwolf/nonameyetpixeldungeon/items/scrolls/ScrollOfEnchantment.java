/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.scrolls;

import com.ravenwolf.nonameyetpixeldungeon.items.Item;
import com.ravenwolf.nonameyetpixeldungeon.items.armours.Armour;
import com.ravenwolf.nonameyetpixeldungeon.items.rings.Ring;
import com.ravenwolf.nonameyetpixeldungeon.items.wands.Wand;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.Weapon;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.SpellSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.EnergyParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.ShadowParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.windows.WndBag;

public class ScrollOfEnchantment extends InventoryScroll {

    private static final String TXT_ITEM_ENCHANT	= "the scroll turned your %s into %s!";
    private static final String TXT_ITEM_UPGRADE	= "the scroll upgraded your %s because it is already enchanted!";
    private static final String TXT_ITEM_RESTORED	= "the scroll restored the enchantment on your %s!";
    private static final String TXT_ITEM_UNCURSE	= "the scroll removed the curse from your %s!";
	private static final String TXT_ITEM_UNKNOWN	= "the scroll is useless for the %s!";

	{
		name = "Scroll of Enchantment";
        shortName = "En";

		inventoryTitle = "Select an enchantable item";
		mode = WndBag.Mode.ENCHANTABLE;

        spellSprite = SpellSprite.SCROLL_ENCHANT;
        spellColour = SpellSprite.COLOUR_RUNE;
        icon=2;
	}
	
	@Override
    public void onItemSelected(Item item) {

        if (item instanceof Weapon) {

            Weapon weapon = (Weapon)item;
            
            if( !weapon.isCursed() ) {

                weapon.identify(ENCHANT_KNOWN);
                weapon.enchant();
                GLog.i( TXT_ITEM_ENCHANT, weapon.simpleName(), weapon.name() );
                curUser.sprite.centerEmitter().burst(EnergyParticle.FACTORY, 10);

            } else {

                weapon.cursed=false;
                curUser.sprite.emitter().start( ShadowParticle.UP, 0.05f, 10 );
                GLog.i( TXT_ITEM_RESTORED, weapon.simpleName(), weapon.name() );
                weapon.identify( CURSED_KNOWN );
            }

        } else if (item instanceof Armour) {

            Armour armour = (Armour)item;

            if( !armour.isCursed()) {

                armour.identify(ENCHANT_KNOWN);
                armour.inscribe();
                GLog.i( TXT_ITEM_ENCHANT, armour.simpleName(), armour.name() );

                curUser.sprite.centerEmitter().burst(EnergyParticle.FACTORY, 10);
            } else {

                armour.cursed=false;
                curUser.sprite.emitter().start( ShadowParticle.UP, 0.05f, 10 );
                GLog.i( TXT_ITEM_RESTORED, armour.simpleName(), armour.name() );
                armour.identify( CURSED_KNOWN );
            }


        } else if ( item instanceof Wand || item instanceof Ring) {

            item.identify( CURSED_KNOWN );

            if( !item.isCursed() ) {

                ScrollOfTransmutation.transmute( item );
                curUser.sprite.centerEmitter().burst(EnergyParticle.FACTORY, 10);

            } else {

                item.cursed=false;
                GLog.w( TXT_ITEM_UNCURSE, item.name() );
                curUser.sprite.emitter().burst(ShadowParticle.CURSE, 6);
            }

        } else {

            GLog.w( TXT_ITEM_UNKNOWN, item.name() );
		
		}
	}
	
	@Override
	public String desc() {
		return
			"This scroll is able to imbue weapons or armors with a random enchantment " +
            "Wands and rings will be transmuted instead. " +
            "If used on a cursed item, it will completely dispel (or event revert) the curse";
	}

    @Override
    public int price() {
        return isTypeKnown() ? 100 * quantity : super.price();
    }
}
