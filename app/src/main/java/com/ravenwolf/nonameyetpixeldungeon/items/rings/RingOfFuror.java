/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.rings;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;

import java.util.Locale;

public class RingOfFuror extends Ring {

	{
		name = "Ring of Furor";
        shortName = "Fu";
	}
	
	@Override
	protected RingBuff buff( ) {
		return new Furor();
	}
	
	@Override
	public String desc() {
        String mainEffect = "??";
        String currentEffect = "??";

        if( isIdentified() ){
            mainEffect = String.format( Locale.getDefault(), "%.0f", 150 * effect( bonus ) );
            currentEffect = String.format( Locale.getDefault(), "%.0f", 150 * effect( bonus )*(1-(float) Dungeon.hero.HP/Dungeon.hero.HT) );
        }

        StringBuilder desc = new StringBuilder(
            "This ring have the ability to turn pain and suffering into a devastating inner fury, " +
                    "granting additional speed to all melee attacks. " +
                    "Fury grow stronger the more injured the wearer is, but have no effect on a healthy wearer."
        );

        desc.append( "\n\n" );

        desc.append( super.desc() );

        desc.append( " " );

        desc.append(
            "Wearing this ring can modify your melee attack speed up to _" + mainEffect + "%_ based " +
            "on your current health. Current speed bonus is _" + currentEffect + "%_."
        );

        return desc.toString();
	}
	
	public class Furor extends RingBuff {
        @Override
        public String desc() {
            return !isCursed() ?
                    "You feel a battle-lust rushing throw your veins." :
                    "Suddenly, you are afraid of death, your muscles become numb as death approaches" ;
        }
	}
}
