/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.weapons.melee;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.items.armours.shields.Shield;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.criticals.BladeCritical;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.layers.AdditionalSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.layers.WeaponSprite;

public class ObsidianBlade extends MeleeWeaponLight {

	{
		name = "obsidian blade";
		image = ItemSpriteSheet.OBSIDIAN_BLADE;
		drawId= WeaponSprite.OBSIDIAN_BLADE;
		critical=new BladeCritical(this, false, 1.5f);
	}

	public ObsidianBlade() {
		super(4 );
	}

	protected int[][] weapRun() {
        return new int[][]{	{0, 0, 4, 4, 1, 1  },	//frame
                {2, 3, 5, 3, 1, 1  },	//x
                {0, 0, 0, 0, 0, 0 }};
	}
/*
	@Override
	public int dmgMod() {
		return super.dmgMod() -1;
	}
*/
	@Override
	public int penaltyBase() {
		return 4;
	}

	@Override
	public Type weaponType() {
		return Type.M_SWORD;
	}

	@Override
	public float getBackstabMod(){
		return 0.50f;
	}

	@Override
	public String desc() {
		/*return "A pair of curved blades made from splinters of finely chipped obsidian. "
		+"Often used by gnolls in their sacrifice rituals, but also deadly weapons."
				+"\n\n This is a fast weapon and is very effective against unaware enemies.";*/
		return "A curved blade made from splinters of finely chipped obsidian. "
				+"Often used by gnolls in their sacrifice rituals, but also a deadly weapon."
				+"\n\n This weapon is very effective against unaware enemies and is good at scoring critical hits.";
	}
}
