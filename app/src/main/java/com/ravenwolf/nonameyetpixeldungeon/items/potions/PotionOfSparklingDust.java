/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.potions;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Electricity;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.watabou.noosa.audio.Sample;

public class PotionOfSparklingDust extends Potion {

    public static final int BASE_VAL	= 200;
    //public static final int MODIFIER	= 15;

	{
		name = "Potion of Sparkling Dust";
        shortName = "Th";
        harmful = true;
		icon=13;
	}
	
	@Override
	public void shatter( int cell ) {


		Blob blob = Blob.seed( cell, BASE_VAL /*+ MODIFIER * alchemySkill()*/, Electricity.class );
		GameScene.add( blob );
		if (Dungeon.visible[cell]) {

            setKnown();
            splash(cell);
            Sample.INSTANCE.play(Assets.SND_SHATTER);

        }
	}
	
	@Override
	public String desc() {

		return
				"The dust stored in this flask is heavily charged with electricity, " +
						"upon exposure it will create an electric field that will spread instantly over " +
						"water, and will shock any creature that comes in contact ";

	}
	
	@Override
	public int price() {
		return isTypeKnown() ? 40 * quantity : super.price();
	}
}
