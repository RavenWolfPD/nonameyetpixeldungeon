/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.wands;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.Buff;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Blinded;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Dazed;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.debuffs.Ensnared;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.misc.mechanics.Ballistica;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.CellEmitter;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.MagicMissile;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.watabou.noosa.audio.Sample;
import com.watabou.noosa.tweeners.AlphaTweener;
import com.watabou.utils.Callback;


public class CharmOfBlink extends WandUtility {


	{
		name = "Charm of Blink";
        hitChars = false;
		image = ItemSpriteSheet.CHARM_BLINK;
	}

	private int getBlinkDistance(){
		return curCharges+1;
	}


	protected void cursedProc(Hero hero){
		curCharges = (curCharges+1)/2;
		int dmg=hero.absorb( damageRoll(), true )/2;
		hero.damage(dmg, this, Element.ENERGY);
		BuffActive.add(hero, Dazed.class, dmg);
	}

	@Override
	protected void onZap( int cell ) {

		super.onZap(cell );

		cell =getDestinationPos( getBlinkDistance(),cell);

		Ballistica.cast(curUser.pos, cell, goThrough, hitChars);
		for (int i=1; i < Ballistica.distance; i++) {
			int c = Ballistica.trace[i];
			Char ch = Actor.findChar( c );
			if ( ch != null ) {
				ch.damage( ch.absorb( damageRoll(), true ), curUser, Element.ENERGY );
				CellEmitter.center( c ).start( Speck.factory( Speck.LIGHT ), 0.1f, 3 );
			}
		}
		
		curUser.sprite.visible = true;
		appear( Dungeon.hero, cell );
		Dungeon.observe();
	}

	private int getDestinationPos(int distance, int cell){
		int correction=1;
		if (Ballistica.distance > distance + 1 ) {
			cell = Ballistica.trace[ distance ];
		} else if (Actor.findChar( cell ) != null && Ballistica.distance > 1) {
			cell = Ballistica.trace[Ballistica.distance - ++correction];
		}
		while (Actor.findChar( cell )!= null && correction<Ballistica.distance){
			cell = Ballistica.trace[Ballistica.distance - ++correction];
		}
		return cell;
	}
	
	@Override
	protected void fx( int cell, Callback callback ) {

		Ballistica.cast(curUser.pos, cell, goThrough, hitChars);
		cell =getDestinationPos( getBlinkDistance(),cell);

		MagicMissile.whiteLight( curUser.sprite.parent, curUser.pos, cell, callback );
		Sample.INSTANCE.play( Assets.SND_ZAP );
		curUser.sprite.visible = false;
	}

	public static void appear( Char ch, int pos ) {
		
		ch.sprite.interruptMotion();
		Buff.detach( ch, Ensnared.class );
		
		ch.move( pos , false);
		ch.sprite.place( pos );
		
		if (ch.invisible == 0) {
			ch.sprite.alpha( 0 );
			ch.sprite.parent.add( new AlphaTweener( ch.sprite, 1, 0.4f ) );
		}
		
		ch.sprite.emitter().start( Speck.factory( Speck.LIGHT ), 0.2f, 3 );
		Sample.INSTANCE.play( Assets.SND_TELEPORT );
	}
	
	@Override
	public String desc() {
		return
			"This charm is composed by two twisted branches of wood and a blinking crystal in the center. " +
			"Summoning its powers will turn your body into radiant energy "/*that will blind any near critter " */+
					"and allow you to quickly travel in the chosen direction, passing through any foe and damaging them, " +
			"inanimate obstructions, however, will block your path.";
	}

	protected String getEffectDescription(int min , int max, boolean known){
		int reach=maxCharges(known? bonus : 0)*3;
		return  "this charm will " +(known? "":"(probably) ")+"have a max reach of "+reach+" and will deal _" + min + "-" + max + " points _of damage ";
	}
}
