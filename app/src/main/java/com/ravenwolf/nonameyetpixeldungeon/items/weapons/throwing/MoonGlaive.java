/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.ravenwolf.nonameyetpixeldungeon.items.weapons.throwing;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.mobs.npcs.NPC;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.Weapon;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.criticals.BladeCritical;
import com.ravenwolf.nonameyetpixeldungeon.levels.Level;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.BArray;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.MissileSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.ui.QuickSlot;
import com.watabou.utils.Callback;
import com.watabou.utils.PathFinder;
import com.watabou.utils.Random;

import java.util.HashSet;

public class MoonGlaive extends ThrowingWeaponLight {

    {
        name = "moon glaives";
        image = ItemSpriteSheet.MOON_GLAIVE;
        critical=new BladeCritical(this, false, 1.5f);
    }

    public MoonGlaive() {
        this( 1 );
    }

    public MoonGlaive(int number) {
        super( 3 );
        quantity = number;
    }

    @Override
    public int baseAmount() {
        return 4;
    }

    @Override
    public int penaltyBase() {
        return 4;
    }

    @Override
    public int str( int bonus ) {
        return super.str(bonus)+1;
    }

    @Override
    public boolean returnOnHit(Char enemy){
        return  true;
    }


    @Override
    protected boolean returnOnMiss(){
        return true;
    }

    @Override
    public String desc() {
        return "A sharp three-bladed designed for throwing. " +
                "A skilled user can gain feats to ricochet the weapon off opponents and back into their own hands.";
    }

    private boolean canBounceTo(Char enemy){
        return (enemy != null && enemy != curUser && !(enemy instanceof NPC) && Level.fieldOfView[enemy.pos]);
    }


    protected boolean bounce(Char enemy, final boolean hit){
        int cell = enemy.pos;
        HashSet<Char> ns = new HashSet<Char>();

        PathFinder.buildDistanceMap( cell, BArray.not( Dungeon.level.solid, null ), 2 );
        for (int i = 0; i < PathFinder.distance.length; i++) {
            if (PathFinder.distance[i] < Integer.MAX_VALUE) {
                Char ch = Actor.findChar(i);
                if (canBounceTo(ch))
                    ns.add(ch);
            }
        }

        if (ns.size() > 0) {
            final Char newEnemy = Random.element(ns);
            final Weapon weap=this;
            final int enemyPos=newEnemy.pos;
            ((MissileSprite) curUser.sprite.parent.recycle(MissileSprite.class)).
                    reset(cell, newEnemy.pos, curItem.imageAlt(),0.75f, null,new Callback() {
                        @Override
                        public void call() {
                            boolean enemyHit = hit || curUser.shoot(newEnemy, weap);

                            curUser.spendAndNext( getAttackDelay()/*1/weap.speedFactor( curUser )*/ );

                            if ( enemyHit && Random.Float() < breakingRateWhenShot()) {
                                missileBreak(newEnemy);
                            }else {
                                ((MissileSprite) curUser.sprite.parent.recycle(MissileSprite.class)).
                                        reset(enemyPos, curUser.pos, curItem.imageAlt(), null);
                            }
                            QuickSlot.refresh();
                        }
                    });

            //Changes the returning cell to the new target
            //if bounce and dont return uncomment the following lines
            //super.onThrow(newEnemy.pos);
            //curUser.spendAndNext( 1/weapon.speedFactor( curUser ) );
            //QuickSlot.refresh();
            return true;
        }
        if (hit && Random.Float() < breakingRateWhenShot())
            missileBreak(enemy);
        return false;

    }
}
