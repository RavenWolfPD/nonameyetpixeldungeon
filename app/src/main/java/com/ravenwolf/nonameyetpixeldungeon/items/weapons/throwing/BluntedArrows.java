/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.weapons.throwing;

import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.criticals.BluntCritical;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;

public class BluntedArrows extends ThrowingWeaponAmmo {

	{
		name = "blunted arrows";
		image = ItemSpriteSheet.BLUNTED_ARROW;
		critical=new BluntCritical(this);
	}

	public BluntedArrows() {
		this( 1 );
	}

	public boolean stick(Char enemy){
		return false;
	}

	public BluntedArrows(int number) {
		super( 2 );
		quantity = number;
	}

	@Override
	public int baseAmount() {
		return 14;
	}

	@Override
	public String desc() {
		return 
			"Originally designed for knocking birds from the air without damaging their meat and feathers," +
			" blunted arrows quickly became a staple of thieves and thugs who preferred to take their victims down from a distance.";
	}
}
