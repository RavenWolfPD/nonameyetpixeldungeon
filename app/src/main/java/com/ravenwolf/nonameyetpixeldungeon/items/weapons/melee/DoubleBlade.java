/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.weapons.melee;


import com.ravenwolf.nonameyetpixeldungeon.items.weapons.criticals.BladeCritical;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.HeroSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.layers.WeaponSprite;

public class DoubleBlade extends MeleeWeaponMediumPolearms  {

	{
		name = "twinblades";
		image = ItemSpriteSheet.DOUBLE_BLADE;
		drawId= WeaponSprite.DOUBLE_BLADE;
		critical=new BladeCritical(this);
	}

	protected int[][] weapRun() {
        return new int[][]{	{0, 0, 1, 1, 0, 0  },	//frame
                {-4, -3, -1, -3, -5, -5  },	//x
                {0, 0, 0, 0, 0, 0 }};
	}

	protected int[][] weapIddle() {
        return new int[][]{	{0, 0, 0, 0, 0, 0 ,0,0 },	//frame
                {-3, -3, -3, -3, -3, -3 ,-3, -3 },	//x
                {0, 0, 0, 0, 0, 0 ,0, 0 }};
	}
	protected int[][] weapAtk() {
        return new int[][]{	{1, 2, 3, 0 },	//frame
                {-5, -2, 4, 3 },	//x
                {0, -2, 0, 0}};
	}
	protected int[][] weapFly() {
        return new int[][]{	{0 },	//frame
                {-3},	//x
                {0}};
	}
	protected int[][] shieldSlam() {
        return new int[][]{	{0, 0, 0, 0, },	//frame
                {-3, -3, -5, -3, },	//x
                {0, 0, 0, 0, }};
	}
	protected int[][] weapDoubleAtk() {
        return new int[][]{	{1, 2, 3, 4, 0 },	//frame
                {-5, -2, 4, 3, 3 },	//x
                {0, -2, 0, 0, 0}};
	}

	public int[][] getDrawData(int action){
		if (action == HeroSprite.ANIM_DOUBLEHIT)
			return weapDoubleAtk();
		else
			return super.getDrawData(action);
	}


	public DoubleBlade() {
		super( 3 );
	}

	@Override
	public Type weaponType() {
		return Type.M_POLEARM;
	}

	@Override
	public float counterBonusDmg(){//have better counter damage
		return 0.60f;
	}


	@Override
	public String desc() {
		return "Twinblades are wielded with an unusual technique, allowing to attack in every direction with a single strike. Very handy when fighting multiple opponents."
		+"\n\n This weapon will damage any nearby enemy and is great for counter attacks.";
	}
}
