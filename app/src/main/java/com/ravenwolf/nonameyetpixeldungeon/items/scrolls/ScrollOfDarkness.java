/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.scrolls;

import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Blob;
import com.ravenwolf.nonameyetpixeldungeon.actors.blobs.Darkness;
import com.ravenwolf.nonameyetpixeldungeon.misc.utils.GLog;
import com.ravenwolf.nonameyetpixeldungeon.scenes.GameScene;
import com.ravenwolf.nonameyetpixeldungeon.visuals.Assets;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Speck;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.SpellSprite;
import com.watabou.noosa.audio.Sample;

public class ScrollOfDarkness extends Scroll {

    private static final String TXT_MESSAGE	= "You are suddenly engulfed by clouds of impenetrable shadow.";

	{
		name = "Scroll of Darkness";
        shortName = "Da";

        spellSprite = SpellSprite.SCROLL_DARKNESS;
        spellColour = SpellSprite.COLOUR_WILD;
        icon=12;
	}
	
	@Override
	protected void doRead() {

        curUser.sprite.centerEmitter().start( Speck.factory( Speck.DARKNESS ), 0.3f, 5 );
        Sample.INSTANCE.play( Assets.SND_GHOST );

        GameScene.add( Blob.seed( curUser.pos, 1000 * ( 110 + curUser.magicSkill() ) / 100, Darkness.class ) );

        GLog.i( TXT_MESSAGE );

        super.doRead();
	}
	
	@Override
	public String desc() {
		return
			"Unnatural darkness from the deepest abysses is summoned by the call of this otherwise " +
            "unremarkable sheet of paper. This darkness is so thick that nothing can see through it " +
            "without magical means." +
            "\n\nDuration of this effect depends on magic skill of the reader.";
	}

    @Override
    public int price() {
        return isTypeKnown() ? 45 * quantity : super.price();
    }
}
