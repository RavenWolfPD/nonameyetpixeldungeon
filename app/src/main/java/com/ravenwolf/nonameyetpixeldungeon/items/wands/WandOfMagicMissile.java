/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.wands;

import com.ravenwolf.nonameyetpixeldungeon.Dungeon;
import com.ravenwolf.nonameyetpixeldungeon.Element;
import com.ravenwolf.nonameyetpixeldungeon.actors.Actor;
import com.ravenwolf.nonameyetpixeldungeon.actors.Char;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.Weapon;
import com.ravenwolf.nonameyetpixeldungeon.items.weapons.melee.MeleeWeapon;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.Splash;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.EnergyParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.effects.particles.SparkParticle;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSprite;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.watabou.utils.Random;

import java.util.ArrayList;

public class WandOfMagicMissile extends WandCombat {


	{
		name = "Wand of Magic Missile";
        //shortName = "Ma";
		image = ItemSpriteSheet.WAND_MAGICMISSILE;
	}


	@Override
	public int maxCharges( int bonus ) {
		return super.maxCharges(bonus) +1 ;
	}

	protected float rechargeRate(int bonus) {
		return 26f;
	}

	@Override
	public ArrayList<String> actions( Hero hero ) {
		ArrayList<String> actions = super.actions( hero );
		return actions;
	}

    @Override
    protected void cursedProc(Hero hero){
        int dmg=hero.absorb( damageRoll())/2;
        hero.damage(dmg, this, Element.ENERGY);
    }

	@Override
	protected void onZap( int cell ) {

        int power = 2+bonus;

        Splash.at( cell, 0x33FFFFFF, power );

        Char ch = Actor.findChar( cell );

        if (ch != null) {

			int dmg=ch.absorb(damageRoll());

			if (curUser.belongings.weap1 instanceof MeleeWeapon && curUser.belongings.weap1.isEnchanted()){
				int currBonus=curUser.belongings.weap1.bonus;
				curUser.belongings.weap1.bonus=bonus+2;//bonus determine chance to proc, so we use this shitty hack
				curUser.belongings.weap1.enchantment.proc(curUser.belongings.weap1,curUser, ch, dmg);
				curUser.belongings.weap1.bonus=currBonus;
			}

			ch.damage(dmg, curUser, Element.ENERGY);
			ch.sprite.centerEmitter().burst( SparkParticle.FACTORY, Random.IntRange( power , 4 + power ) );

		}
	}


	@Override
	public String desc() {
		return
			"This wand launches missiles of pure magical energy, it has more available charges and recharges faster than most wands. " +
			"While the wand itself lacks any special effects, its bolts can be empowered by the enchantment on your main weapon, " +
			"making it a deadly tool in combination with the right weapon";
	}


}
