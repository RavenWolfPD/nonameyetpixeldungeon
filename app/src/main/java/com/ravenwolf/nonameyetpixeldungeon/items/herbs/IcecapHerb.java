/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.herbs;

import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.BuffActive;
import com.ravenwolf.nonameyetpixeldungeon.actors.buffs.bonuses.Body_AcidResistance;
import com.ravenwolf.nonameyetpixeldungeon.actors.hero.Hero;
import com.ravenwolf.nonameyetpixeldungeon.items.potions.PotionOfFrigidVapours;
import com.ravenwolf.nonameyetpixeldungeon.items.rings.RingOfSatiety;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;

public class IcecapHerb extends Herb {
    {
        name = "Icecap herb";
        image = ItemSpriteSheet.HERB_ICECAP;

        alchemyClass = PotionOfFrigidVapours.class;
        message = "That herb tasted fresh like mint.";
    }

    @Override
    public void onConsume( Hero hero ) {

        float buff=hero.ringBuffs( RingOfSatiety.Satiety.class );
        BuffActive.add( hero, Body_AcidResistance.class, 80*buff );


        super.onConsume( hero );
    }

    @Override
    public String desc() {
        return "Icecap herbs feel cold to touch and have some numbing capabilities. Eating it will slow down your metabolism, increasing resistance against chemicals and body affections. " +
                "They are also used to brew potions of Frigid Vapours.";

       /* return "Icecap herbs feel cold to touch and have some numbing capabilities. Eating it will slow down your metabolism, providing more resistance to chemicals and body affections. "
                +"This is an elemental herb, if combined with any other elemental herb will create a hazard potion";*/

    }



}

