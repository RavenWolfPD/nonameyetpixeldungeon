/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * Yet Another Pixel Dungeon
 * Copyright (C) 2015-2019 Considered Hamster
 *
 * No Name Yet Pixel Dungeon
 * Copyright (C) 2018-2019 RavenWolf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.ravenwolf.nonameyetpixeldungeon.items.weapons.melee;

import com.ravenwolf.nonameyetpixeldungeon.items.weapons.criticals.BluntCritical;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.ItemSpriteSheet;
import com.ravenwolf.nonameyetpixeldungeon.visuals.sprites.layers.WeaponSprite;

public class Quarterstaff extends MeleeWeaponMedium {

	{
		name = "short staff";
		image = ItemSpriteSheet./*QUARTER*/MAGE_STAFF;
		drawId= WeaponSprite.MAGE_STAFF;
		critical=new BluntCritical(this);
	}
	
	public Quarterstaff() {
		super( 1 );
	}

	@Override
	public int str(int bonus) {
		return super.str(bonus)+1;
	}

	@Override
	public int max( int bonus ) {
		return super.max(bonus) -2;
	}

	@Override
	public Type weaponType() {
		return Type.M_POLEARM;
	}

	@Override
	public String desc() {
		return "A well-carved staff, it allows greater " +
                "control over magic wands."
				+"\n\n This weapon increase wands effectiveness.";
	}
}
